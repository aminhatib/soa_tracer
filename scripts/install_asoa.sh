#!/usr/bin/env bash
set -e

# Get OS information
os_info=$(cat /etc/os-release)

# Check if the OS is Ubuntu
if [[ $os_info != *"Ubuntu"* ]] ; then
    echo "[ERROR]: Not running on Ubuntu. This script will not execute." >&2
    exit 1
fi

remove_flag=false
# Parse the command line arguments
while getopts ":r" opt; do
   case $opt in
       r)
           remove_flag=true
           ;;
       \?)
           echo "Invalid option: -$OPTARG" >&2
           exit 1
           ;;
       :)
           echo "Option -$OPTARG requires an argument." >&2
           exit 1
           ;;
       *)
           echo "Unimplemented option: -$option" >&2
           exit 1
           ;;
   esac
done

VERSION="0.5.0a"
DEPS_FOLDER=../deps/asoa/

PACKAGES=(
    "asoa_security"
    "asoa_core"
    "asoa_orchestrator"
    "asoa_utilities"
    )

# Check if ASOA is already installed
err_flag=0
for package in "${PACKAGES[@]}"; do
    if ! dpkg -s "$package" >/dev/null 2>&1; then
        err_flag=1
    fi
done

if [ $err_flag == 0 ]; then
    if $remove_flag; then
        sudo dpkg --remove asoa_security asoa_core asoa_orchestrator asoa_utilities
        exit 0
    fi
    echo "ASOA is already installed on the system"
    exit 0
fi

# get directory name of the script and cd to it
SCRIPT_DIR=$(dirname $0)
cd $SCRIPT_DIR

sudo dpkg --install ${DEPS_FOLDER}asoa_security-$VERSION.deb ${DEPS_FOLDER}asoa_core-$VERSION.deb ${DEPS_FOLDER}asoa_orchestrator-$VERSION.deb ${DEPS_FOLDER}asoa_utilities-$VERSION.deb
