#!/bin/bash

stop_flag=false
view_flag=false

# Parse the command line arguments
while getopts ":sv" opt; do
   case $opt in
       s)
           stop_flag=true
           ;;
       v)
           view_flag=true
           ;;
       \?)
           echo "Invalid option: -$OPTARG" >&2
           exit 1
           ;;
       :)
           echo "Option -$OPTARG requires an argument." >&2
           exit 1
           ;;
       *)
           echo "Unimplemented option: -$option" >&2
           exit 1
           ;;
   esac
done

# Set location for trace files
# export LTTNG_HOME=$HOME/soatracer

if $stop_flag; then
    lttng stop

    if $view_flag; then
        lttng view
    fi

    lttng destroy
    exit 0
fi

lttng-sessiond --daemonize
lttng create
lttng enable-event -u 'soatracer_trace_provider:*' 
lttng start