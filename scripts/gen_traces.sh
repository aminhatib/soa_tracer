#!/bin/bash

# Set location for trace files
export LTTNG_HOME=$HOME/soatracer

lttng create
lttng enable-event -u 'soatracer_trace_provider:*' 
lttng start

## Start command passed as an argument that should be traced here
eval $@

# if [ $? -ne 0 ]; then
#     echo "Failed"
#     lttng clear
#     exit 1
# fi

lttng stop
lttng view
lttng destroy