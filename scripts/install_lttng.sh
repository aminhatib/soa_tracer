#!/usr/bin/env bash
set -e

# Get OS information
os_info=$(cat /etc/os-release)

# Check if the OS is Ubuntu
if [[ $os_info != *"Ubuntu"* ]] ; then
    echo "[ERROR]: Not running on Ubuntu. This script will not execute." >&2
    exit 1
fi

VERSION=2.12

# add repo
apt-add-repository ppa:lttng/stable-$VERSION
apt-get update


sudo apt-get install lttng-tools
sudo apt-get install lttng-modules-dkms
sudo apt-get install liblttng-ust-dev