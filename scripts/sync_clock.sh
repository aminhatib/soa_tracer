#!/bin/bash

set -e

# Check if the user provided the required arguments
if [ -z "$1" ]; then
   echo "Usage: $0 -i <network_interface> [-m]"
   exit 1
fi

master_flag=false
sleep_time=0

# Parse the command line arguments
while getopts ":i:s:m" opt; do
   case $opt in
       i)
           network_interface="$OPTARG"
           ;;
       s)
           sleep_time="$OPTARG"
           ;;
       m)
           master_flag=true
           ;;
       \?)
           echo "Invalid option: -$OPTARG" >&2
           exit 1
           ;;
       :)
           echo "Option -$OPTARG requires an argument." >&2
           exit 1
           ;;
       *)
           echo "Unimplemented option: -$option" >&2
           exit 1
           ;;
   esac
done

# Check if the network interface exists
if ! ip link show "$network_interface" > /dev/null 2>&1; then
   echo "Network interface '$network_interface' does not exist."
   exit 1
fi

echo "Starting ptp sync on $network_interface"

if $master_flag; then
    echo "Setup for 'master' clock"
    sudo timeout $sleep_time ptp4l -i $network_interface -m -S &

else
    echo "Setup for 'slave' clock"
    sudo timeout $sleep_time ptp4l -i $network_interface -s &

    sudo timeout $sleep_time phc2sys -c CLOCK_REALTIME -s $network_interface -w -m &

fi