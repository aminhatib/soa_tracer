#!/usr/bin/env bash
set -e

# Get OS information
os_info=$(cat /etc/os-release)

# Check if the OS is Ubuntu
if [[ $os_info != *"Ubuntu"* ]] ; then
    echo "[ERROR]: Not running on Ubuntu. This script will not execute." >&2
    exit 1
fi

ROS_DISTRO=iron
INSTALL_PKG=ros-base

# Check if ROS2 is already installed
if dpkg -s ros-$ROS_DISTRO-$INSTALL_PKG >/dev/null 2>&1; then
    echo "ROS2 is already installed on the system"
    exit 0
fi

sudo apt-get update && sudo apt upgrade -y

# Based on the documentation in https://docs.ros.org/en/iron/Installation/Ubuntu-Install-Debians.html
sudo apt install software-properties-common
sudo add-apt-repository universe -y

sudo apt update && sudo apt install curl -y
sudo curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -o /usr/share/keyrings/ros-archive-keyring.gpg

echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(. /etc/os-release && echo $UBUNTU_CODENAME) main" | sudo tee /etc/apt/sources.list.d/ros2.list > /dev/null

sudo apt update
sudo apt upgrade -y
sudo apt install ros-$ROS_DISTRO-$INSTALL_PKG -y