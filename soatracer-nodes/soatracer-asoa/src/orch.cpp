#include <iostream>
#include <chrono>
#include <memory>
#include <string>

#include <nlohmann/json.hpp>
#include <argparse/argparse.hpp>

#include <asoa/orchestrator/orchestrator.hpp>

#include "../../common.hpp"

#include "soatracer_tp.h"


int main(int argc, char *argv[])
{
    argparse::ArgumentParser program("asoa-orch");

    program.add_argument("-t", "--time")
        .help("time to start the program at");
    
    program.add_argument("-s", "--samples")
        // .required()
        .help("how many samples")
        .scan<'i', int>();

    auto &group = program.add_mutually_exclusive_group(false);
    group.add_argument("-f", "--file")
        .help("add config file for machine setup");

    group.add_argument("-c", "--config")
        .help("add config for machine setup (in json inside of '')");


    try
    {
        // DEBUG
        // for (int i = 0; i < argc; i++) {
        //     std::cout << argv[i] << std::endl;
        // }
        program.parse_args(argc, argv);
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        std::cerr << program;
        std::exit(1);
    }

    // -----------------------------

    std::string config_file, config_arg;
    int samples;
    if (program.is_used("samples"))
    {
        samples = program.get<int>("samples");
    }

    if (program.is_used("config"))
    {
        config_arg = program.get<std::string>("config");
    }
    else if (program.is_used("file"))
    {
        config_file = program.get<std::string>("file");
    }
    else
    {
        return 1;
    }
    
    std::vector<MachineConfig> conf_list;
    if (config_arg != "")
    {
        json data = json::parse(config_arg);
        conf_list = read_config_list(data);
    }
    else
    {
        return 1;
    }

    if (program.is_used("time"))
    {
        std::string time_to_start = program.get<std::string>("time");

        auto time_now = std::chrono::system_clock::now();
        auto time_now_t = std::chrono::system_clock::to_time_t(time_now);
        auto tm = std::localtime(&time_now_t);
        std::cout << "time now: " << std::put_time(tm, "%H:%M:%S") << " start time: " << time_to_start << std::endl;

        // Wait until the given time is reached
        auto [hours, mins, secs] = parse_time(time_to_start);
        wait_until(hours, mins, secs);
    }

    // init
    Orchestrator orch;

    std::string hw = "SOATracer";
    std::string func_type = "DummyData";

    auto a_conf = conf_list[0];
    int publishers = a_conf.publishers;
    int subscribers = a_conf.subscribers;

    auto [service_pubs, service_subs] = get_service_pubs_subs(conf_list);

    std::vector<std::string> pubs_list;
    std::vector<std::string> subs_list;
    for (const auto& conf : conf_list) {
        for (const auto& service : conf.services) {
            // In case of 1 machine
            if (conf_list.size() == 1) {
                if (!std::count(pubs_list.begin(), pubs_list.end(), "sub")) {
                    pubs_list.push_back("sub");
                }
                if (!std::count(subs_list.begin(), subs_list.end(), "sub")) {
                    subs_list.push_back("sub");
                }
                break;
            }

            if (service.type == "publisher") {
                pubs_list.push_back(service.name + "_pub" + std::to_string(service.id));
            } else {
                if (publishers >= subscribers) {
                    if (!std::count(subs_list.begin(), subs_list.end(), "sub")) {
                        subs_list.push_back("sub");
                    }
                } else {
                    subs_list.push_back(service.name + "_sub" + std::to_string(service.id));
                }
            }
        }
    }

    std::vector<std::string> all_services;
    all_services.insert(all_services.end(), subs_list.begin(), subs_list.end());
    all_services.insert(all_services.end(), pubs_list.begin(), pubs_list.end());

    std::cout << "pubs_subs " << service_pubs << ", " << service_subs << std::endl;
    std::cout << "[INFO]: All services: ";
    for (const auto &item : all_services) {
        std::cout << item << " ";
    }
    std::cout << std::endl;

    int wait_count = 0;
    while(true){
        bool have_pub_services = std::all_of(pubs_list.begin(), pubs_list.end(), [&orch, &hw](const std::string& s_name) {
            return orch.haveService(hw, s_name);
        });

        bool have_sub_services = false;
        // If it is not distributed and is one executable
        if (is_intra_proc(conf_list[0])) {
            have_pub_services = true;
            pubs_list.clear();

            have_sub_services = orch.haveService(hw, "sub");
            subs_list.clear();
            subs_list.push_back("sub");

        } else {
            have_sub_services = std::all_of(subs_list.begin(), subs_list.end(), [&orch, &hw](const std::string& s_name) {
                return orch.haveService(hw, s_name);
            });
        }

        // If its local then ignore pubs services names
        if (conf_list.size() == 1) {
            have_pub_services = true;
        }

        if (have_pub_services && have_sub_services) {
            break;
        } else {
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
            std::cout << "Waiting for all services to appear" << std::endl;
            if (wait_count++ == 10)
            {
                std::cout << "couldn't find all services within 5s, aborting" << std::endl;
                std::exit(5);
            }
        }
    }

    // Connect services
    if (service_pubs < service_subs)
    {
        std::string gua_service;
        std::string req_service;

        for (int i = 0; i < service_subs; i++)
        {
            // In case of 1 machine
            if (conf_list.size() == 1)
            {
                gua_service = "sub";
                req_service = "sub";
            }
            else
            {
                gua_service = pubs_list[0];
                req_service = subs_list[i];
            }
            if (is_intra_proc(conf_list[0])) {
                orch.connectInput(hw, gua_service, "G1", hw, req_service, "R" + std::to_string(i+1), func_type);
            } else {
                orch.connectInput(hw, gua_service, "G1", hw, req_service, "R1" , func_type);
            }
        }
    }
    else
    {
        for (int i = 0; i < service_pubs; i++)
        {
            std::string gua_service;
            std::string req_service;

            // In case of 1 machine
            if (conf_list.size() == 1)
            {
                gua_service = "sub";
                req_service = "sub";
            }
            else
            {
                gua_service = pubs_list[i];
                req_service = subs_list[0];
            }
            orch.connectInput(hw, gua_service, "G1", hw, req_service, "R" + std::to_string(i+1), func_type);
        }
    }

    drop_duplicates(all_services);
    for (const auto& service_name : all_services) {
        orch.setServiceState(hw, service_name, 1);
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }
}