#include <iostream>
#include <chrono>
#include <memory>

#include <nlohmann/json.hpp>
#include <argparse/argparse.hpp>

#include "../../common.hpp"

#include <asoa/core/runtime.hpp>
#include <asoa/core/task.hpp>
#include "services.hpp"


// -------------------------------------------------------------------------------------

int main(int argc, char *argv[])
{
    argparse::ArgumentParser program("soatracer-asoa");

    program.add_argument("-g", "--test");

    program.add_argument("-i", "--id")
        .help("id of service")
        .scan<'i', int>();

    program.add_argument("-t", "--time")
        .help("time to start the program at");
    
    program.add_argument("-s", "--samples")
        // .required()
        .help("how many samples")
        .scan<'i', int>();

    auto &group = program.add_mutually_exclusive_group(false);
    group.add_argument("-f", "--file")
        .help("add config file for machine setup");

    group.add_argument("-c", "--config")
        .help("add config for machine setup (in json inside of '')");


    try
    {
        // DEBUG
        // for (int i = 0; i < argc; i++) {
        //     std::cout << argv[i] << std::endl;
        // }
        program.parse_args(argc, argv);
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        std::cerr << program;
        std::exit(1);
    }

    // -----------------------------

    std::string config_file, config_arg;
    int samples = 0;
    if (program.is_used("samples"))
    {
        samples = program.get<int>("samples");
    }

    if (program.is_used("config"))
    {
        config_arg = program.get<std::string>("config");
    }
    else if (program.is_used("file"))
    {
        config_file = program.get<std::string>("file");
    }
    else
    {
        return 1;
    }
    
    MachineConfig conf;
    if (config_file != "")
    {
        conf = read_config_file(config_file);
    }
    else if (config_arg != "")
    {
        json data = json::parse(config_arg);
        conf = read_config(data);
    }
    else
    {
        return 1;
    }

    if (program.is_used("time"))
    {
        std::string time_to_start = program.get<std::string>("time");

        auto time_now = std::chrono::system_clock::now();
        auto time_now_t = std::chrono::system_clock::to_time_t(time_now);
        auto tm = std::localtime(&time_now_t);
        std::cout << "time now: " << std::put_time(tm, "%H:%M:%S") << " start time: " << time_to_start << std::endl;

        // Wait until the given time is reached
        auto [hours, mins, secs] = parse_time(time_to_start);
        wait_until(hours, mins, secs);
    }

    int service_id = -1;
    if (program.is_used("id")) {
        service_id = program.get<int>("id");
    }

    // Debugging
    std::string test_arg;
    if (program.is_used("test")) {
        test_arg = program.get<std::string>("test");
    }

    auto [service_pubs, service_subs] = get_service_pubs_subs(conf);

    // -----------------------------
    // INIT ASOA
    // initialize the RTPS driver and the ASOA Security extension
    auto asoa_driver = asoa_init();

    // return in case of an error
    if(asoa_driver == nullptr) {
        std::cout << "Failed to initialize RTPS protocol." << std::endl;
        return -1;
    }
    
    // name the hardware like in the architecture tool
    char hardware_name[] = "SOATracer";
    std::cout << "Hello, I am the " << hardware_name << "." << std::endl;

    // make sure the hardware name equals the name specified in the configuration
    if(strcmp(asoa_driver->hardware_name, "nosec") == 0 ||
        strcmp(asoa_driver->hardware_name, hardware_name) != 0) {
        std::cout << "WARNING: Hardware name does not match the name provided in the security configuration." << std::endl;
    }
    // initialize the ASOA runtime
    Runtime::init(hardware_name);

    // create your service
    SOAService* my_service;
    
    std::string service_name = "";

    if ((is_intra_proc(conf) && !conf.distributed) || (conf.publishers > conf.subscribers && service_subs > service_pubs)) {
        service_name = "sub";
    } else if (!is_intra_proc(conf) && (conf.subscribers > conf.publishers && service_subs > service_pubs)) {
        service_name = conf.services[0].name;
        service_id = conf.services[0].id;
        service_name += "_sub" + std::to_string(service_id);
    } else if ((service_pubs >= 1 && service_subs == 0) || (service_pubs == 0 && service_subs >= 1)) {
        service_name = conf.services[0].name;
        service_id = conf.services[0].id;
        if (is_publisher_machine(conf)) {
            service_name += "_pub" + std::to_string(service_id);
        } else {
            service_name = "sub";
        }
    }

    if (service_name == "") {
        std::cout << "[ERROR]: Service provide a service name" << is_intra_proc(conf) << std::endl;
        return 1;
    }

    my_service = new SOAService(service_name.c_str(), conf, samples);

    // add service to the runtime
    Runtime::get()->publishService(my_service);
    std::cout << "Looping..." << std::endl;

    // main runtime loop
    Runtime::get()->loop();

    // clean up after all services have been terminated
    Runtime::get()->destroy();
    asoa_destroy();

    std::cout << "Runtime destroyed." << std::endl;
    
    return 0;

}
