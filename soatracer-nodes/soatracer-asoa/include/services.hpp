#include <asoa/driver/rtps.h>

#include <asoa/core/runtime.hpp>
#include <asoa/core/task.hpp>
#include <fstream>
#include <idl/data.hpp>
#include <iostream>
#include <string>
#include <cctype>

#include <functional>
#include <chrono>
#include <stdexcept>

#include "../../common.hpp"

#include "soatracer_tp.h"

const char r1[] = "R1";
const char r2[] = "R2";
const char r3[] = "R3";
const char r4[] = "R4";
const char g1[] = "G1";
const char g2[] = "G2";
const char g3[] = "G3";
const char g4[] = "G4";

rtps_qos_parameters get_qos_params(ServiceConfig service) {
    Parameters parameters = service.parameters;
    AsoaReliabilityQosPolicy reliability;
    AsoaDurabilityQosPolicy durability;
    AsoaHistoryQosPolicy history;

    if (parameters.reliability == "reliable") {
        reliability = AsoaReliabilityQosPolicy::RELIABLE_RELIABILITY_QOS;
    } else if (parameters.reliability == "best_effort") {
        reliability = AsoaReliabilityQosPolicy::BEST_EFFORT_RELIABILITY_QOS;
    }

    if (parameters.durability == "transient_local") {
        durability = AsoaDurabilityQosPolicy::TRANSIENT_LOCAL_DURABILITY_QOS;
    } else if (parameters.durability == "volatile") {
        durability = AsoaDurabilityQosPolicy::VOLATILE_DURABILITY_QOS;
    }

    if (parameters.history == "keep_all") {
        history = AsoaHistoryQosPolicy::KEEP_ALL_HISTORY_QOS;
    } else if (parameters.history == "keep_last_1") {
        history = AsoaHistoryQosPolicy::KEEP_LAST_HISTORY_QOS;
    }

    return rtps_qos_parameters{history, reliability, durability};
}

class SOAService : public Service
{
private:
public:
    static std::string data;

    Requirement<DummyData, 5, r1> req_data1;
    Requirement<DummyData, 5, r2> req_data2;
    Requirement<DummyData, 5, r3> req_data3;
    Requirement<DummyData, 5, r4> req_data4;
    Guarantee<DummyData, g1> gua_data1;
    Guarantee<DummyData, g2> gua_data2;
    Guarantee<DummyData, g3> gua_data3;
    Guarantee<DummyData, g4> gua_data4;

    // Access handles for conditional tasks for receival
    Requirement<DummyData, 5, r1>::AccessHandle *h_req_data1;
    Requirement<DummyData, 5, r2>::AccessHandle *h_req_data2;
    Requirement<DummyData, 5, r3>::AccessHandle *h_req_data3;
    Requirement<DummyData, 5, r4>::AccessHandle *h_req_data4;

    // Access handles for periodic task
    Requirement<DummyData, 5, r1>::AccessHandle *ph_req_data1;
    Requirement<DummyData, 5, r2>::AccessHandle *ph_req_data2;
    Requirement<DummyData, 5, r3>::AccessHandle *ph_req_data3;
    Requirement<DummyData, 5, r4>::AccessHandle *ph_req_data4;

    // Access handles for conditional task
    Requirement<DummyData, 5, r1>::AccessHandle *ch_req_data1;
    Requirement<DummyData, 5, r2>::AccessHandle *ch_req_data2;
    Requirement<DummyData, 5, r3>::AccessHandle *ch_req_data3;
    Requirement<DummyData, 5, r4>::AccessHandle *ch_req_data4;

    // Publisher periodic tasks
    PeriodicTask pp_task_1;
    PeriodicTask pp_task_2;
    PeriodicTask pp_task_3;
    PeriodicTask pp_task_4;

    // Subscriber periodic tasks
    PeriodicTask sp_task;
    PeriodicTask sp_task_1;
    PeriodicTask sp_task_2;
    PeriodicTask sp_task_3;
    PeriodicTask sp_task_4;

    // Subscriber conditional tasks
    ConditionalTask sc_task;
    ConditionalTask sc_task_1;
    ConditionalTask sc_task_2;
    ConditionalTask sc_task_3;
    ConditionalTask sc_task_4;

    // Receival conditional tasks
    ConditionalTask rc_task_1;
    ConditionalTask rc_task_2;
    ConditionalTask rc_task_3;
    ConditionalTask rc_task_4;

    std::map<int, std::string> service_names;
    std::map<int, int> service_ids;

    MachineConfig conf_;

    std::map<std::string, int> topic_id_;
    std::map<std::string, int> subs_samples_count_;
    std::map<std::string, int> pubs_samples_count_;
    int samples_limit_;
    int subs_finished_;
    int pubs_finished_;

    int service_pubs_;
    int service_subs_;

    // Message buffer to put the current msg sample ids in
    msg_ring_t msgs_ring_;
    msg_buffer_t msgs_buffer_;
    int process_count_;
    // This is to check if msgs_buffer_ has changed after x time
    msg_ring_t test_ring_;
    msg_buffer_t test_buffer_;

    Timer* proc_timer_;
    Timer* stop_timer_;

    SOAService(const char *name, MachineConfig conf, int samples)
        : Service(name), conf_(conf), samples_limit_(samples),
        subs_finished_(0), pubs_finished_(0), service_pubs_(0), service_subs_(0),
        process_count_(0)
    {
        std::tie(service_pubs_, service_subs_) = get_service_pubs_subs(conf_);

        rtps_qos_parameters qos_parameters = get_qos_params(conf_.services[0]);
        uint32_t max_payload_size = 60000000;

        gua_data1.setQosParams(&qos_parameters);
        gua_data1.setPayloadMaxSize(max_payload_size);
        gua_data2.setQosParams(&qos_parameters);
        gua_data2.setPayloadMaxSize(max_payload_size);
        gua_data3.setQosParams(&qos_parameters);
        gua_data3.setPayloadMaxSize(max_payload_size);
        gua_data4.setQosParams(&qos_parameters);
        gua_data4.setPayloadMaxSize(max_payload_size);

        req_data1.setQosParams(&qos_parameters);
        req_data1.setPayloadMaxSize(max_payload_size);
        req_data2.setQosParams(&qos_parameters);
        req_data2.setPayloadMaxSize(max_payload_size);
        req_data3.setQosParams(&qos_parameters);
        req_data3.setPayloadMaxSize(max_payload_size);
        req_data4.setQosParams(&qos_parameters);
        req_data4.setPayloadMaxSize(max_payload_size);
    }

    ~SOAService() = default;

    void setupServiceStructure() override {
        auto services = conf_.services;
        // sort services according to name
        std::sort(services.begin(), services.end(), [](ServiceConfig &a, ServiceConfig &b){
            return a.name < b.name;
        });
        // reverse sort services according to type
        std::sort(services.begin(), services.end(), [](ServiceConfig &a, ServiceConfig &b){
            return a.type > b.type;
        });

        int req_counter = 0;
        int gua_counter = 0;
        int topic_counter = 0;

        bool first = true;
        for (auto service : services) {

            // Add topic(i) with i being the service id to have a unique identifiier,
            // we use that for counters
            std::string topic_i = service.name + std::to_string(service.id);
            // unique topic name to use for ROS2 to publish/subscribe to
            // depending on the situation
            std::string topic = topic_i;
            // In case there are more subscribers (1-n), then we will have
            // only one publisher topic, so we just use the service name
            if (conf_.publishers <= conf_.subscribers){
                topic = service.name;
            }

            int frequency = std::stoi(service.msg.frequency);
            int ms = hz_to_ms(frequency);
            int task_frequency = std::stoi(service.parameters.task_frequency);
            int task_ms = hz_to_ms(task_frequency);
            // In case there was no frequency specified for the task,
            // use the msg frequency instead
            if (task_frequency == 0) {
                task_frequency = frequency;
                task_ms = ms;
            }

            // Add topic id if it doesn't exist in the map
            if (topic_id_.find(topic_i) == topic_id_.end()) {
                topic_id_[topic_i] = topic_counter;
            }

            if (service.type == "publisher") {
                pubs_samples_count_[topic_i] = 0;

                PeriodicTask::PeriodicTaskParam_t p_task_param;

                p_task_param.frequencyHz = frequency;
                p_task_param.start_ref = asoa::OS::time::getTime();
                p_task_param.onWork = [this, service]() {
                    this->publisherCallback(service);
                };

                PeriodicTask *p_task;
                if (gua_counter == 0) {
                    p_task = &pp_task_1;
                } else if (gua_counter == 1) {
                    p_task = &pp_task_2;
                } else if (gua_counter == 2) {
                    p_task = &pp_task_3;
                } else if (gua_counter == 3) {
                    p_task = &pp_task_4;
                }

                initializeTask(p_task, p_task_param);
                if (gua_counter == 0) {
                    addGuarantee(&gua_data1);
                    taskWritesGuarantee(*p_task, gua_data1);
                } else if (gua_counter == 1) {
                    addGuarantee(&gua_data2);
                    taskWritesGuarantee(*p_task, gua_data2);
                } else if (gua_counter == 2) {
                    addGuarantee(&gua_data3);
                    taskWritesGuarantee(*p_task, gua_data3);
                } else if (gua_counter == 3) {
                    addGuarantee(&gua_data4);
                    taskWritesGuarantee(*p_task, gua_data4);
                }

                gua_counter++;
            } else if (service.type == "subscriber") {
                subs_samples_count_[topic_i] = 0;

                ///////////////////////////////////////////////////////
                // Conditional tasks to get the time of receival
                ///////////////////////////////////////////////////////
                ConditionalTask::ConditionalTaskParam_t task_parameter;
                task_parameter.onWork = [this, service]() {
                    this->receiveCallback(service);
                };

                ConditionalTask *rc_task;
                if (req_counter == 0) {
                    rc_task = &rc_task_1;
                } else if (req_counter == 1) {
                    rc_task = &rc_task_2;
                } else if (req_counter == 2) {
                    rc_task = &rc_task_3;
                } else if (req_counter == 3) {
                    rc_task = &rc_task_4;
                }
                initializeTask(rc_task, task_parameter);

                if (req_counter == 0) {
                    taskReadsFromRequirement(*rc_task, req_data1, &h_req_data1);
                    taskAddDataTrigger(*rc_task, req_data1, h_req_data1);
                } else if (req_counter == 1) {
                    taskReadsFromRequirement(*rc_task, req_data2, &h_req_data2);
                    taskAddDataTrigger(*rc_task, req_data2, h_req_data2);
                } else if (req_counter == 2) {
                    taskReadsFromRequirement(*rc_task, req_data3, &h_req_data3);
                    taskAddDataTrigger(*rc_task, req_data3, h_req_data3);
                } else if (req_counter == 3) {
                    taskReadsFromRequirement(*rc_task, req_data4, &h_req_data4);
                    taskAddDataTrigger(*rc_task, req_data4, h_req_data4);
                }

                ///////////////////////////////////////////////////////

                if (service.parameters.task_type == "periodic") {
                    if (first) {
                        PeriodicTask::PeriodicTaskParam_t p_task_parameter;

                        p_task_parameter.frequencyHz = task_frequency;
                        p_task_parameter.start_ref = asoa::OS::time::getTime();
                        p_task_parameter.onWork = [this]() {
                            this->periodicCallback();
                        };

                        // TRACEPOINT
                        lttng_ust_tracepoint(TRACEPOINT_PROVIDER, init_proc_timer, topic_i.c_str(), conf_.id);
                        initializeTask(&sp_task, p_task_parameter);
                        first = false;
                    }
                    
                    if (req_counter == 0) {
                        addRequirement(&req_data1);
                        taskReadsFromRequirement(sp_task, req_data1, &ph_req_data1);
                    } else if (req_counter == 1) {
                        addRequirement(&req_data2);
                        taskReadsFromRequirement(sp_task, req_data2, &ph_req_data2);
                    } else if (req_counter == 2) {
                        addRequirement(&req_data3);
                        taskReadsFromRequirement(sp_task, req_data3, &ph_req_data3);
                    } else if (req_counter == 3) {
                        addRequirement(&req_data4);
                        taskReadsFromRequirement(sp_task, req_data4, &ph_req_data4);
                    }
                }

                ///////////////////////////////////////////////////////

                if (service.parameters.task_type == "conditional") {
                    if (first) {
                        ConditionalTask::ConditionalTaskParam_t c_task_parameter;
                        c_task_parameter.onWork = [this, service]() {
                            this->conditionalCallback(service);
                        };

                        initializeTask(&sc_task, c_task_parameter);
                        first = false;
                    }

                    if (req_counter == 0) {
                        addRequirement(&req_data1);
                        taskReadsFromRequirement(sc_task, req_data1, &ch_req_data1);
                        taskAddDataTrigger(sc_task, req_data1, ch_req_data1);
                    } else if (req_counter == 1) {
                        addRequirement(&req_data2);
                        taskReadsFromRequirement(sc_task, req_data2, &ch_req_data2);
                        taskAddDataTrigger(sc_task, req_data2, ch_req_data2);
                    } else if (req_counter == 2) {
                        addRequirement(&req_data3);
                        taskReadsFromRequirement(sc_task, req_data3, &ch_req_data3);
                        taskAddDataTrigger(sc_task, req_data3, ch_req_data3);
                    } else if (req_counter == 3) {
                        addRequirement(&req_data4);
                        taskReadsFromRequirement(sc_task, req_data4, &ch_req_data4);
                        taskAddDataTrigger(sc_task, req_data4, ch_req_data4);
                    }
                }

                service_names[req_counter] = topic;
                service_ids[req_counter] = service.id;
                req_counter++;
            }
            topic_counter++;
        }

        stop_timer_ = new Timer(std::chrono::seconds(5), [this](){
            if (msgs_ring_.empty() || areMsgBuffersEqual(msgs_ring_, test_ring_)) {
                Runtime::get()->destroy();
                asoa_destroy();
                std::exit(1);
            }

            test_ring_ = cloneBuffer(msgs_ring_);
        });
        stop_timer_->start();

    }

    bool onStartRequest()
    {
        std::cout << "Service is starting. " << std::endl;
        return true;
    }

    bool onStopRequest()
    {
        std::cout << "Service is not stopped. " << std::endl;
        return true;
    }

    void publisherCallback(const ServiceConfig &service) {
        std::string topic_i = service.name + std::to_string(service.id);
        std::string topic = topic_i;
        if (conf_.publishers <= conf_.subscribers)
        {
            topic = service.name;
        }
        auto topic_id = topic_id_[topic_i];

        if (pubs_samples_count_[topic_i] >= samples_limit_) {
            return;
        }

        DummyData::Data data_;

        data_.data = std::to_string(pubs_samples_count_[topic_i] + 1) + ":";
        int difference = data_.data.length();

        int msg_size = std::stoi(service.msg.size);
        int msg_frequency = std::stoi(service.msg.frequency);

        for (int i = 0; i < msg_size - difference; i++){
            data_.data += "0";
        }

        std::cout << "[SAMPLE: " << pubs_samples_count_[topic_i] + 1 << "] " << "Service is sending dummy data: "\
                    << std::to_string(data_.data.size()) << " bytes and frequency " << msg_frequency << std::endl;

        // TRACEPOINT
        lttng_ust_tracepoint(TRACEPOINT_PROVIDER, pre_publish, topic.c_str(), pubs_samples_count_[topic_i] + 1, conf_.id);
        // std::cout << "[DEBUG]: Before sendData" << std::endl;
        if (topic_id == 0){
            gua_data1.sendData(data_);
        } else if (topic_id == 1) {
            gua_data2.sendData(data_);
        } else if (topic_id == 2) {
            gua_data3.sendData(data_);
        } else if (topic_id == 3) {
            gua_data4.sendData(data_);
        }
        // std::cout << "[DEBUG]: After sendData" << std::endl;
        // TRACEPOINT
        lttng_ust_tracepoint(TRACEPOINT_PROVIDER, after_publish, topic.c_str(), pubs_samples_count_[topic_i] + 1, conf_.id);

        pubs_samples_count_[topic_i]++;

        if (pubs_samples_count_[topic_i] == samples_limit_) {
            pubs_finished_++;

            // TRACEPOINT
            lttng_ust_tracepoint(TRACEPOINT_PROVIDER, published_all, topic.c_str(), pubs_samples_count_[topic_i] + 1, conf_.id);

            if (pubs_finished_ == service_pubs_) {
                if (service_subs_ == 0 || subs_finished_ == service_subs_) {
                    stop_timer_->stop();
                    std::exit(0);
                }
            }
        }
    }

    void periodicCallback() {
        process_count_++;
        for (const auto& [topic_i, data] : msgs_ring_) {
            std::string topic = get_topic_only(topic_i);
            int seq_num = 0;
            int msg_count = 0;

            auto msg_it = data.find("seq");
            if (msg_it != data.end()) {
                seq_num = msg_it->second;
            } else {
                continue;
            }
            auto count_it = data.find("count");
            if (count_it != data.end()) {
                msg_count = count_it->second;
            } else {
                continue;
            }

            // TRACEPOINT
            if (conf_.subscribers >= conf_.publishers) {
                lttng_ust_tracepoint(TRACEPOINT_PROVIDER, periodic_processing, process_count_, topic.c_str(), subs_samples_count_[topic_i] + 1, seq_num, conf_.id);
            } else {
                lttng_ust_tracepoint(TRACEPOINT_PROVIDER, periodic_processing, process_count_, topic_i.c_str(), subs_samples_count_[topic_i] + 1, seq_num, conf_.id);
            }

            if (subs_samples_count_[topic_i] == samples_limit_) {
                if (subs_finished_ == service_subs_) {
                    if (service_pubs_ == 0 || pubs_finished_ == service_pubs_) {
                        stop_timer_->stop();
                        std::exit(0);
                    }
                }
            }
        }

        return;
    }

    void conditionalCallback(const ServiceConfig &service) {
        for (const auto& [topic_id, topic] : service_names) {
            int service_id = service_ids[topic_id];
            std::string topic_i = get_topic_only(topic) + std::to_string(service_id);

            if (subs_samples_count_[topic_i] >= samples_limit_) {
                return;
            }

            DummyData::Data data_;

            // std::cout << "[DEBUG]: Before pullData" << std::endl;
            if (is_intra_proc(conf_)) {
                if (service_id == 1) {
                    ch_req_data1->pullData(data_);
                } else if (service_id == 2) {
                    ch_req_data2->pullData(data_);
                } else if (service_id == 3) {
                    ch_req_data3->pullData(data_);
                } else if (service_id == 4) {
                    ch_req_data4->pullData(data_);
                }
            } else {
                if (topic_id == 0) {
                    ch_req_data1->pullData(data_);
                } else if (topic_id == 1) {
                    ch_req_data2->pullData(data_);
                } else if (topic_id == 2) {
                    ch_req_data3->pullData(data_);
                } else if (topic_id == 3) {
                    ch_req_data4->pullData(data_);
                }
            }
            // std::cout << "[DEBUG]: After pullData" << std::endl;

            auto [seq_num, msg] = get_seq_msg(data_.data);


            // TRACEPOINT
            lttng_ust_tracepoint(TRACEPOINT_PROVIDER, after_receiving_cond, topic.c_str(), subs_samples_count_[topic_i] + 1, seq_num, conf_.id);

            if (subs_samples_count_[topic_i] == samples_limit_) {
                if (subs_finished_ == service_subs_) {
                    if (service_pubs_ == 0 || pubs_finished_ == service_pubs_) {
                        stop_timer_->stop();
                        std::exit(0);
                    }
                }
            }
        }
    }

    void receiveCallback(const ServiceConfig &service) {
        std::string topic_i = service.name + std::to_string(service.id);
        std::string topic = topic_i;
        if (conf_.publishers <= conf_.subscribers) {
            topic = service.name;
        }

        if (subs_samples_count_[topic_i] >= samples_limit_) {
            return;
        }

        auto topic_id = topic_id_[topic_i];

        DummyData::Data data_;

        if (is_intra_proc(conf_)) {
            if (service.id == 1) {
                h_req_data1->pullData(data_);
            } else if (service.id == 2) {
                h_req_data2->pullData(data_);
            } else if (service.id == 3) {
                h_req_data3->pullData(data_);
            } else if (service.id == 4) {
                h_req_data4->pullData(data_);
            }
        } else {
            if (topic_id == 0) {
                h_req_data1->pullData(data_);
            } else if (topic_id == 1) {
                h_req_data2->pullData(data_);
            } else if (topic_id == 2) {
                h_req_data3->pullData(data_);
            } else if (topic_id == 3) {
                h_req_data4->pullData(data_);
            }
        }

        auto seq_num = get_seq(data_.data);

        msgs_ring_[topic_i] = {
            {"count", subs_samples_count_[topic_i]},
            {"seq", seq_num},
        };

        // TRACEPOINT
        lttng_ust_tracepoint(TRACEPOINT_PROVIDER, after_receiving, topic.c_str(), subs_samples_count_[topic_i] + 1, seq_num, conf_.id);

        std::cout << "[" << topic_i << "]" << "[COUNT: " << subs_samples_count_[topic_i] + 1 << "][SEQ: " << seq_num << "] Received: " << data_.data.size() << " bytes" << std::endl;

        subs_samples_count_[topic_i]++;

        if (subs_samples_count_[topic_i] == samples_limit_) {
            // TRACEPOINT
            lttng_ust_tracepoint(TRACEPOINT_PROVIDER, received_all, topic.c_str(), seq_num, conf_.id);

            subs_finished_++;
            if (service.parameters.task_type == "conditional") {
                if (subs_finished_ == service_subs_) {
                    if (service_pubs_ == 0 || pubs_finished_ == service_pubs_) {
                        stop_timer_->stop();
                        std::exit(0);
                    }
                }
            }
        }
    }
    std::string name();
};