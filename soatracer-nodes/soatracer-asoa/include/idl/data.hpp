#include <asoa/core/functionality.hpp>

#include <string>


class DummyData : public Functionality {
 private:
  const std::string type_name_ = "DummyData";

  uint32_t type_id_ = 1;

 public:
  struct Data : public FuncComponentBase {
    std::string data;
    uint32_t data_max_size = 100000000;

    uint32_t maxSize() override {
      if (data.size() == 0) {
        return data_max_size;
      } else {
        return data.size();
      }
    }

    bool deserialize(const uint8_t *buffer, uint32_t length) override {
      data = std::string(reinterpret_cast<const char*>(buffer));
      return true;
    }

    long serialize(uint8_t *buffer, uint32_t max_size) override {
      std::copy(data.begin(), data.end(), buffer);
      return maxSize();
    }
  } data_;

  struct Quality : public FuncComponentBase {
    uint8_t quality[500];

    virtual uint32_t maxSize() override { return sizeof(quality); }

    bool deserialize(const uint8_t *buffer, uint32_t length) override {
      UNUSED(buffer);
      UNUSED(length);

      return true;
    }

    long serialize(uint8_t *buffer, uint32_t max_size) override {
      UNUSED(buffer);
      UNUSED(max_size);

      return maxSize();
    }
  } quality_;

  struct Parameter : public FuncComponentBase {
    uint8_t param[500];

    uint32_t maxSize() override { return sizeof(param); }

    bool deserialize(const uint8_t *buffer, uint32_t length) override {
      UNUSED(buffer);
      UNUSED(length);

      return true;
    }

    long serialize(uint8_t *buffer, uint32_t max_size) override { 
      UNUSED(buffer);
      UNUSED(max_size);

      return 0; 
    }
  } parameter_;

  void Functionality() {}

  const std::string &getTypeName() override { return type_name_; }

  std::uint32_t getTypeID() override { return type_id_; }
};