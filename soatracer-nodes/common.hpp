#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <thread>
#include <sstream>
#include <string>
#include <tuple>
#include <set>
#include <variant>
#include <mutex>
#include <condition_variable>

#include <nlohmann/json.hpp>

using json = nlohmann::json;


typedef std::map<std::string, std::map<std::string, std::variant<std::string, int>>> msg_buffer_t;
typedef std::map<std::string, std::map<std::string, int>> msg_ring_t;

class Timer {
public:
    Timer(std::chrono::milliseconds duration, std::function<void()> callback)
        : duration_(duration), callback_(callback), stop_(false) {}

    void start() {
        thread_ = std::thread{[this]() {
            while (!stop_)
            {
                std::unique_lock<std::mutex> lock(mutex_);
                condition_.wait_for(lock, duration_, [this] { return stop_; });
                if (!stop_) {
                    callback_();
                }
            }
        }};
    }

    void stop() {
        {
            std::lock_guard<std::mutex> lock(mutex_);
            stop_ = true;
        }
        // Notify the thread to stop early
        condition_.notify_one();
        if (thread_.joinable()) {
            thread_.join();
        }
    }

    ~Timer() {
        stop();
    }

private:
    std::chrono::milliseconds duration_;
    std::function<void()> callback_;
    std::thread thread_;
    bool stop_;
    std::mutex mutex_;
    std::condition_variable condition_;
};

struct Parameters
{
    std::string reliability;
    std::string history;
    std::string durability;
    std::string task_type;
    std::string task_frequency;
};

struct ServiceMsg
{
    std::string size;
    std::string frequency;
};

struct ServiceConfig
{
    int id;
    std::string name;
    std::string type;
    ServiceMsg msg;
    Parameters parameters;
};

struct MachineConfig
{
    int id;
    bool distributed;
    std::string comm_type;
    int publishers;
    int subscribers;
    std::vector<ServiceConfig> services;
};


MachineConfig read_config(json data) {
    MachineConfig conf;

    conf.id = data["id"];
    conf.distributed = data["distributed"];
    conf.comm_type = data["comm_type"];
    conf.publishers = data["publishers"];
    conf.subscribers = data["subscribers"];
    for (auto service : data["services"]) {

        ServiceMsg service_msg;
        if (service["msg"].is_null()) {
            service_msg = {"0","0"};
        } else {
            service_msg = {service["msg"]["size"], service["msg"]["frequency"]};
        }

        conf.services.push_back({
            service["id"],
            service["name"],
            service["type"],
            service_msg,
            Parameters{
                service["parameters"]["reliability"],
                service["parameters"]["history"],
                service["parameters"]["durability"],
                service["parameters"]["task_type"],
                service["parameters"]["task_frequency"],
            },
        });
    }

    return conf;
}

std::vector<MachineConfig> read_config_list(json data) {
    std::vector<MachineConfig> conf_list;
    for (const auto &item : data)
    {
        MachineConfig conf = read_config(item);

        conf_list.push_back(conf);
    }
    return conf_list;
}

MachineConfig read_config_file(std::string file_path) {
    std::ifstream f(file_path);
    json data = json::parse(f);

    return read_config(data);
}

// -------------------------------------------------------------------------------------

void wait_until(int hours, int mins, int secs){
    auto now = std::chrono::system_clock::now();

    // Convert the current time to time_t
    std::time_t now_t = std::chrono::system_clock::to_time_t(now);

    // Convert the time_t to tm
    std::tm now_tm = *std::localtime(&now_t);

    // Set the desired time to
    now_tm.tm_hour = hours;
    now_tm.tm_min = mins;
    now_tm.tm_sec = secs;

    // Convert the tm back to time_t
    std::time_t desired_time_t = std::mktime(&now_tm);

    // Convert the desired time_t back to system_clock time
    std::chrono::system_clock::time_point desired_time = std::chrono::system_clock::from_time_t(desired_time_t);

    // Wait in loop until the desired time is reached
    while (now < desired_time) {
        now = std::chrono::system_clock::now();
        std::this_thread::sleep_for(std::chrono::nanoseconds(100000));
    }

    // The desired time has been reached
    std::cout << "Desired time has been reached!" << std::endl;
    return;
}

std::tuple<int, int, int> parse_time(std::string time_str) {
    std::istringstream iss(time_str);
    std::string token;
    int hours, mins, secs;

    // Parse the hours
    std::getline(iss, token, ':');
    if (!(std::istringstream(token) >> hours)) {
        throw std::invalid_argument("Invalid time stamp: invalid hours");
    }

    // Parse the minutes
    std::getline(iss, token, ':');
    if (!(std::istringstream(token) >> mins)) {
        throw std::invalid_argument("Invalid time stamp: invalid minutes");
    }

    // Parse the seconds
    std::getline(iss, token, ':');
    if (!(std::istringstream(token) >> secs)) {
        throw std::invalid_argument("Invalid time stamp: invalid seconds");
    }

    // Check for invalid time stamp for each of the hours, mins, secs
    if (hours < 0 || hours > 23 || mins < 0 || mins > 59 || secs < 0 || secs > 59) {
        throw std::invalid_argument("Invalid time stamp: hours, minutes, or seconds out of range");
    }

    return std::tuple<int, int, int> (hours, mins, secs);
}


bool is_publisher_machine(MachineConfig conf) {
    auto services = conf.services;
    for (auto service : services) {
        if (service.type == "publisher") {
            return true;
        }
        return false;
    }
    return false;
}

std::tuple<int, int> get_service_pubs_subs(MachineConfig conf) {
    int pubs = 0;
    int subs = 0;
    
    auto services = conf.services;
    for (auto service : services) {
        if (service.type == "publisher") {
            pubs++;
        } else {
            subs++;
        }
    }

    return std::tuple<int, int> (pubs, subs);
}

std::tuple<int, int> get_service_pubs_subs(std::vector<MachineConfig> conf_list) {
    int sum_pubs = 0;
    int sum_subs = 0;
    for (const auto& conf : conf_list) {
        auto [service_pubs, service_subs] = get_service_pubs_subs(conf);
        sum_pubs += service_pubs;
        sum_subs += service_subs;
    }

    return std::tuple<int, int> (sum_pubs, sum_subs);
}

// Find and remove a string by name in a vector
void removeStringByName(std::vector<std::string>& strings, const std::string& name) {
  for (size_t i = 0; i < strings.size(); ++i) {
    if (strings[i] == name) {
      // Remove the string at index i
      strings.erase(strings.begin() + i);
      // Decrement i since we've removed an element
      --i;
    }
  }
}

bool compareMaps(const std::map<std::string, int>& map1, const std::map<std::string, int>& map2) {
    if (map1.size() != map2.size()) {
        return false;
    }

    for (const auto& keyValue : map1) {
        auto range2 = map2.equal_range(keyValue.first);
        if (range2.first == range2.second || range2.first->second != keyValue.second) {
            return false;
        }
    }

    return true;
}

std::map<std::string, int> cloneMap(const std::map<std::string, int>& originalMap) {
    std::map<std::string, int> clonedMap;
    for (const auto& keyValue : originalMap) {
        clonedMap.insert(keyValue);
    }
    return clonedMap;
}

bool areMsgBuffersEqual(const msg_buffer_t& map1, const msg_buffer_t& map2)
{
    // Check if the sizes of the maps are equal
    if (map1.size() != map2.size()) {
        return false;
    }
    // Iterate over the keys and values of both maps
    for (const auto& pair1 : map1) {
        // Check if the key exists in the second map
        auto it2 = map2.find(pair1.first);
        if (it2 == map2.end()) {
            return false;
        }

        // Check if the inner maps are equal
        if (pair1.second.size() != it2->second.size()) {
            return false;
        }

        // Iterate over the keys and values of the inner maps
        for (const auto& pair2 : pair1.second) {
            auto it3 = it2->second.find(pair2.first);
            if (it3 == it2->second.end()) {
                return false;
            }

            // Check if the values are equal
            if (pair2.second != it3->second) {
                return false;
            }
        }
    }

    return true;
}

msg_buffer_t cloneBuffer(const msg_buffer_t &original)
{
    msg_buffer_t cloned;

    for (const auto& outerPair : original)
    {
        std::map<std::string, std::variant<std::string, int>> innerMap;

        for (const auto& innerPair : outerPair.second)
        {
            std::variant<std::string, int> value;

            if (std::holds_alternative<std::string>(innerPair.second))
            {
                value = innerPair.second;
            }
            else if (std::holds_alternative<int>(innerPair.second))
            {
                value = innerPair.second;
            }

            innerMap.insert({innerPair.first, value});
        }

        cloned.insert({outerPair.first, innerMap});
    }

    return cloned;
}

msg_ring_t cloneBuffer(const msg_ring_t &original)
{
    msg_ring_t cloned;

    for (const auto& outerPair : original)
    {
        std::map<std::string, int> innerMap;

        for (const auto& innerPair : outerPair.second)
        {
            int value;

            value = innerPair.second;

            innerMap.insert({innerPair.first, value});
        }

        cloned.insert({outerPair.first, innerMap});
    }

    return cloned;
}

bool areMsgBuffersEqual(const msg_ring_t& map1, const msg_ring_t& map2)
{
    // Check if the sizes of the maps are equal
    if (map1.size() != map2.size()) {
        return false;
    }
    // Iterate over the keys and values of both maps
    for (const auto& pair1 : map1) {
        // Check if the key exists in the second map
        auto it2 = map2.find(pair1.first);
        if (it2 == map2.end()) {
            return false;
        }

        // Check if the inner maps are equal
        if (pair1.second.size() != it2->second.size()) {
            return false;
        }

        // Iterate over the keys and values of the inner maps
        for (const auto& pair2 : pair1.second) {
            auto it3 = it2->second.find(pair2.first);
            if (it3 == it2->second.end()) {
                return false;
            }

            // Check if the values are equal
            if (pair2.second != it3->second) {
                return false;
            }
        }
    }

    return true;
}

bool is_intra_proc(const MachineConfig &conf) {
    if (conf.distributed) {
        return false;
    }
    if (conf.comm_type != "intra") {
        return false;
    }
    return true;
}

int hz_to_ms(int hz) {
    if (hz == 0) {
        return 0;
    }
    return 1000 / hz;
}

template <typename T>
void drop_duplicates(std::vector<T> &vec) {
    std::vector<T> unique_vec;
    std::set<T> set;

    for (auto item : vec)
    {
        if (set.insert(item).second)
        {
            unique_vec.push_back(item);
        }
    }

    vec = unique_vec;
}

std::tuple<int, std::string> get_seq_msg(const std::string& str) {
    int seq_num = 0;
    std::string payload;

    size_t pos = str.find(":");
    if (pos != std::string::npos)
    {
        seq_num = std::stoi(str.substr(0, pos));
        payload = str.substr(pos + 1);
    }
    return std::tuple<int, std::string> (seq_num, payload);
}

int get_seq(const std::string& str) {
    int seq_num = 0;

    size_t pos = str.find(":");
    if (pos != std::string::npos)
    {
        seq_num = std::stoi(str.substr(0, pos));
    }
    return seq_num;
}

std::string get_topic_only(const std::string& service_name) {
    std::string result;
    
    // Iterate through the characters in the string
    for (char ch : service_name) {
        // Check if the character is a digit
        if (!isdigit(ch)) {
            // If it's not a digit, add it to the result string
            result += ch;
        } else {
            // If it's a digit, stop iterating
            break;
        }
    }
    
    return result;
}