#include <iostream>
#include <chrono>
#include <memory>

#include <nlohmann/json.hpp>
#include <argparse/argparse.hpp>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "rmw/qos_profiles.h"

#include "../../common.hpp"

#include "soatracer_tp.h"

using namespace std::chrono_literals;
using std::placeholders::_1;


rclcpp::QoS get_qos_profile(ServiceConfig service)
{
    Parameters parameters = service.parameters;
    rclcpp::ReliabilityPolicy reliability;
    rclcpp::DurabilityPolicy durability;

    if (parameters.reliability == "best_effort") {
        reliability = rclcpp::ReliabilityPolicy::BestEffort;
    } else if (parameters.reliability == "reliable") {
        reliability = rclcpp::ReliabilityPolicy::Reliable;
    } else {
        reliability = rclcpp::ReliabilityPolicy::BestAvailable;
    }

    if (parameters.durability == "transient_local") {
        durability = rclcpp::DurabilityPolicy::TransientLocal;
    } else if (parameters.durability == "volatile") {
        durability = rclcpp::DurabilityPolicy::Volatile;
    } else {
        durability = rclcpp::DurabilityPolicy::BestAvailable;
    }

    rclcpp::QoS qos_profile(rclcpp::KeepLast(10));
    if (parameters.history == "keep_last_1") {
        rclcpp::QoS qos_profile(rclcpp::KeepLast(1), rmw_qos_profile_default);
    } else if (parameters.history == "keep_all") {
        rclcpp::QoS qos_profile(rclcpp::KeepAll);
    } else {
        rclcpp::QoS qos_profile(rclcpp::KeepLast(10), rmw_qos_profile_default);
    }

    qos_profile.reliability(reliability);
    qos_profile.durability(durability);
    return qos_profile;
}

std::vector<std::string> get_pubs_topics(std::vector<ServiceConfig> &services) {
    std::vector<std::string> topics;
    for (auto service : services) {
        if (service.type == "publisher") {
            topics.push_back(service.name);
        }
    }
    return topics;
}

// -------------------------------------------------------------------------------------

class SOANode : public rclcpp::Node
{
public:
    SOANode(const std::string &node_name, MachineConfig conf, int samples)
        : Node(node_name), conf_(conf), count_(0), samples_limit_(samples), subs_finished_(0), pubs_finished_(0),
        service_pubs_(0), service_subs_(0), process_count_(0)
    {
        std::tie(service_pubs_, service_subs_) = get_service_pubs_subs(conf_);

        // used for subscribers topic name, according to the publishers topics
        int topic_counter = 0;

        auto services = conf.services;
        // reverse sort services according to type
        std::sort(services.begin(), services.end(), [](ServiceConfig &a, ServiceConfig &b){
            return a.type > b.type;
        });

        bool first = true;
        for (auto service : services) {
            auto qos_profile = get_qos_profile(service);
            int frequency = std::stoi(service.msg.frequency);
            int ms = hz_to_ms(frequency);

            // Add topic(i) with i being the service id to have a unique identifiier,
            // we use that for counters
            std::string topic_i = service.name + std::to_string(service.id);
            // unique topic name to use for ROS2 to publish/subscribe to
            // depending on the situation
            std::string topic = topic_i;
            // In case there are more subscribers (1-n), then we will have
            // only one publisher topic, so we just use the service name
            if (conf_.publishers <= conf_.subscribers){
                topic = service.name;
            }

            int task_frequency = std::stoi(service.parameters.task_frequency);
            int task_ms = hz_to_ms(task_frequency);
            // In case there was no frequency specified for the task,
            // use the msg frequency instead
            if (task_frequency == 0) {
                task_frequency = frequency;
                task_ms = ms;
            }

            if (service.type == "publisher") {
                // Initialize map
                pubs_samples_count_[topic_i] = 0;
                publishers_[topic_i] = this->create_publisher<std_msgs::msg::String>(topic, qos_profile);

                pub_timers_[topic_i] = this->create_wall_timer(
                    std::chrono::milliseconds(ms), 
                    [this, service]() {
                        this->publisher_callback(service);
                    }
                );

            } else if (service.type == "subscriber") {
                // Initialize map
                subs_samples_count_[topic_i] = 0;

                // If there is a subscriber defined in the config
                if (first && service.parameters.task_type == "periodic") {
                    // TRACEPOINT
                    lttng_ust_tracepoint(TRACEPOINT_PROVIDER, init_proc_timer, topic_i.c_str(), conf_.id);
                    proc_timer_ = this->create_wall_timer(
                        std::chrono::milliseconds(task_ms), std::bind(&SOANode::process_msg, this)
                    );
                    first = false;
                }

                subscribers_[topic_i] = this->create_subscription<std_msgs::msg::String>(
                    topic, qos_profile, 
                    [this, service](const std_msgs::msg::String::SharedPtr msg) {
                        this->subscriber_callback(msg, service);
                    }
                );
                // Shutdown in case no msgs were received after 5 seconds
                sub_kill_timer_ = this->create_wall_timer(
                    std::chrono::seconds(5), std::bind(&SOANode::stop_subscribing, this)
                );
                topic_counter++;
            }
        }
        
    }

private:
    void publisher_callback(const ServiceConfig &service)
    {
        std::string topic_i = service.name + std::to_string(service.id);
        std::string topic = topic_i;
        if (conf_.publishers <= conf_.subscribers) {
            topic = service.name;
        }

        if (pubs_samples_count_[topic_i] >= samples_limit_) {
            return;
        }

        auto message = std_msgs::msg::String();
        message.data = std::to_string(pubs_samples_count_[topic_i] + 1) + ":";
        int difference = message.data.length();

        int msg_size = std::stoi(service.msg.size);
        int msg_frequency = std::stoi(service.msg.frequency);

        for (int i = 0; i < msg_size - difference; i++){
            message.data += "0";
        }
        RCLCPP_INFO(this->get_logger(), "Publishing: [SAMPLE: %d] dummy message with '%ld' bytes and frequency: '%d'", 
                pubs_samples_count_[topic_i] + 1, message.data.length(), msg_frequency);
        if (publishers_.find(topic_i) != publishers_.end()) {
            auto publisher = publishers_[topic_i];

            // TRACEPOINT
            lttng_ust_tracepoint(TRACEPOINT_PROVIDER, pre_publish, topic.c_str(), pubs_samples_count_[topic_i] + 1, conf_.id);

            publisher->publish(message);

            // TRACEPOINT
            lttng_ust_tracepoint(TRACEPOINT_PROVIDER, after_publish, topic.c_str(), pubs_samples_count_[topic_i] + 1, conf_.id);
        }

        pubs_samples_count_[topic_i]++;

        if (pubs_samples_count_[topic_i] == samples_limit_) {
            pubs_finished_++;
            // TRACEPOINT
            lttng_ust_tracepoint(TRACEPOINT_PROVIDER, published_all, topic.c_str(), pubs_samples_count_[topic_i], conf_.id);
            pub_timers_[topic_i]->cancel();
            if (conf_.distributed || pubs_finished_ == service_pubs_) {
                if (service_subs_ == 0 || subs_finished_ == service_subs_) {
                    rclcpp::shutdown();
                }
            }
            return;
        }
    }

    void subscriber_callback(const std_msgs::msg::String::SharedPtr msg, const ServiceConfig &service)
    {
        std::string topic_i = service.name + std::to_string(service.id);
        std::string topic = topic_i;
        // topic = service.name;
        if (conf_.publishers <= conf_.subscribers)
        {
            topic = service.name;
        }
        
        if (subs_samples_count_[topic_i] >= samples_limit_) {
            return;
        }

        auto seq_num = get_seq(msg->data);

        msgs_ring_[topic_i] = {
            {"count", subs_samples_count_[topic_i]},
            {"seq", seq_num}
        };


        // TRACEPOINT
        lttng_ust_tracepoint(TRACEPOINT_PROVIDER, after_receiving, topic.c_str(), subs_samples_count_[topic_i], seq_num, conf_.id);

        RCLCPP_INFO(this->get_logger(), "[COUNT: %d][SEQ: %d] I heard: %ld bytes of 0's", subs_samples_count_[topic_i] + 1, seq_num, msg->data.size());

        subs_samples_count_[topic_i]++;

        received_all(subs_samples_count_[topic_i]);

        if (subs_samples_count_[topic_i] == samples_limit_) {
            // TRACEPOINT
            lttng_ust_tracepoint(TRACEPOINT_PROVIDER, received_all, topic.c_str(), seq_num, conf_.id);

            subs_finished_++;
            if (service.parameters.task_type == "conditional") {
                if (subs_finished_ == service_subs_) {
                    if (service_pubs_ == 0 || pubs_finished_ == service_pubs_) {
                        rclcpp::shutdown();
                    }
                } else {
                    subscribers_[topic_i].reset();
                }
            }
        }
    }

    // Stop subscriber if no msgs are received
    void stop_subscribing()
    {
        if (msgs_ring_.empty()) {
            rclcpp::shutdown();
        } else if (areMsgBuffersEqual(msgs_ring_, test_ring_)) {
            rclcpp::shutdown();
        }

        test_ring_ = cloneBuffer(msgs_ring_);
    }

    // Simulate msg processing
    void process_msg()
    {
        process_count_++;

        for (const auto& [topic_i, data] : msgs_ring_) {
            std::string topic = get_topic_only(topic_i);
            int seq_num = 0;
            int msg_count = 0;

            auto seq_it = data.find("seq");
            if (seq_it != data.end()) {
                seq_num = seq_it->second;
            } else {
                continue;
            }
            auto count_it = data.find("count");
            if (count_it != data.end()) {
                msg_count = count_it->second;
            } else {
                continue;
            }

            // TRACEPOINT
            if (conf_.subscribers >= conf_.publishers) {
                lttng_ust_tracepoint(TRACEPOINT_PROVIDER, periodic_processing, process_count_, topic.c_str(), msg_count, seq_num, conf_.id);
            } else {
                lttng_ust_tracepoint(TRACEPOINT_PROVIDER, periodic_processing, process_count_, topic_i.c_str(), msg_count, seq_num, conf_.id);
            }

            if (subs_samples_count_[topic_i] == samples_limit_) {
                if (subs_finished_ == service_subs_) {
                    if (service_pubs_ == 0 || pubs_finished_ == service_pubs_) {
                        rclcpp::shutdown();
                    }
                } else {
                    subscribers_[topic_i].reset();
                }
            }
        }
    }

    /// @brief Check if all messages we currently have, have the same 
    /// sample count, to simulate a conditional task, that gets fired
    /// if we have received all msgs
    /// @param curr_count current msg count of received message
    /// @return 
    bool received_all(int curr_count)
    {
        // Iterate over all msgs and check if all msgs have 
        // the same count, to check if we received them all
        for (const auto& [topic_i, data] : msgs_ring_) {
            std::string topic = get_topic_only(topic_i);
            int seq_num = 0;
            int msg_count = 0;

            auto seq_it = data.find("seq");
            if (seq_it != data.end()) {
                seq_num = seq_it->second;
            } else {
                continue;
            }
            auto count_it = data.find("count");
            if (count_it != data.end()) {
                msg_count = count_it->second;
            } else {
                continue;
            }

            
            if (subs_samples_count_[topic_i] < curr_count) {
                return false;
            }
        }

        // Iterate over all msgs to capture the timings
        for (const auto& [topic_i, data] : msgs_ring_) {
            std::string topic = get_topic_only(topic_i);
            int seq_num = 0;

            auto seq_it = data.find("seq");
            if (seq_it != data.end()) {
                seq_num = seq_it->second;
            } else {
                continue;
            }

            lttng_ust_tracepoint(TRACEPOINT_PROVIDER, after_receiving_cond, topic.c_str(), subs_samples_count_[topic_i], seq_num, conf_.id);
        }

        return true;
    }

    std::map<std::string, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr> publishers_;
    std::map<std::string, rclcpp::Subscription<std_msgs::msg::String>::SharedPtr> subscribers_;
    std::map<std::string, rclcpp::TimerBase::SharedPtr> pub_timers_;
    std::map<std::string, rclcpp::TimerBase::SharedPtr> sub_timers_;

    rclcpp::TimerBase::SharedPtr sub_kill_timer_;

    rclcpp::TimerBase::SharedPtr pub_glob_timer_;
    rclcpp::TimerBase::SharedPtr sub_glob_timer_;

    MachineConfig conf_;
    
    size_t count_;
    std::map<std::string, int> subs_samples_count_;
    std::map<std::string, int> pubs_samples_count_;
    int samples_limit_;
    int subs_finished_;
    int pubs_finished_;

    int service_pubs_;
    int service_subs_;

    rclcpp::TimerBase::SharedPtr proc_timer_;
    // Message buffer to put the current msg sample ids in
    // {topic_id: {"count": 0, "seq": 0}}
    msg_ring_t msgs_ring_;
    int process_count_;
    // This is to check if msgs_ring_ has changed after x time
    msg_ring_t test_ring_;
};

// -------------------------------------------------------------------------------------

int main(int argc, char *argv[])
{
    argparse::ArgumentParser program("soatracer-ros2");

    // program.add_argument("-t", "--type")
    //     // .required()
    //     .help("type of node, publisher or subscriber (pub, sub)");

    program.add_argument("-t", "--time")
        .help("time to start the program at");
    
    program.add_argument("-l", "--log")
        .flag()
        .help("print out the log of subs and pubs");
    
    program.add_argument("-s", "--samples")
        .required()
        .help("how many samples")
        .scan<'i', int>();

    auto &group = program.add_mutually_exclusive_group(true);
    group.add_argument("-f", "--file")
        .help("add config file for machine setup");

    group.add_argument("-c", "--config")
        .help("add config for machine setup (in json inside of '')");


    try
    {
        // DEBUG
        // for (int i = 0; i < argc; i++) {
        //     std::cout << argv[i] << std::endl;
        // }
        program.parse_args(argc, argv);
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        std::cerr << program;
        std::exit(1);
    }

    // -----------------------------

    bool log_flag = false;
    if (program["--log"] == true) {
        log_flag = true;
    }

    std::string config_file, config_arg;
    int samples;
    samples = program.get<int>("samples");

    if (program.is_used("config"))
    {
        config_arg = program.get<std::string>("config");
    }
    else if (program.is_used("file"))
    {
        config_file = program.get<std::string>("file");
    }
    else
    {
        return 1;
    }
    
    MachineConfig conf;
    if (config_file != "")
    {
        conf = read_config_file(config_file);
    }
    else if (config_arg != "")
    {
        json data = json::parse(config_arg);
        conf = read_config(data);
    }
    else
    {
        return 1;
    }

    ServiceConfig service = conf.services[0];
    std::cout << service.name << std::endl;

    if (program.is_used("time"))
    {
        std::string time_to_start = program.get<std::string>("time");

        auto time_now = std::chrono::system_clock::now();
        auto time_now_t = std::chrono::system_clock::to_time_t(time_now);
        auto tm = std::localtime(&time_now_t);
        std::cout << "time now: " << std::put_time(tm, "%H:%M:%S") << " start time: " << time_to_start << std::endl;

        // Wait until the given time is reached
        auto [hours, mins, secs] = parse_time(time_to_start);
        wait_until(hours, mins, secs);
    }

    // -----------------------------
    // INIT ROS2
    rclcpp::init(argc, argv);

    auto node = std::make_shared<SOANode>("soatracer_ros2_node", conf, samples);
    rclcpp::spin(node);

    rclcpp::shutdown();
    return 0;
}
