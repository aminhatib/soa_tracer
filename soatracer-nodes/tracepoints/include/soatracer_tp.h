#undef TRACEPOINT_PROVIDER
#define TRACEPOINT_PROVIDER soatracer_trace_provider

#undef LTTNG_UST_TRACEPOINT_INCLUDE
#define LTTNG_UST_TRACEPOINT_INCLUDE "./soatracer_tp.h"
#if !defined(_SOATRACER_TP_H) || defined(LTTNG_UST_TRACEPOINT_HEADER_MULTI_READ)
#define _SOATRACER_TP_H

#include <lttng/tracepoint.h>


// pre publish
LTTNG_UST_TRACEPOINT_EVENT(
    TRACEPOINT_PROVIDER,
    pre_publish,
    LTTNG_UST_TP_ARGS(
        const char *, topic_name,
        int, msg_id,
        int, test_id
    ),
    LTTNG_UST_TP_FIELDS(
        lttng_ust_field_string(service_name, topic_name)
        lttng_ust_field_integer(int, msg_id, msg_id)
        lttng_ust_field_integer(int, test_id, test_id)
    )
)

// after publish
LTTNG_UST_TRACEPOINT_EVENT(
    TRACEPOINT_PROVIDER,
    after_publish,
    LTTNG_UST_TP_ARGS(
        const char *, topic_name,
        int, msg_id,
        int, test_id
    ),
    LTTNG_UST_TP_FIELDS(
        lttng_ust_field_string(service_name, topic_name)
        lttng_ust_field_integer(int, msg_id, msg_id)
        lttng_ust_field_integer(int, test_id, test_id)
    )
)

// published all
LTTNG_UST_TRACEPOINT_EVENT(
    TRACEPOINT_PROVIDER,
    published_all,
    LTTNG_UST_TP_ARGS(
        const char *, topic_name,
        int, msg_id,
        int, test_id
    ),
    LTTNG_UST_TP_FIELDS(
        lttng_ust_field_string(service_name, topic_name)
        lttng_ust_field_integer(int, msg_id, msg_id)
        lttng_ust_field_integer(int, test_id, test_id)
    )
)

// -------------------------------------------------------------------------------------

// init subscriber process simulate timer
LTTNG_UST_TRACEPOINT_EVENT(
    TRACEPOINT_PROVIDER,
    init_proc_timer,
    LTTNG_UST_TP_ARGS(
        const char *, topic_name,
        int, test_id
    ),
    LTTNG_UST_TP_FIELDS(
        lttng_ust_field_string(service_name, topic_name)
        lttng_ust_field_integer(int, test_id, test_id)
    )
)

// current msg processed
LTTNG_UST_TRACEPOINT_EVENT(
    TRACEPOINT_PROVIDER,
    curr_msg_proc,
    LTTNG_UST_TP_ARGS(
        const char *, topic_name,
        int, msg_id,
        int, process_id,
        int, test_id
    ),
    LTTNG_UST_TP_FIELDS(
        lttng_ust_field_string(service_name, topic_name)
        lttng_ust_field_integer(int, msg_id, msg_id)
        lttng_ust_field_integer(int, process_id, process_id)
        lttng_ust_field_integer(int, test_id, test_id)
    )
)

// pre subscribing
LTTNG_UST_TRACEPOINT_EVENT(
    TRACEPOINT_PROVIDER,
    pre_subscribing,
    LTTNG_UST_TP_ARGS(
        const char *, topic_name,
        int, test_id
    ),
    LTTNG_UST_TP_FIELDS(
        lttng_ust_field_string(service_name, topic_name)
        lttng_ust_field_integer(int, test_id, test_id)
    )
)

// after receiving
LTTNG_UST_TRACEPOINT_EVENT(
    TRACEPOINT_PROVIDER,
    after_receiving,
    LTTNG_UST_TP_ARGS(
        const char *, topic_name,
        int, msg_count,
        int, msg_id,
        int, test_id
    ),
    LTTNG_UST_TP_FIELDS(
        lttng_ust_field_string(service_name, topic_name)
        lttng_ust_field_integer(int, msg_count, msg_count)
        lttng_ust_field_integer(int, msg_id, msg_id)
        lttng_ust_field_integer(int, test_id, test_id)
    )
)

// after receiving all expected data
LTTNG_UST_TRACEPOINT_EVENT(
    TRACEPOINT_PROVIDER,
    after_receiving_cond,
    LTTNG_UST_TP_ARGS(
        const char *, topic_name,
        int, msg_count,
        int, msg_id,
        int, test_id
    ),
    LTTNG_UST_TP_FIELDS(
        lttng_ust_field_string(service_name, topic_name)
        lttng_ust_field_integer(int, msg_count, msg_count)
        lttng_ust_field_integer(int, msg_id, msg_id)
        lttng_ust_field_integer(int, test_id, test_id)
    )
)

// periodic processing
LTTNG_UST_TRACEPOINT_EVENT(
    TRACEPOINT_PROVIDER,
    periodic_processing,
    LTTNG_UST_TP_ARGS(
        int, process_id,
        const char *, topic_name,
        int, msg_count,
        int, msg_id,
        int, test_id
    ),
    LTTNG_UST_TP_FIELDS(
        lttng_ust_field_string(service_name, topic_name)
        lttng_ust_field_integer(int, process_id, process_id)
        lttng_ust_field_integer(int, msg_count, msg_count)
        lttng_ust_field_integer(int, msg_id, msg_id)
        lttng_ust_field_integer(int, test_id, test_id)
    )
)

// received all
LTTNG_UST_TRACEPOINT_EVENT(
    TRACEPOINT_PROVIDER,
    received_all,
    LTTNG_UST_TP_ARGS(
        const char *, topic_name,
        int, msg_id,
        int, test_id
    ),
    LTTNG_UST_TP_FIELDS(
        lttng_ust_field_string(service_name, topic_name)
        lttng_ust_field_integer(int, msg_id, msg_id)
        lttng_ust_field_integer(int, test_id, test_id)
    )
)

#endif /* _SOATRACER_TP_H */

#include <lttng/tracepoint-event.h>