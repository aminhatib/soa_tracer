#include <iostream>
#include <nlohmann/json.hpp>
#include <argparse/argparse.hpp>

#include "common.hpp"

int main(int argc, char *argv[]) {
    argparse::ArgumentParser program("soatracer-nodes");

    program.add_argument("-c", "--config")
        .required()
        .help("add config for machine setup (in json inside of '')");
    
    try
    {
        program.parse_args(argc, argv);
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        std::cerr << program;
        std::exit(1);
    }
    
    // Read from args
    std::string config_arg = program.get<std::string>("config");
    json arg_data = json::parse(config_arg);
    auto arg_conf = read_config(arg_data);
    std::cout << arg_conf.services[0].type << std::endl;



    // Read from file:
    std::string config_file = "tests/machine_config.json";

    auto conf = read_config_file(config_file);

    std::cout << conf.services[0].name << std::endl;

    return 0;
}
