# soatracer

## Installing
### Requirements
1. Install `poetry`:
    
    -  using the official installer
        ```bash
        curl -sSL https://install.python-poetry.org | python3 -
        ```
    - or using [pipx](https://github.com/pypa/pipx)
        ```bash
        pipx install poetry
        ```
2. Install dependencies (in repo directory):
    ```bash
    poetry install
    ```

## Running

1. First activate the virtual environment with:
    ```bash
    poetry shell
    ```
2. Now you can simply run the app using:
    ```bash
    soatracer
    ```
    or
    ```bash
    python soatracer/app.py
    ```

## Debugging

**Note**: you need to have the virtual environment activated to be able to use the commands below.

Open two terminals:

1. Debug console:
```bash
# Anywhere
textual console
```

2. Actual app:
```bash
# In repository
textual run --dev ./soatracer/app.py
```

# soatracer-nodes
The `soatracer-nodes` are the architecture specific executables that run the service(s), that will be used to pass the configuration parameters to.

## Requirements

These dependencies must be installed:
- [CMake](https://cmake.org/download/)

## Building and Running
Simply use the `build.py` tool from the repo directory, which handles the `CMakeLists.txt` file.

The build and exectuable files will be generated in the directory based on the OS used: `./build/<linux|windows>`.

1. To generate build files and then build:
```bash
python build.py
```

2. To run the project in addition:
```bash
python build.py -r
```

3. To pass arguments to the executable when running:
```bash
python build.py -r -- -h
```

4. For more options use: 
```bash
python build.py -h
```