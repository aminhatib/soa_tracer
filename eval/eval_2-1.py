from eval.analyse_results import *


for time_type in time_types:
    print(get_test_ids(topology="2-1", arch="asoa", dist=True, rel="reliable"))
    print(get_test_ids(topology="1-2", arch="asoa", dist=True, rel="reliable"))
    asoa_times_ms = get_times_dict(get_test_ids(topology="1-2", arch="asoa", dist=True, rel="reliable"), "ms", "imu")[time_type]
    ros2_times_ms = get_times_dict(get_test_ids(topology="1-2", arch="ros2", dist=True, rel="reliable"), "ms", "imu")[time_type]
    print(len(asoa_times_ms))
    print(len(ros2_times_ms))
    asoa_times_ms = get_times_dict(get_test_ids(topology="2-1", arch="asoa", dist=True, rel="reliable"), "ms", "imu")[time_type]
    ros2_times_ms = get_times_dict(get_test_ids(topology="2-1", arch="ros2", dist=True, rel="reliable"), "ms", "imu")[time_type]
    print(len(asoa_times_ms))
    print(len(ros2_times_ms))

    asoa_times_ms, ros2_times_ms = trim_list(asoa_times_ms, ros2_times_ms)

    max_samples = max(len(asoa_times_ms), len(ros2_times_ms))
    x = np.linspace(1, max_samples, num=max_samples)
    fig = plt.figure()
    ax = plt.axes()
    ax.margins(x = 0.005, y = 0.005)
    plt.scatter(x, asoa_times_ms, s=2, color='blue', zorder = 10, label="ASOA")
    plt.scatter(x, ros2_times_ms, s=2, color='red', zorder = 10, label="ROS2")
    plt.plot(x, [min(asoa_times_ms) for a in asoa_times_ms], "--", label=f"Max: {max(asoa_times_ms)} [ms] {asoa_times_ms.index(max(asoa_times_ms))}",color='blue')
    plt.plot(x, [max(asoa_times_ms) for a in asoa_times_ms], "--", label=f"Min: {min(asoa_times_ms)} [ms] {asoa_times_ms.index(min(asoa_times_ms))}",color='blue')

    plt.plot(x, [min(ros2_times_ms) for a in ros2_times_ms], "--", label=f"Max: {max(ros2_times_ms)} [ms] {ros2_times_ms.index(max(ros2_times_ms))}",color='red')
    plt.plot(x, [max(ros2_times_ms) for a in ros2_times_ms], "--", label=f"Min: {min(ros2_times_ms)} [ms] {ros2_times_ms.index(min(ros2_times_ms))}",color='red')

    plt.legend(loc='upper right')
    ax.set_ylabel('Latency [ms]')
    ax.set_xlabel('Sample')
    ax.set_title(f'ASOA - ROS2 ({time_type}) runtimes [2-1, dist, reliable]')
    plt.savefig(f"{path}/figures/arch_reliable_2-1_dist_{time_type}_ms.png", dpi = 300)
    plt.savefig(f"{path}/figures/arch_reliable_2-1_dist_{time_type}_ms.pdf")

    plt.clf()
    plt.cla()
    plt.close()