from eval.analyse_results import *

# best effort vs reliable
for arch in archs:
    compare_best_effort = arch_reliability_imu_tests[arch]["best_effort"]
    compare_reliable = arch_reliability_imu_tests[arch]["reliable"]

    for time_type in time_types:
        best_effort_times_ms = compare_best_effort[time_type]
        reliable_times_ms = compare_reliable[time_type]

        best_effort_times_ms, reliable_times_ms = trim_list(best_effort_times_ms, reliable_times_ms)

        fig = plt.figure()
        ax = plt.axes()
        ax.boxplot([reliable_times_ms, best_effort_times_ms], labels=["Reliable", "Best Effort"], patch_artist=True)

        ax.set_ylabel('Latency [ms]')
        ax.set_title(f'{arch.upper()} Reliability ({time_type}) runtimes [1-1, dist]')
        plt.savefig(f"{path}/figures/{arch}_BOX_reliability_1-1_dist_{time_type}_ms.png", dpi = 300)
        plt.savefig(f"{path}/figures/{arch}_BOX_reliability_1-1_dist_{time_type}_ms.pdf", dpi = 300)

        plt.clf()
        plt.cla()
        plt.close()


for time_type in time_types:
    asoa_11_times_ms = get_times_dict(get_test_ids(topology="1-1", arch="asoa", dist=True, rel="reliable"), "ms", "imu")[time_type]
    asoa_12_times_ms = get_times_dict(get_test_ids(topology="1-2", arch="asoa", dist=True, rel="reliable"), "ms", "imu")[time_type]
    asoa_21_times_ms = get_times_dict(get_test_ids(topology="2-1", arch="asoa", dist=True, rel="reliable"), "ms", "imu")[time_type]

    fig = plt.figure()
    ax = plt.axes()

    ax.boxplot([asoa_11_times_ms, asoa_12_times_ms, asoa_21_times_ms], labels=["1-1", "1-2", "2-1"], patch_artist=True)

    ax.set_ylabel('Latency [ms]')
    ax.set_title(f'ASOA ({time_type}) runtimes [dist, reliable]')
    plt.savefig(f"{path}/figures/asoa_BOX_topology_reliable_dist_{time_type}_ms.png", dpi = 300)
    plt.savefig(f"{path}/figures/asoa_BOX_topology_reliable_dist_{time_type}_ms.pdf", dpi = 300)


    plt.clf()
    plt.cla()
    plt.close()

for time_type in time_types:
    ros2_11_times_ms = get_times_dict(get_test_ids(topology="1-1", arch="ros2", dist=True, rel="reliable"), "ms", "imu")[time_type]
    ros2_12_times_ms = get_times_dict(get_test_ids(topology="1-2", arch="ros2", dist=True, rel="reliable"), "ms", "imu")[time_type]
    ros2_21_times_ms = get_times_dict(get_test_ids(topology="2-1", arch="ros2", dist=True, rel="reliable"), "ms", "imu")[time_type]

    fig = plt.figure()
    ax = plt.axes()

    ax.boxplot([ros2_11_times_ms, ros2_12_times_ms, ros2_21_times_ms], labels=["1-1", "1-2", "2-1"], patch_artist=True)

    ax.set_ylabel('Latency [ms]')
    ax.set_title(f'ROS2 ({time_type}) runtimes [dist, reliable]')
    plt.savefig(f"{path}/figures/ros2_BOX_topology_reliable_dist_{time_type}_ms.png", dpi = 300)
    plt.savefig(f"{path}/figures/ros2_BOX_topology_reliable_dist_{time_type}_ms.pdf", dpi = 300)


    plt.clf()
    plt.cla()
    plt.close()



###################################################
# Compare distr and centralized
###################################################
for time_type in time_types:
    asoa_dist_times_ms = get_times_dict(get_test_ids(topology="1-1", arch="asoa", dist=True, rel="reliable"), "ms", "imu")[time_type]
    asoa_cent_times_ms = get_times_dict(get_test_ids(topology="1-1", arch="asoa", dist=False, rel="reliable"), "ms", "imu")[time_type]

    asoa_dist_times_ms = remove_outliers(asoa_dist_times_ms)
    asoa_cent_times_ms = remove_outliers(asoa_cent_times_ms)

    fig = plt.figure()
    ax = plt.axes()

    ax.boxplot([asoa_dist_times_ms, asoa_cent_times_ms], labels=["Distributed", "Centralized"], patch_artist=True)

    ax.set_ylabel('Latency [ms]')
    ax.set_title(f'ASOA ({time_type}) runtimes [1-1, reliable]')
    plt.savefig(f"{path}/figures/asoa_BOX_dist-cent_reliable_1-1_{time_type}_ms.png", dpi = 300)
    plt.savefig(f"{path}/figures/asoa_BOX_dist-cent_reliable_1-1_{time_type}_ms.pdf", dpi = 300)

    plt.clf()
    plt.cla()
    plt.close()

for time_type in time_types:
    ros2_dist_times_ms = get_times_dict(get_test_ids(topology="1-1", arch="ros2", dist=True, rel="reliable"), "ms", "imu")[time_type]
    ros2_cent_times_ms = get_times_dict(get_test_ids(topology="1-1", arch="ros2", dist=False, rel="reliable"), "ms", "imu")[time_type]

    ros2_dist_times_ms = remove_outliers(ros2_dist_times_ms)
    ros2_cent_times_ms = remove_outliers(ros2_cent_times_ms)

    fig = plt.figure()
    ax = plt.axes()

    ax.boxplot([ros2_dist_times_ms, ros2_cent_times_ms], labels=["Distributed", "Centralized"], patch_artist=True)

    ax.set_ylabel('Latency [ms]')
    ax.set_title(f'ROS2 ({time_type}) runtimes [1-1, reliable]')
    plt.savefig(f"{path}/figures/ros2_BOX_dist-cent_reliable_1-1_{time_type}_ms.png", dpi = 300)
    plt.savefig(f"{path}/figures/ros2_BOX_dist-cent_reliable_1-1_{time_type}_ms.pdf", dpi = 300)


    plt.clf()
    plt.cla()
    plt.close()



###################################################
# Compare inter and intra
###################################################
for time_type in time_types:
    asoa_inter_times_ms = get_times_dict(get_test_ids(topology="1-1", arch="asoa", dist=False, comm_type="inter", rel="reliable"), "ms", "imu")[time_type]
    asoa_intra_times_ms = get_times_dict(get_test_ids(topology="1-1", arch="asoa", dist=False, comm_type="intra", rel="reliable"), "ms", "imu")[time_type]

    asoa_inter_times_ms = remove_outliers(asoa_inter_times_ms)
    asoa_intra_times_ms = remove_outliers(asoa_intra_times_ms)

    fig = plt.figure()
    ax = plt.axes()

    ax.boxplot([asoa_inter_times_ms, asoa_intra_times_ms], labels=["Inter-Process", "Intra-Process"], patch_artist=True)

    ax.set_ylabel('Latency [ms]')
    ax.set_title(f'ASOA ({time_type}) runtimes [1-1, cent, reliable]')
    plt.savefig(f"{path}/figures/asoa_BOX_inter-intra_reliable_1-1_{time_type}_ms.png", dpi = 300)
    plt.savefig(f"{path}/figures/asoa_BOX_inter-intra_reliable_1-1_{time_type}_ms.pdf", dpi = 300)

    plt.clf()
    plt.cla()
    plt.close()

for time_type in time_types:
    ros2_inter_times_ms = get_times_dict(get_test_ids(topology="1-1", arch="ros2", dist=False, comm_type="inter", rel="reliable"), "ms", "imu")[time_type]
    ros2_intra_times_ms = get_times_dict(get_test_ids(topology="1-1", arch="ros2", dist=False, comm_type="intra", rel="reliable"), "ms", "imu")[time_type]

    ros2_inter_times_ms = remove_outliers(ros2_inter_times_ms)
    ros2_intra_times_ms = remove_outliers(ros2_intra_times_ms)

    fig = plt.figure()
    ax = plt.axes()

    ax.boxplot([ros2_inter_times_ms, ros2_intra_times_ms], labels=["Inter-Process", "Intra-Process"], patch_artist=True)

    ax.set_ylabel('Latency [ms]')
    ax.set_title(f'ROS2 ({time_type}) runtimes [1-1, cent, reliable]')
    plt.savefig(f"{path}/figures/ros2_BOX_inter-intra_reliable_1-1_{time_type}_ms.png", dpi = 300)
    plt.savefig(f"{path}/figures/ros2_BOX_inter-intra_reliable_1-1_{time_type}_ms.pdf", dpi = 300)


    plt.clf()
    plt.cla()
    plt.close()



###################################################
# Compare data types
###################################################
for time_type in time_types:
    asoa_imu_times_ms = get_times_dict(get_test_ids(topology="1-1", arch="asoa", dist=False, comm_type="intra", rel="reliable"), "ms", "imu")[time_type]
    asoa_radar_times_ms = get_times_dict(get_test_ids(topology="1-1", arch="asoa", dist=False, comm_type="intra", rel="reliable"), "ms", "radar")[time_type]
    asoa_camera_times_ms = get_times_dict(get_test_ids(topology="1-1", arch="asoa", dist=False, comm_type="intra", rel="reliable"), "ms", "camera")[time_type]
    asoa_lidar_times_ms = get_times_dict(get_test_ids(topology="1-1", arch="asoa", dist=False, comm_type="intra", rel="reliable"), "ms", "lidar")[time_type]

    asoa_imu_times_ms = remove_outliers(asoa_imu_times_ms)
    asoa_radar_times_ms = remove_outliers(asoa_radar_times_ms)
    asoa_camera_times_ms = remove_outliers(asoa_camera_times_ms)
    asoa_lidar_times_ms = remove_outliers(asoa_lidar_times_ms)

    fig = plt.figure()
    ax = plt.axes()

    ax.boxplot([asoa_imu_times_ms, asoa_radar_times_ms, asoa_camera_times_ms, asoa_lidar_times_ms], labels=["IMU", "Radar", "Camera", "Lidar"], patch_artist=True)

    ax.set_ylabel('Latency [ms]')
    ax.set_title(f'ASOA ({time_type}) runtimes [1-1, cent, intra, reliable]')
    plt.savefig(f"{path}/figures/asoa_BOX_data-types_reliable_1-1_cent_intra_{time_type}_ms.png", dpi = 300)
    plt.savefig(f"{path}/figures/asoa_BOX_data-types_reliable_1-1_cent_intra_{time_type}_ms.pdf", dpi = 300)

    plt.clf()
    plt.cla()
    plt.close()

for time_type in time_types:
    ros2_imu_times_ms = get_times_dict(get_test_ids(topology="1-1", arch="ros2", dist=False, comm_type="intra", rel="reliable"), "ms", "imu")[time_type]
    ros2_radar_times_ms = get_times_dict(get_test_ids(topology="1-1", arch="ros2", dist=False, comm_type="intra", rel="reliable"), "ms", "radar")[time_type]
    ros2_camera_times_ms = get_times_dict(get_test_ids(topology="1-1", arch="ros2", dist=False, comm_type="intra", rel="reliable"), "ms", "camera")[time_type]
    ros2_lidar_times_ms = get_times_dict(get_test_ids(topology="1-1", arch="ros2", dist=False, comm_type="intra", rel="reliable"), "ms", "lidar")[time_type]

    ros2_imu_times_ms = remove_outliers(ros2_imu_times_ms)
    ros2_radar_times_ms = remove_outliers(ros2_radar_times_ms)
    ros2_camera_times_ms = remove_outliers(ros2_camera_times_ms)
    ros2_lidar_times_ms = remove_outliers(ros2_lidar_times_ms)

    fig = plt.figure()
    ax = plt.axes()

    ax.boxplot([ros2_imu_times_ms, ros2_radar_times_ms, ros2_camera_times_ms, ros2_lidar_times_ms], labels=["IMU", "Radar", "Camera", "Lidar"], patch_artist=True)

    ax.set_ylabel('Latency [ms]')
    ax.set_title(f'ROS2 ({time_type}) runtimes [1-1, cent, intra, reliable]')
    plt.savefig(f"{path}/figures/ros2_BOX_data-types_reliable_1-1_cent_intra_{time_type}_ms.png", dpi = 300)
    plt.savefig(f"{path}/figures/ros2_BOX_data-types_reliable_1-1_cent_intra_{time_type}_ms.pdf", dpi = 300)


    plt.clf()
    plt.cla()
    plt.close()