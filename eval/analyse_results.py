import sys
import datetime
import os
import statistics

import pandas
from pandas import DataFrame
import matplotlib
from matplotlib.backends.backend_pgf import FigureCanvasPgf
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import numpy as np

matplotlib.use("pgf")
matplotlib.backend_bases.register_backend('pdf', FigureCanvasPgf)

plt.rc("font", size=14)

# from rich.pretty import pprint

# Add the system-wide Python library directory to the sys.path list
# system_site_packages = "/usr/lib/python3/dist-packages"
# sys.path.insert(0, system_site_packages)

import bt2

table_file = "generated_tests.xlsx"
archs = ["asoa", "ros2"]
time_types = ["comm", "idle", "end"]

def get_tests_df(path: str = None):
    """Return dataframe of the excel file of the table"""
    if path:
        path = find_table_file(path)
    if not path:
        path = table_file

    print(f"[INFO]: Used table: {path}")
    data = pandas.read_excel(path)
    df = pandas.DataFrame(data)
    return df

def find_table_file(dir_path):
    for root, _, files in os.walk(dir_path):
        for file in files:
            if file.endswith('.xlsx'):
                return os.path.join(root, file)
    return None

def get_time_list(id_ls: list, time_type: str, data_type: str = None, time_unit: str = None):
    """Get list of times depending on id list given. 
    `msg_type` can be provided for a specific msg type.
    `time_type` must be one of `["comm", "idle", "end"]`"""
    if time_type not in time_types:
        return None
    
    result = []

    for test_id in id_ls:
        if test_id not in test_id_times:
            continue

        if data_type is None:
            for msg_type in test_id_times[test_id]:
                result.extend(test_id_times[test_id][msg_type][time_type])
        else:
            if data_type not in test_id_times[test_id]:
                continue

            result.extend(test_id_times[test_id][data_type][time_type])
            # print("test_id: ", test_id)
    
    if time_unit == "ms":
        result = [t / 1e6 for t in result]
    elif time_unit == "us":
        result = [t / 1000 for t in result]
    
    # Get only positives
    result = [t for t in result if t >= 0]
    
    return result

def get_test_ids(topology: str = None, dist: bool = None, arch: str = None, comm_type: str = None, rel: str = None):
    res = []

    if topology:
        res.append(df.loc[df["topology"] == topology]["id"].to_list())
    if dist:
        res.append(df.loc[df["distributed"] == dist]["id"].to_list())
    if comm_type:
        res.append(df.loc[df["comm_type"] == comm_type]["id"].to_list())
    if arch:
        res.append(df.loc[df["architecture"] == arch]["id"].to_list())
    if rel:
        res.append(df.loc[df["reliability"] == rel]["id"].to_list())

    return list(set.intersection(*[set(ls) for ls in res]))

def get_times_dict(test_id_ls: str, time_unit: str = None, data_type: str = None):
    return {
        "comm": get_time_list(test_id_ls, "comm", time_unit=time_unit, data_type=data_type),
        "idle": get_time_list(test_id_ls, "idle", time_unit=time_unit, data_type=data_type),
        "end": get_time_list(test_id_ls, "end", time_unit=time_unit, data_type=data_type),
    }

def make_figure(d: dict, title: str = ""):
    for time_type in d:
        msg_times_ms = d[time_type]
        x = np.linspace(1, len(msg_times_ms), num=len(msg_times_ms))
        fig = plt.figure()
        ax = plt.axes()
        ax.margins(x = 0.005, y = 0.005)
        plt.scatter(x, msg_times_ms, s=2, color='blue', zorder = 10)
        plt.plot(x, [min(msg_times_ms) for a in msg_times_ms], "--", label=f"Max: {max(msg_times_ms)} [ms] {msg_times_ms.index(max(msg_times_ms))}",color='blue')
        plt.plot(x, [max(msg_times_ms) for a in msg_times_ms], "--", label=f"Min: {min(msg_times_ms)} [ms] {msg_times_ms.index(min(msg_times_ms))}",color='blue')

        plt.legend(loc='upper right')
        ax.set_ylabel('Latency [ms]')
        ax.set_xlabel('Sample')
        # ax.set_title(f'Runtime with N:{ 80* 30000} {is_rt_mes}')
        plt.savefig(f"{path}/figures/{title}_msg_{time_type}_ms.png", dpi = 300)

def trim_list(l1, l2):
    """Make lists have the same number of data"""
    if len(l1) > len(l2):
        l1 = l1[:len(l2)]
    elif len(l1) < len(l2):
        l2 = l2[:len(l1)]
    return l1, l2

def remove_outliers(numbers):
    median = statistics.median(numbers)
    return [n for n in numbers if abs(n - median) <= 2 * statistics.median([abs(n - median) for n in numbers])]


###############################################################
# Start
###############################################################

print("..:: SOATracer Results Evaluation ::..")

# Create a trace collection message iterator from the first command-line
# argument.
path = sys.argv[1]
msg_it = bt2.TraceCollectionMessageIterator(path)

# Last event's time (ns from origin).
last_event_ns_from_origin = None

tracepoints = []

# Iterate the trace messages.
for msg in msg_it:
    # `bt2._EventMessageConst` is the Python type of an event message.
    if type(msg) is bt2._EventMessageConst:
        # Get event message's default clock snapshot's ns from origin
        # value.
        nanosec_from_origin = msg.default_clock_snapshot.ns_from_origin

        # Compute the time difference since the last event message.
        diff_s = 0

        if last_event_ns_from_origin is not None:
            diff_s = (nanosec_from_origin - last_event_ns_from_origin) / 1e9

        # Create a `datetime.datetime` object from `ns_from_origin` for
        # presentation. Note that such an object is less accurate than
        # `ns_from_origin` as it holds microseconds, not nanoseconds.
        dt = datetime.datetime.fromtimestamp(nanosec_from_origin / 1e9)

        # Print line.
        # fmt = '{} (+{:.6f} s): {} [service_name: {}, test_id: {}]'
        # print(fmt.format(dt, diff_s, msg.event.name, msg.event["service_name"], msg.event["test_id"]))

        # Update last event's time.
        last_event_ns_from_origin = nanosec_from_origin

        tracepoints.append({"name": msg.event.name, "nanosec": nanosec_from_origin, "msg": msg, "diff_s": diff_s})


tracepoints = sorted(tracepoints, key=lambda x: x["nanosec"])

print("[INFO]: Tracepoints read")

# print(len(tracepoints))
# exit(1)

# print(tracepoints[0]["nanosec"] / 1000)

# Use path to check if there is an excel file in the path specified
df: DataFrame = get_tests_df(path)
# Insert id column as a first column
df.insert(0, "id", df.index + 1)
# df["id"] = df.index + 1
print(f"[INFO]: Table size: {len(df)}")

asoa_tests = df.loc[df["architecture"] == "asoa"]
ros2_tests = df.loc[df["architecture"] == "ros2"]

# [test["id"] for test in asoa_tests]
asoa_test_ids = asoa_tests["id"].to_list()
# [test["id"] for test in ros2_tests]
ros2_test_ids = ros2_tests["id"].to_list()

periodic_tests = df.loc[df["task_type"] == "periodic"]
conditional_tests = df.loc[df["task_type"] == "conditional"]

ros2_periodic = pandas.merge(ros2_tests, periodic_tests)
ros2_conditional = pandas.merge(ros2_tests, conditional_tests)
asoa_periodic = pandas.merge(asoa_tests, periodic_tests)
asoa_conditional = pandas.merge(asoa_tests, conditional_tests)

###############################################################
# Extract times from tracepoints
###############################################################

# Example

# msg_times = {
#     "100":{
#         "imu1": {
#             "1": {
#                 "tracepoints": {
#                     "pre_publish": 120387123,
#                     "after_receiving": 120387123,
#                     "periodic_processing": 120387123,
#                 },
#             },
#             "2": {
#                 "tracepoints": {
#                     "pre_publish": 120387123,
#                     "after_receiving": 120387123,
#                     "periodic_processing": 120387123,
#                 }
#             }
#         },
#         "imu2": {
#             "1": {
#                 "tracepoints": {
#                     "pre_publish": 120387123,
#                     "after_receiving": 120387123,
#                     "periodic_processing": 120387123,
#                 }
#             },
#             "2": {
#                 "tracepoints": {
#                     "pre_publish": 120387123,
#                     "after_receiving": 120387123,
#                     "periodic_processing": 120387123,
#                 }
#             }
#         } 
#     }
# }

msg_times = {}

commtimes_ns = []
idletimes_ns = []
endtimes_ns = []

test_commtimes_ns = {}
test_idletimes_ns = {}

start_time_ns = 0

data_type_times = {}
receive_times = {}
idle_times = {}
comm_times = {}


for tp in tracepoints:
    event_name = tp["name"]
    name: str = tp["name"]
    msg: bt2._EventMessageConst = tp["msg"]

    test_id = msg.event["test_id"]
    # if test_id != curr_id:
    #     continue

    name = name.removeprefix("soatracer_trace_provider:")

    if name == "init_proc_timer":
        continue

    nanosec = tp["nanosec"]
    msg_id = msg.event["msg_id"]
    service_name = msg.event["service_name"]
    # Remove digits from service name
    service_name = ''.join([i for i in service_name if not i.isdigit()])

    if test_id not in msg_times:
        msg_times[test_id] = {}

    if service_name not in msg_times[test_id]:
        msg_times[test_id][service_name] = {}

    if msg_id not in msg_times[test_id][service_name]:
        msg_times[test_id][service_name][msg_id] = {}

    if "tracepoints" not in msg_times[test_id][service_name][msg_id]:
        msg_times[test_id][service_name][msg_id]["tracepoints"] = {}


    if test_id not in test_commtimes_ns:
        test_commtimes_ns[test_id] = []

    if test_id not in test_idletimes_ns:
        test_idletimes_ns[test_id] = []

    
    if name == "pre_publish":
        # data_type_time[service_name] = nanosec
        start_time_ns = nanosec

        msg_times[test_id][service_name][msg_id]["tracepoints"]["pre_publish"] = nanosec

    elif name == "periodic_processing":
        if msg_id not in receive_times[service_name]:
            continue
        idle_time = nanosec - receive_times[service_name][msg_id]

        if service_name not in idle_times:
            idle_times[service_name] = {}

        idle_times[service_name][msg_id] = idle_time
        
        # idle_time = nanosec - msg_times[test_id][service_name][msg_id]["tracepoints"]["after_receiving"]
        idletimes_ns.append(idle_time)

        msg_times[test_id][service_name][msg_id]["tracepoints"]["periodic_processing"] = nanosec

    elif name == "after_receiving":
        comm_time = nanosec - start_time_ns

        if service_name not in receive_times:
            receive_times[service_name] = {}
        
        if service_name not in comm_times:
            comm_times[service_name] = {}
        
        comm_times[service_name][msg_id] = comm_time
            
        receive_times[service_name][msg_id] = nanosec

        # comm_time = nanosec - data_type_time[service_name]
        commtimes_ns.append(comm_time)

        msg_times[test_id][service_name][msg_id]["tracepoints"]["after_receiving"] = nanosec
    
    if service_name in idle_times and service_name in comm_times:
        if msg_id in idle_times[service_name] and msg_id in comm_times[service_name]:
            end_to_end = idle_times[service_name][msg_id] + comm_times[service_name][msg_id]
            endtimes_ns.append(end_to_end)

    # elif name == "received_all":
    #     pass
    # elif name == "after_publish":
    #     pass
    # elif name == "published_all":
    #     pass
    # elif name == "init_proc_timer":
    #     pass
    # elif name == "curr_msg_proc":
    #     pass

# for m in msg_times.values():

#     print(m)
# print(commtimes_ns)
# print(idletimes_ns)
# print(endtimes_ns)


# Get microseconds
commtimes_us = [c / 1000 for c in commtimes_ns]
idletimes_us = [c / 1000 for c in idletimes_ns]
endtimes_us = [c / 1000 for c in endtimes_ns]


# For each msg type we create a list of comm/idle/end2end -times
msg_type_times_ns = {}
msg_type_idletimes_ns = {}
msg_type_endtimes_ns = {}

test_id_times = {}
"""```py
test_id: {
    data_type: {
        "comm": [],
        "idle": [],
        "end": [],
    }
}
```"""

# For each test id, and each service name (data type)
# we iterate over all found msgs and their tracepoints
# and calculate the comm, idle and sum of them (end to end)
for test_id in msg_times:
    if test_id in ros2_test_ids:
        arch = "ros2"
    elif test_id in asoa_test_ids:
        arch = "asoa"

    for service_name in msg_times[test_id]:
        service_name: str = service_name
        # Get service name without id
        data_name = ''.join([i for i in service_name if not i.isdigit()])

        for msg_id in msg_times[test_id][service_name]:
            if "after_receiving" in msg_times[test_id][service_name][msg_id]["tracepoints"] and "pre_publish" in msg_times[test_id][service_name][msg_id]["tracepoints"] and "periodic_processing" in msg_times[test_id][service_name][msg_id]["tracepoints"]:
                if data_name not in msg_type_times_ns:
                    msg_type_times_ns[data_name] = []

                if data_name not in msg_type_idletimes_ns:
                    msg_type_idletimes_ns[data_name] = []

                if data_name not in msg_type_endtimes_ns:
                    msg_type_endtimes_ns[data_name] = []


                after_receiving = msg_times[test_id][service_name][msg_id]["tracepoints"]["after_receiving"]
                pre_publish = msg_times[test_id][service_name][msg_id]["tracepoints"]["pre_publish"]
                curr_commtime = after_receiving - pre_publish

                msg_type_times_ns[data_name].append(curr_commtime)

                periodic_processing = msg_times[test_id][service_name][msg_id]["tracepoints"]["periodic_processing"]
                curr_idletime = periodic_processing - after_receiving

                msg_type_idletimes_ns[data_name].append(curr_idletime)
                msg_type_endtimes_ns[data_name].append(curr_commtime + curr_idletime)

                if test_id not in test_id_times:
                    test_id_times[test_id] = {}
                
                if data_name not in test_id_times[test_id]:
                    test_id_times[test_id][data_name] = {"comm": [], "idle": [], "end": []}

                test_id_times[test_id][data_name]["comm"].append(curr_commtime)
                test_id_times[test_id][data_name]["idle"].append(curr_idletime)
                test_id_times[test_id][data_name]["end"].append(curr_commtime + curr_idletime)


# Remove negative values of the results
for msg_type in msg_type_times_ns:
    msg_type_times_ns[msg_type] = [number for number in msg_type_times_ns[msg_type] if number >= 0]

for msg_type in msg_type_idletimes_ns:
    msg_type_idletimes_ns[msg_type] = [number for number in msg_type_idletimes_ns[msg_type] if number >= 0]

for msg_type in msg_type_endtimes_ns:
    msg_type_endtimes_ns[msg_type] = [number for number in msg_type_endtimes_ns[msg_type] if number >= 0]


# Microseconds
msg_type_times_us = {}
msg_type_idletimes_us = {}
msg_type_endtimes_us = {}

for msg_type in msg_type_times_ns:
    msg_type_times_us[msg_type] = [time / 1000 for time in msg_type_times_ns[msg_type]]

for msg_type in msg_type_idletimes_ns:
    msg_type_idletimes_us[msg_type] = [time / 1000 for time in msg_type_idletimes_ns[msg_type]]

for msg_type in msg_type_endtimes_ns:
    msg_type_endtimes_us[msg_type] = [time / 1000 for time in msg_type_endtimes_ns[msg_type]]

# Milliseconds
msg_type_times_ms = {}
msg_type_idletimes_ms = {}
msg_type_endtimes_ms = {}

for msg_type in msg_type_times_ns:
    msg_type_times_us[msg_type] = [time / 1e6 for time in msg_type_times_ns[msg_type]]

for msg_type in msg_type_idletimes_ns:
    msg_type_idletimes_ms[msg_type] = [time / 1e6 for time in msg_type_idletimes_ns[msg_type]]

for msg_type in msg_type_endtimes_ns:
    msg_type_endtimes_ms[msg_type] = [time / 1e6 for time in msg_type_endtimes_ns[msg_type]]

###############################################################
# Figures
###############################################################

print("ROS2:")
print("comm: ", len(get_time_list(ros2_test_ids, "comm")))
print("idle: ", len(get_time_list(ros2_test_ids, "idle")))
print("end: ", len(get_time_list(ros2_test_ids, "end")))
print("comm - imu: ", len(get_time_list(ros2_test_ids, "comm", "imu")))

print("ASOA:")
print("comm: ", len(get_time_list(asoa_test_ids, "comm")))
print("idle: ", len(get_time_list(asoa_test_ids, "idle")))
print("end: ", len(get_time_list(asoa_test_ids, "end")))
print("comm - imu: ", len(get_time_list(asoa_test_ids, "comm", "imu")))

# asoa_test_ids
# print(len(test_id_times[]))

# exit(1)
asoa_dist_ids = get_test_ids(arch="asoa", dist=True)
asoa_dist = get_times_dict(asoa_dist_ids, "ms", "imu")
asoa_inter_ids = get_test_ids(arch="asoa", dist=False, comm_type="inter")
asoa_inter = get_times_dict(asoa_inter_ids, "ms", "imu")


asoa_imu_tests = {
    "dist": {
        "all": get_times_dict(get_test_ids(arch="asoa", dist=True), "ms", "imu"),
        "1-1": get_times_dict(get_test_ids(topology="1-1", arch="asoa", dist=True), "ms", "imu"),
        "1-2": get_times_dict(get_test_ids(topology="1-2", arch="asoa", dist=True), "ms", "imu"),
        "2-1": get_times_dict(get_test_ids(topology="2-1", arch="asoa", dist=True), "ms", "imu"),
        "1-3": get_times_dict(get_test_ids(topology="1-3", arch="asoa", dist=True), "ms", "imu"),
        "3-1": get_times_dict(get_test_ids(topology="3-1", arch="asoa", dist=True), "ms", "imu"),
        "fix": get_times_dict(get_test_ids(topology="1-1", arch="asoa", dist=True, rel="reliable"), "ms", "imu"),
    },
    "inter": {
        "all": get_times_dict(get_test_ids(arch="asoa", dist=False, comm_type="inter"), "ms", "imu"),
        "1-1": get_times_dict(get_test_ids(topology="1-1", arch="asoa", dist=False, comm_type="inter"), "ms", "imu"),
        "1-2": get_times_dict(get_test_ids(topology="1-2", arch="asoa", dist=False, comm_type="inter"), "ms", "imu"),
        "2-1": get_times_dict(get_test_ids(topology="2-1", arch="asoa", dist=False, comm_type="inter"), "ms", "imu"),
        "1-3": get_times_dict(get_test_ids(topology="1-3", arch="asoa", dist=False, comm_type="inter"), "ms", "imu"),
        "3-1": get_times_dict(get_test_ids(topology="3-1", arch="asoa", dist=False, comm_type="inter"), "ms", "imu"),
    },
    "intra": {
        "all": get_times_dict(get_test_ids(arch="asoa", dist=False, comm_type="intra"), "ms", "imu"),
        "1-1": get_times_dict(get_test_ids(topology="1-1", arch="asoa", dist=False, comm_type="intra"), "ms", "imu"),
        "1-2": get_times_dict(get_test_ids(topology="1-2", arch="asoa", dist=False, comm_type="intra"), "ms", "imu"),
        "2-1": get_times_dict(get_test_ids(topology="2-1", arch="asoa", dist=False, comm_type="intra"), "ms", "imu"),
        "1-3": get_times_dict(get_test_ids(topology="1-3", arch="asoa", dist=False, comm_type="intra"), "ms", "imu"),
        "3-1": get_times_dict(get_test_ids(topology="3-1", arch="asoa", dist=False, comm_type="intra"), "ms", "imu"),
    },
}
ros2_imu_tests = {
    "dist": {
        "all": get_times_dict(get_test_ids(arch="ros2", dist=True), "ms", "imu"),
        "1-1": get_times_dict(get_test_ids(topology="1-1", arch="ros2", dist=True), "ms", "imu"),
        "1-2": get_times_dict(get_test_ids(topology="1-2", arch="ros2", dist=True), "ms", "imu"),
        "2-1": get_times_dict(get_test_ids(topology="2-1", arch="ros2", dist=True), "ms", "imu"),
        "1-3": get_times_dict(get_test_ids(topology="1-3", arch="ros2", dist=True), "ms", "imu"),
        "3-1": get_times_dict(get_test_ids(topology="3-1", arch="ros2", dist=True), "ms", "imu"),
        "best_effort": get_times_dict(get_test_ids(arch="ros2", dist=True), "ms", "imu"),
        "reliable": get_times_dict(get_test_ids(arch="ros2", dist=True), "ms", "imu"),
        "fix": get_times_dict(get_test_ids(topology="1-1", arch="ros2", dist=True, rel="reliable"), "ms", "imu"),
    },
    "inter": {
        "all": get_times_dict(get_test_ids(arch="ros2", dist=False, comm_type="inter"), "ms", "imu"),
        "1-1": get_times_dict(get_test_ids(topology="1-1", arch="ros2", dist=False, comm_type="inter"), "ms", "imu"),
        "1-2": get_times_dict(get_test_ids(topology="1-2", arch="ros2", dist=False, comm_type="inter"), "ms", "imu"),
        "2-1": get_times_dict(get_test_ids(topology="2-1", arch="ros2", dist=False, comm_type="inter"), "ms", "imu"),
        "1-3": get_times_dict(get_test_ids(topology="1-3", arch="ros2", dist=False, comm_type="inter"), "ms", "imu"),
        "3-1": get_times_dict(get_test_ids(topology="3-1", arch="ros2", dist=False, comm_type="inter"), "ms", "imu"),
        "best_effort": get_times_dict(get_test_ids(arch="ros2", dist=False, comm_type="inter"), "ms", "imu"),
        "reliable": get_times_dict(get_test_ids(arch="ros2", dist=False, comm_type="inter"), "ms", "imu"),
    },
    "intra": {
        "all": get_times_dict(get_test_ids(arch="ros2", dist=False, comm_type="intra"), "ms", "imu"),
        "1-1": get_times_dict(get_test_ids(topology="1-1", arch="ros2", dist=False, comm_type="intra"), "ms", "imu"),
        "1-2": get_times_dict(get_test_ids(topology="1-2", arch="ros2", dist=False, comm_type="intra"), "ms", "imu"),
        "2-1": get_times_dict(get_test_ids(topology="2-1", arch="ros2", dist=False, comm_type="intra"), "ms", "imu"),
        "1-3": get_times_dict(get_test_ids(topology="1-3", arch="ros2", dist=False, comm_type="intra"), "ms", "imu"),
        "3-1": get_times_dict(get_test_ids(topology="3-1", arch="ros2", dist=False, comm_type="intra"), "ms", "imu"),
        "best_effort": get_times_dict(get_test_ids(arch="ros2", dist=False, comm_type="intra"), "ms", "imu"),
        "reliable": get_times_dict(get_test_ids(arch="ros2", dist=False, comm_type="intra"), "ms", "imu"),
    },
}
general_imu_tests = {
    "dist": {
        "all": get_times_dict(get_test_ids(dist=True), "ms", "imu"),
        "1-1": get_times_dict(get_test_ids(topology="1-1", dist=True), "ms", "imu"),
        "1-2": get_times_dict(get_test_ids(topology="1-2", dist=True), "ms", "imu"),
        "2-1": get_times_dict(get_test_ids(topology="2-1", dist=True), "ms", "imu"),
        "1-3": get_times_dict(get_test_ids(topology="1-3", dist=True), "ms", "imu"),
        "3-1": get_times_dict(get_test_ids(topology="3-1", dist=True), "ms", "imu"),
        "best_effort": get_times_dict(get_test_ids(dist=True, rel="best_effort"), "ms", "imu"),
        "reliable": get_times_dict(get_test_ids(dist=True, rel="reliable"), "ms", "imu"),
    },
    "inter": {
        "all": get_times_dict(get_test_ids(dist=False, comm_type="inter"), "ms", "imu"),
        "1-1": get_times_dict(get_test_ids(topology="1-1", dist=False, comm_type="inter"), "ms", "imu"),
        "1-2": get_times_dict(get_test_ids(topology="1-2", dist=False, comm_type="inter"), "ms", "imu"),
        "2-1": get_times_dict(get_test_ids(topology="2-1", dist=False, comm_type="inter"), "ms", "imu"),
        "1-3": get_times_dict(get_test_ids(topology="1-3", dist=False, comm_type="inter"), "ms", "imu"),
        "3-1": get_times_dict(get_test_ids(topology="3-1", dist=False, comm_type="inter"), "ms", "imu"),
        "best_effort": get_times_dict(get_test_ids(dist=False, comm_type="inter", rel="best_effort"), "ms", "imu"),
        "reliable": get_times_dict(get_test_ids(dist=False, comm_type="inter", rel="reliable"), "ms", "imu"),
    },
    "intra": {
        "all": get_times_dict(get_test_ids(dist=False, comm_type="intra"), "ms", "imu"),
        "1-1": get_times_dict(get_test_ids(topology="1-1", dist=False, comm_type="intra"), "ms", "imu"),
        "1-2": get_times_dict(get_test_ids(topology="1-2", dist=False, comm_type="intra"), "ms", "imu"),
        "2-1": get_times_dict(get_test_ids(topology="2-1", dist=False, comm_type="intra"), "ms", "imu"),
        "1-3": get_times_dict(get_test_ids(topology="1-3", dist=False, comm_type="intra"), "ms", "imu"),
        "3-1": get_times_dict(get_test_ids(topology="3-1", dist=False, comm_type="intra"), "ms", "imu"),
        "best_effort": get_times_dict(get_test_ids(dist=False, comm_type="intra", rel="best_effort"), "ms", "imu"),
        "reliable": get_times_dict(get_test_ids(dist=False, comm_type="intra", rel="reliable"), "ms", "imu"),
    },
}
"""
```py
comm_type: {
    # topology or "all"
    topology: {
        "comm": [],
        "idle": [],
        "end": [],
    }
}
```
"""

reliability_imu_tests = {
    "best_effort": get_times_dict(get_test_ids(topology="1-1", dist=True, rel="best_effort"), "ms", "imu"),
    "reliable": get_times_dict(get_test_ids(topology="1-1", dist=True, rel="reliable"), "ms", "imu"),
}
arch_reliability_imu_tests = {
    "asoa": {
        "best_effort": get_times_dict(get_test_ids(arch="asoa", topology="1-1", dist=True, rel="best_effort"), "ms", "imu"),
        "reliable": get_times_dict(get_test_ids(arch="asoa", topology="1-1", dist=True, rel="reliable"), "ms", "imu"),
    },
    "ros2": {
        "best_effort": get_times_dict(get_test_ids(arch="ros2", topology="1-1", dist=True, rel="best_effort"), "ms", "imu"),
        "reliable": get_times_dict(get_test_ids(arch="ros2", topology="1-1", dist=True, rel="reliable"), "ms", "imu"),
    }
}

class TopologyTests:
    one: list
    pub1_subn: list
    pubn_sub1: list

class DistributedTests:
    distributed: list
    local: list


class TestData:
    arch: str
    topology: TopologyTests
    dist: DistributedTests

    def __init__(self, arch: str, dist: DistributedTests, topology: TopologyTests) -> None:
        self.arch = arch
        self.topology = topology
        self.dist = dist
        pass

os.makedirs(f"{path}/figures", exist_ok=True)

if __name__ == "__main__":


    compare_dist = general_imu_tests["dist"]["all"]
    compare_inter = general_imu_tests["inter"]["all"]

    for time_type in time_types:
        dist_times_ms = compare_dist[time_type]
        inter_times_ms = compare_inter[time_type]

        dist_times_ms, inter_times_ms = trim_list(dist_times_ms, inter_times_ms)

        max_samples = max(len(dist_times_ms), len(inter_times_ms))
        x = np.linspace(1, max_samples, num=max_samples)
        fig = plt.figure()
        ax = plt.axes()
        ax.margins(x = 0.005, y = 0.005)
        plt.scatter(x, dist_times_ms, s=2, color='blue', zorder = 10, label="Distributed")
        plt.scatter(x, inter_times_ms, s=2, color='red', zorder = 10, label="Inter-proc")
        plt.plot(x, [min(dist_times_ms) for a in dist_times_ms], "--", label=f"Max: {max(dist_times_ms)} [ms] {dist_times_ms.index(max(dist_times_ms))}",color='blue')
        plt.plot(x, [max(dist_times_ms) for a in dist_times_ms], "--", label=f"Min: {min(dist_times_ms)} [ms] {dist_times_ms.index(min(dist_times_ms))}",color='blue')

        plt.plot(x, [min(inter_times_ms) for a in inter_times_ms], "--", label=f"Max: {max(inter_times_ms)} [ms] {inter_times_ms.index(max(inter_times_ms))}",color='red')
        plt.plot(x, [max(inter_times_ms) for a in inter_times_ms], "--", label=f"Min: {min(inter_times_ms)} [ms] {inter_times_ms.index(min(inter_times_ms))}",color='red')

        plt.legend(loc='upper right')
        ax.set_ylabel('Latency [ms]')
        ax.set_xlabel('Sample')
        ax.set_title(f'Distributed - Inter-proc (local) ({time_type}) runtimes')
        plt.savefig(f"{path}/figures/general_dist_inter_{time_type}_ms.png", dpi = 300)

        plt.clf()
        plt.cla()
        plt.close()

    for arch in archs:
        compare_best_effort = arch_reliability_imu_tests[arch]["best_effort"]
        compare_reliable = arch_reliability_imu_tests[arch]["reliable"]

        for time_type in time_types:
            best_effort_times_ms = compare_best_effort[time_type]
            reliable_times_ms = compare_reliable[time_type]

            best_effort_times_ms, reliable_times_ms = trim_list(best_effort_times_ms, reliable_times_ms)

            max_samples = max(len(best_effort_times_ms), len(reliable_times_ms))
            x = np.linspace(1, max_samples, num=max_samples)
            fig = plt.figure()
            ax = plt.axes()
            ax.margins(x = 0.005, y = 0.005)
            plt.scatter(x, best_effort_times_ms, s=2, color='blue', zorder = 10, label="best_effort")
            plt.scatter(x, reliable_times_ms, s=2, color='red', zorder = 10, label="reliable")
            plt.plot(x, [min(best_effort_times_ms) for a in best_effort_times_ms], "--", label=f"Max: {max(best_effort_times_ms)} [ms] {best_effort_times_ms.index(max(best_effort_times_ms))}",color='blue')
            plt.plot(x, [max(best_effort_times_ms) for a in best_effort_times_ms], "--", label=f"Min: {min(best_effort_times_ms)} [ms] {best_effort_times_ms.index(min(best_effort_times_ms))}",color='blue')

            plt.plot(x, [min(reliable_times_ms) for a in reliable_times_ms], "--", label=f"Max: {max(reliable_times_ms)} [ms] {reliable_times_ms.index(max(reliable_times_ms))}",color='red')
            plt.plot(x, [max(reliable_times_ms) for a in reliable_times_ms], "--", label=f"Min: {min(reliable_times_ms)} [ms] {reliable_times_ms.index(min(reliable_times_ms))}",color='red')

            plt.legend(loc='upper right')
            ax.set_ylabel('Latency [ms]')
            ax.set_xlabel('Sample')
            ax.set_title(f'{arch.upper()} Reliability ({time_type}) runtimes [1-1, dist]')
            plt.savefig(f"{path}/figures/{arch}_reliability_1-1_dist_{time_type}_ms.png", dpi = 300)

            plt.clf()
            plt.cla()
            plt.close()


    asoa_compare_best_effort = arch_reliability_imu_tests["asoa"]["best_effort"]
    asoa_compare_reliable = arch_reliability_imu_tests["asoa"]["reliable"]
    ros2_compare_best_effort = arch_reliability_imu_tests["ros2"]["best_effort"]
    ros2_compare_reliable = arch_reliability_imu_tests["ros2"]["reliable"]

    for time_type in time_types:
        asoa_best_effort_times_ms = asoa_compare_best_effort[time_type]
        asoa_reliable_times_ms = asoa_compare_reliable[time_type]
        ros2_best_effort_times_ms = ros2_compare_best_effort[time_type]
        ros2_reliable_times_ms = ros2_compare_reliable[time_type]

        min_size = min(len(asoa_best_effort_times_ms), len(asoa_reliable_times_ms), len(ros2_best_effort_times_ms), len(ros2_reliable_times_ms))

        asoa_best_effort_times_ms = asoa_best_effort_times_ms[:min_size]
        asoa_reliable_times_ms = asoa_reliable_times_ms[:min_size]
        ros2_best_effort_times_ms = ros2_best_effort_times_ms[:min_size]
        ros2_reliable_times_ms = ros2_reliable_times_ms[:min_size]

        max_samples = max(len(asoa_best_effort_times_ms), len(asoa_reliable_times_ms), len(ros2_best_effort_times_ms), len(ros2_reliable_times_ms))
        x = np.linspace(1, max_samples, num=max_samples)
        fig = plt.figure()
        ax = plt.axes()
        ax.margins(x = 0.005, y = 0.005)
        plt.scatter(x, asoa_best_effort_times_ms, s=2, color='blue', zorder = 10, label="ASOA best_effort")
        plt.scatter(x, asoa_reliable_times_ms, s=2, color='red', zorder = 10, label="ASOA reliable")
        plt.scatter(x, ros2_best_effort_times_ms, s=2, color='green', zorder = 10, label="ROS2 best_effort")
        plt.scatter(x, ros2_reliable_times_ms, s=2, color='orange', zorder = 10, label="ROS2 reliable")

        plt.plot(x, [min(asoa_best_effort_times_ms) for a in asoa_best_effort_times_ms], "--", label=f"Max: {max(asoa_best_effort_times_ms)} [ms] {asoa_best_effort_times_ms.index(max(asoa_best_effort_times_ms))}",color='blue')
        plt.plot(x, [max(asoa_best_effort_times_ms) for a in asoa_best_effort_times_ms], "--", label=f"Min: {min(asoa_best_effort_times_ms)} [ms] {asoa_best_effort_times_ms.index(min(asoa_best_effort_times_ms))}",color='blue')

        plt.plot(x, [min(asoa_reliable_times_ms) for a in asoa_reliable_times_ms], "--", label=f"Max: {max(asoa_reliable_times_ms)} [ms] {asoa_reliable_times_ms.index(max(asoa_reliable_times_ms))}",color='red')
        plt.plot(x, [max(asoa_reliable_times_ms) for a in asoa_reliable_times_ms], "--", label=f"Min: {min(asoa_reliable_times_ms)} [ms] {asoa_reliable_times_ms.index(min(asoa_reliable_times_ms))}",color='red')

        plt.plot(x, [min(ros2_best_effort_times_ms) for a in ros2_best_effort_times_ms], "--", label=f"Max: {max(ros2_best_effort_times_ms)} [ms] {ros2_best_effort_times_ms.index(max(ros2_best_effort_times_ms))}",color='green')
        plt.plot(x, [max(ros2_best_effort_times_ms) for a in ros2_best_effort_times_ms], "--", label=f"Min: {min(ros2_best_effort_times_ms)} [ms] {ros2_best_effort_times_ms.index(min(ros2_best_effort_times_ms))}",color='green')

        plt.plot(x, [min(ros2_reliable_times_ms) for a in ros2_reliable_times_ms], "--", label=f"Max: {max(ros2_reliable_times_ms)} [ms] {ros2_reliable_times_ms.index(max(ros2_reliable_times_ms))}",color='orange')
        plt.plot(x, [max(ros2_reliable_times_ms) for a in ros2_reliable_times_ms], "--", label=f"Min: {min(ros2_reliable_times_ms)} [ms] {ros2_reliable_times_ms.index(min(ros2_reliable_times_ms))}",color='orange')

        plt.legend(loc='upper right')
        ax.set_ylabel('Latency [ms]')
        ax.set_xlabel('Sample')
        ax.set_title(f'ASOA - ROS2 Reliability ({time_type}) runtimes [1-1, dist]')
        plt.savefig(f"{path}/figures/asoa_ros2_reliability_1-1_dist_{time_type}_ms.png", dpi = 300)

        plt.clf()
        plt.cla()
        plt.close()

    compare_arch = {
        "asoa": asoa_imu_tests["dist"]["fix"],
        "ros2": ros2_imu_tests["dist"]["fix"]
    }

    for time_type in time_types:
        asoa_times_ms = compare_arch["asoa"][time_type]
        ros2_times_ms = compare_arch["ros2"][time_type]

        asoa_times_ms, ros2_times_ms = trim_list(asoa_times_ms, ros2_times_ms)

        max_samples = max(len(asoa_times_ms), len(ros2_times_ms))
        x = np.linspace(1, max_samples, num=max_samples)
        fig = plt.figure()
        ax = plt.axes()
        ax.margins(x = 0.005, y = 0.005)
        plt.scatter(x, asoa_times_ms, s=2, color='blue', zorder = 10, label="ASOA")
        plt.scatter(x, ros2_times_ms, s=2, color='red', zorder = 10, label="ROS2")
        plt.plot(x, [min(asoa_times_ms) for a in asoa_times_ms], "--", label=f"Max: {max(asoa_times_ms)} [ms] {asoa_times_ms.index(max(asoa_times_ms))}",color='blue')
        plt.plot(x, [max(asoa_times_ms) for a in asoa_times_ms], "--", label=f"Min: {min(asoa_times_ms)} [ms] {asoa_times_ms.index(min(asoa_times_ms))}",color='blue')

        plt.plot(x, [min(ros2_times_ms) for a in ros2_times_ms], "--", label=f"Max: {max(ros2_times_ms)} [ms] {ros2_times_ms.index(max(ros2_times_ms))}",color='red')
        plt.plot(x, [max(ros2_times_ms) for a in ros2_times_ms], "--", label=f"Min: {min(ros2_times_ms)} [ms] {ros2_times_ms.index(min(ros2_times_ms))}",color='red')

        plt.legend(loc='upper right')
        ax.set_ylabel('Latency [ms]')
        ax.set_xlabel('Sample')
        ax.set_title(f'ASOA - ROS2 ({time_type}) runtimes [1-1, dist, reliable]')
        plt.savefig(f"{path}/figures/arch_reliable_1-1_dist_{time_type}_ms.png", dpi = 300)
        plt.savefig(f"{path}/figures/arch_reliable_1-1_dist_{time_type}_ms.pdf")

        plt.clf()
        plt.cla()
        plt.close()


    for time_type in time_types:
        print(get_test_ids(topology="2-1", arch="asoa", dist=True, rel="reliable"))
        print(get_test_ids(topology="1-2", arch="asoa", dist=True, rel="reliable"))
        asoa_times_ms = get_times_dict(get_test_ids(topology="1-2", arch="asoa", dist=True, rel="reliable"), "ms", "imu")[time_type]
        ros2_times_ms = get_times_dict(get_test_ids(topology="1-2", arch="ros2", dist=True, rel="reliable"), "ms", "imu")[time_type]
        print(len(asoa_times_ms))
        print(len(ros2_times_ms))
        asoa_times_ms = get_times_dict(get_test_ids(topology="2-1", arch="asoa", dist=True, rel="reliable"), "ms", "imu")[time_type]
        ros2_times_ms = get_times_dict(get_test_ids(topology="2-1", arch="ros2", dist=True, rel="reliable"), "ms", "imu")[time_type]
        print(len(asoa_times_ms))
        print(len(ros2_times_ms))

        asoa_times_ms, ros2_times_ms = trim_list(asoa_times_ms, ros2_times_ms)

        max_samples = max(len(asoa_times_ms), len(ros2_times_ms))
        x = np.linspace(1, max_samples, num=max_samples)
        fig = plt.figure()
        ax = plt.axes()
        ax.margins(x = 0.005, y = 0.005)
        plt.scatter(x, asoa_times_ms, s=2, color='blue', zorder = 10, label="ASOA")
        plt.scatter(x, ros2_times_ms, s=2, color='red', zorder = 10, label="ROS2")
        plt.plot(x, [min(asoa_times_ms) for a in asoa_times_ms], "--", label=f"Max: {max(asoa_times_ms)} [ms] {asoa_times_ms.index(max(asoa_times_ms))}",color='blue')
        plt.plot(x, [max(asoa_times_ms) for a in asoa_times_ms], "--", label=f"Min: {min(asoa_times_ms)} [ms] {asoa_times_ms.index(min(asoa_times_ms))}",color='blue')

        plt.plot(x, [min(ros2_times_ms) for a in ros2_times_ms], "--", label=f"Max: {max(ros2_times_ms)} [ms] {ros2_times_ms.index(max(ros2_times_ms))}",color='red')
        plt.plot(x, [max(ros2_times_ms) for a in ros2_times_ms], "--", label=f"Min: {min(ros2_times_ms)} [ms] {ros2_times_ms.index(min(ros2_times_ms))}",color='red')

        plt.legend(loc='upper right')
        ax.set_ylabel('Latency [ms]')
        ax.set_xlabel('Sample')
        ax.set_title(f'ASOA - ROS2 ({time_type}) runtimes [2-1, dist, reliable]')
        plt.savefig(f"{path}/figures/arch_reliable_2-1_dist_{time_type}_ms.png", dpi = 300)
        plt.savefig(f"{path}/figures/arch_reliable_2-1_dist_{time_type}_ms.pdf")

        plt.clf()
        plt.cla()
        plt.close()


    for time_type in time_types:
        asoa_times_ms = get_times_dict(get_test_ids(topology="1-2", arch="asoa", dist=True, rel="reliable"), "ms", "imu")[time_type]
        ros2_times_ms = get_times_dict(get_test_ids(topology="1-2", arch="ros2", dist=True, rel="reliable"), "ms", "imu")[time_type]

        asoa_times_ms, ros2_times_ms = trim_list(asoa_times_ms, ros2_times_ms)

        max_samples = max(len(asoa_times_ms), len(ros2_times_ms))
        x = np.linspace(1, max_samples, num=max_samples)
        fig = plt.figure()
        ax = plt.axes()
        ax.margins(x = 0.005, y = 0.005)
        plt.scatter(x, asoa_times_ms, s=2, color='blue', zorder = 10, label="ASOA")
        plt.scatter(x, ros2_times_ms, s=2, color='red', zorder = 10, label="ROS2")
        plt.plot(x, [min(asoa_times_ms) for a in asoa_times_ms], "--", label=f"Max: {max(asoa_times_ms)} [ms] {asoa_times_ms.index(max(asoa_times_ms))}",color='blue')
        plt.plot(x, [max(asoa_times_ms) for a in asoa_times_ms], "--", label=f"Min: {min(asoa_times_ms)} [ms] {asoa_times_ms.index(min(asoa_times_ms))}",color='blue')

        plt.plot(x, [min(ros2_times_ms) for a in ros2_times_ms], "--", label=f"Max: {max(ros2_times_ms)} [ms] {ros2_times_ms.index(max(ros2_times_ms))}",color='red')
        plt.plot(x, [max(ros2_times_ms) for a in ros2_times_ms], "--", label=f"Min: {min(ros2_times_ms)} [ms] {ros2_times_ms.index(min(ros2_times_ms))}",color='red')

        plt.legend(loc='upper right')
        ax.set_ylabel('Latency [ms]')
        ax.set_xlabel('Sample')
        ax.set_title(f'ASOA - ROS2 ({time_type}) runtimes [1-2, dist, reliable]')
        plt.savefig(f"{path}/figures/arch_reliable_1-2_dist_{time_type}_ms.png", dpi = 300)
        plt.savefig(f"{path}/figures/arch_reliable_1-2_dist_{time_type}_ms.pdf")

        plt.clf()
        plt.cla()
        plt.close()
