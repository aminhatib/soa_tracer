from eval.analyse_results import *
from pandas.plotting import table
import statistics as stats
from matplotlib.font_manager import FontProperties

def remove_numbers(test_list, remove_list):
    for num in remove_list:
        if num in test_list:
            test_list.remove(num)
    return test_list

###################################################
# Latency times

fig = plt.figure()
ax = plt.axes()
# hide axes
fig.patch.set_visible(False)
ax.axis('off')
ax.axis('tight')

asoa_comm_table = pandas.DataFrame()

comm_data = {}
# error values
values_to_remove = [334.684378, 334.089554]

for time_type in time_types:
    # asoa_imu_times_ms = get_times_dict(get_test_ids(arch="asoa"), "ms", "imu")[time_type]
    asoa_imu_times_ms = get_times_dict(get_test_ids(arch="asoa", topology="1-1", dist=True, rel="reliable"), "ms", "imu")[time_type]
    remove_numbers(asoa_imu_times_ms, values_to_remove)

    comm_data = {time_type: asoa_imu_times_ms}
    comm_col = pandas.DataFrame(comm_data)

    # get statistics description
    asoa_comm_table[time_type] = comm_col.describe()

asoa_comm_table.rename(columns={"comm": "Communication", "idle": "Idle", "end": "End-to-End"}, inplace=True)
asoa_comm_table.drop(index="count", inplace=True)

asoa_comm_table= asoa_comm_table.round(2)
print(asoa_comm_table)

tb = ax.table(cellText=asoa_comm_table.values, colLabels=asoa_comm_table.columns, rowLabels=asoa_comm_table.index, loc="center", cellLoc="center")

# set table size and font
tb.auto_set_font_size(False)
tb.set_fontsize(24)
tb.scale(2, 2)

# Set the column labels to bold
for (row, col), cell in tb.get_celld().items():
    if row == 0:
        cell.set_text_props(fontproperties=FontProperties(weight='bold'))

#prepare for saving:
# draw canvas once
plt.gcf().canvas.draw()
# get bounding box of table
points = tb.get_window_extent(plt.gcf()._cachedRenderer).get_points()
# add 10 pixel spacing
points[0,:] -= 10; points[1,:] += 10
# get new bounding box in inches
nbbox = matplotlib.transforms.Bbox.from_extents(points/plt.gcf().dpi)

plt.savefig(f"{path}/figures/asoa_latencies_table_ms.png", dpi = 300, bbox_inches=nbbox)
plt.savefig(f"{path}/figures/asoa_latencies_table_ms.pdf", dpi = 300, bbox_inches=nbbox)

plt.clf()
plt.cla()
plt.close()

##########################################################

fig = plt.figure()
ax = plt.axes()
# hide axes
fig.patch.set_visible(False)
ax.axis('off')
ax.axis('tight')

ros2_comm_table = pandas.DataFrame()

comm_data = {}
# error values
values_to_remove = [7995.663039, 7992.930081, 7996.927528, 7993.421395]

for time_type in time_types:
    # ros2_imu_times_ms = get_times_dict(get_test_ids(arch="ros2"), "ms", "imu")[time_type]
    ros2_imu_times_ms = get_times_dict(get_test_ids(arch="ros2", topology="1-1", dist=True, rel="reliable"), "ms", "imu")[time_type]
    remove_numbers(ros2_imu_times_ms, values_to_remove)

    comm_data = {time_type: ros2_imu_times_ms}
    comm_col = pandas.DataFrame(comm_data)

    # get statistics description
    ros2_comm_table[time_type] = comm_col.describe()

ros2_comm_table.rename(columns={"comm": "Communication", "idle": "Idle", "end": "End-to-End"}, inplace=True)
ros2_comm_table.drop(index="count", inplace=True)

ros2_comm_table = ros2_comm_table.round(2)
print(ros2_comm_table)

tb = ax.table(cellText=ros2_comm_table.values, colLabels=ros2_comm_table.columns, rowLabels=ros2_comm_table.index, loc="center", cellLoc="center")

# set table size and font
tb.auto_set_font_size(False)
tb.set_fontsize(24)
tb.scale(2, 2)

# Set the column labels to bold
for (row, col), cell in tb.get_celld().items():
    cell.set_text_props(fontproperties=FontProperties(weight='bold'))
    if row == 0:
        cell.set_text_props(fontproperties=FontProperties(weight='bold'))

#prepare for saving:
# draw canvas once
plt.gcf().canvas.draw()
# get bounding box of table
points = tb.get_window_extent(plt.gcf()._cachedRenderer).get_points()
# add 10 pixel spacing
points[0,:] -= 10; points[1,:] += 10
# get new bounding box in inches
nbbox = matplotlib.transforms.Bbox.from_extents(points/plt.gcf().dpi)

plt.savefig(f"{path}/figures/ros2_latencies_table_ms.png", dpi = 300, bbox_inches=nbbox)
plt.savefig(f"{path}/figures/ros2_latencies_table_ms.pdf", dpi = 300, bbox_inches=nbbox)

plt.clf()
plt.cla()
plt.close()