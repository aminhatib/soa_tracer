from eval.analyse_results import *


###################################################
# Compare data types (mean, median)
###################################################
for arch in archs:
    for time_type in time_types:
        arch_imu_times_ms = get_times_dict(get_test_ids(topology="1-1", arch=arch, dist=False, comm_type="intra", rel="reliable"), "ms", "imu")[time_type]
        arch_radar_times_ms = get_times_dict(get_test_ids(topology="1-1", arch=arch, dist=False, comm_type="intra", rel="reliable"), "ms", "radar")[time_type]
        arch_camera_times_ms = get_times_dict(get_test_ids(topology="1-1", arch=arch, dist=False, comm_type="intra", rel="reliable"), "ms", "camera")[time_type]
        arch_lidar_times_ms = get_times_dict(get_test_ids(topology="1-1", arch=arch, dist=False, comm_type="intra", rel="reliable"), "ms", "lidar")[time_type]

        # arch_imu_times_ms = remove_outliers(arch_imu_times_ms)
        # arch_radar_times_ms = remove_outliers(arch_radar_times_ms)
        # arch_camera_times_ms = remove_outliers(arch_camera_times_ms)
        # arch_lidar_times_ms = remove_outliers(arch_lidar_times_ms)

        arch_imu_mean = np.mean(arch_imu_times_ms)
        arch_imu_median = np.median(arch_imu_times_ms)

        arch_radar_mean = np.mean(arch_radar_times_ms)
        arch_radar_median = np.median(arch_radar_times_ms)

        arch_camera_mean = np.mean(arch_camera_times_ms)
        arch_camera_median = np.median(arch_camera_times_ms)

        arch_lidar_mean = np.mean(arch_lidar_times_ms)
        arch_lidar_median = np.median(arch_lidar_times_ms)

        means = [arch_imu_mean, arch_radar_mean, arch_camera_mean, arch_lidar_mean]
        medians = [arch_imu_median, arch_radar_median, arch_camera_median, arch_lidar_median]

        width = 0.35
        fig, ax = plt.subplots()

        # Set position of bar on X axis 
        br1 = np.arange(len(means)) 
        br2 = [x + width for x in br1] 

        mean_rect = ax.bar(br1, means, width, label='Mean')
        median_rect = ax.bar(br2, medians, width, label='Median')
        
        # Add labels and title
        ax.set_ylabel('Latency [ms]')
        ax.set_title(f'{arch.upper()} ({time_type}) runtimes [1-1, cent, intra, reliable]')
        plt.xticks([r + (width/2) for r in range(len(means))], 
            ['IMU', 'Radar', 'Camera', 'Lidar'])
        ax.legend()

        ax.set_ylabel('Latency [ms]')
        plt.savefig(f"{path}/figures/{arch}_BAR_data-types_mean-median_reliable_1-1_cent_intra_{time_type}_ms.png", dpi = 300)
        plt.savefig(f"{path}/figures/{arch}_BAR_data-types_mean-median_reliable_1-1_cent_intra_{time_type}_ms.pdf")

        plt.clf()
        plt.cla()
        plt.close()


###################################################
# Compare data types (median[50th], 95th, 99th)
###################################################
for arch in archs:
    for time_type in time_types:
        arch_imu_times_ms = get_times_dict(get_test_ids(topology="1-1", arch=arch, dist=False, comm_type="intra", rel="reliable"), "ms", "imu")[time_type]
        arch_radar_times_ms = get_times_dict(get_test_ids(topology="1-1", arch=arch, dist=False, comm_type="intra", rel="reliable"), "ms", "radar")[time_type]
        arch_camera_times_ms = get_times_dict(get_test_ids(topology="1-1", arch=arch, dist=False, comm_type="intra", rel="reliable"), "ms", "camera")[time_type]
        arch_lidar_times_ms = get_times_dict(get_test_ids(topology="1-1", arch=arch, dist=False, comm_type="intra", rel="reliable"), "ms", "lidar")[time_type]

        # arch_imu_times_ms = remove_outliers(arch_imu_times_ms)
        # arch_radar_times_ms = remove_outliers(arch_radar_times_ms)
        # arch_camera_times_ms = remove_outliers(arch_camera_times_ms)
        # arch_lidar_times_ms = remove_outliers(arch_lidar_times_ms)

        arch_imu_median = np.median(arch_imu_times_ms)
        arch_imu_p95 = np.percentile(arch_imu_times_ms, 95)
        arch_imu_p99 = np.percentile(arch_imu_times_ms, 99)

        arch_radar_median = np.median(arch_radar_times_ms)
        arch_radar_p95 = np.percentile(arch_radar_times_ms, 95)
        arch_radar_p99 = np.percentile(arch_radar_times_ms, 99)

        arch_camera_median = np.median(arch_camera_times_ms)
        arch_camera_p95 = np.percentile(arch_camera_times_ms, 95)
        arch_camera_p99 = np.percentile(arch_camera_times_ms, 99)

        arch_lidar_median = np.median(arch_lidar_times_ms)
        arch_lidar_p95 = np.percentile(arch_lidar_times_ms, 95)
        arch_lidar_p99 = np.percentile(arch_lidar_times_ms, 99)

        medians = [arch_imu_median, arch_radar_median, arch_camera_median, arch_lidar_median]
        p95s = [arch_imu_p95, arch_radar_p95, arch_camera_p95, arch_lidar_p95]
        p99s = [arch_imu_p99, arch_radar_p99, arch_camera_p99, arch_lidar_p99]

        width = 0.15
        fig, ax = plt.subplots()

        # Set position of bar on X axis 
        br1 = np.arange(len(medians)) 
        br2 = [x + width for x in br1] 
        br3 = [x + width for x in br2] 

        median_rect = ax.bar(br1, medians, width, label='50th Percentile (Median)')
        p95_rect = ax.bar(br2, p95s, width, label='95th Percentile')
        p99_rect = ax.bar(br3, p99s, width, label='99th Percentile')
        
        # Add labels and title
        ax.set_ylabel('Latency [ms]')
        ax.set_title(f'{arch.upper()} ({time_type}) runtimes [1-1, cent, intra, reliable]')
        plt.xticks([r + (width) for r in range(len(medians))], 
            ['IMU', 'Radar', 'Camera', 'Lidar'])
        ax.legend()

        ax.set_ylabel('Latency [ms]')
        plt.savefig(f"{path}/figures/{arch}_BAR_data-types_percentile_reliable_1-1_cent_intra_{time_type}_ms.png", dpi = 300)
        plt.savefig(f"{path}/figures/{arch}_BAR_data-types_percentile_reliable_1-1_cent_intra_{time_type}_ms.pdf")

        plt.clf()
        plt.cla()
        plt.close()

###################################################
# Compare data types imu-radar (median[50th], 95th, 99th)
###################################################
for arch in archs:
    for time_type in time_types:
        arch_imu_times_ms = get_times_dict(get_test_ids(topology="1-1", arch=arch, dist=False, comm_type="intra", rel="reliable"), "ms", "imu")[time_type]
        arch_radar_times_ms = get_times_dict(get_test_ids(topology="1-1", arch=arch, dist=False, comm_type="intra", rel="reliable"), "ms", "radar")[time_type]

        # arch_imu_times_ms = remove_outliers(arch_imu_times_ms)
        # arch_radar_times_ms = remove_outliers(arch_radar_times_ms)

        arch_imu_median = np.median(arch_imu_times_ms)
        arch_imu_p95 = np.percentile(arch_imu_times_ms, 95)
        arch_imu_p99 = np.percentile(arch_imu_times_ms, 99)

        arch_radar_median = np.median(arch_radar_times_ms)
        arch_radar_p95 = np.percentile(arch_radar_times_ms, 95)
        arch_radar_p99 = np.percentile(arch_radar_times_ms, 99)

        medians = [arch_imu_median, arch_radar_median]
        p95s = [arch_imu_p95, arch_radar_p95]
        p99s = [arch_imu_p99, arch_radar_p99]

        width = 0.15
        fig, ax = plt.subplots()

        # Set position of bar on X axis 
        br1 = np.arange(len(medians)) 
        br2 = [x + width for x in br1] 
        br3 = [x + width for x in br2] 

        median_rect = ax.bar(br1, medians, width, label='50th Percentile (Median)')
        p95_rect = ax.bar(br2, p95s, width, label='95th Percentile')
        p99_rect = ax.bar(br3, p99s, width, label='99th Percentile')
        
        # Add labels and title
        ax.set_ylabel('Latency [ms]')
        ax.set_title(f'{arch.upper()} ({time_type}) runtimes [1-1, cent, intra, reliable]')
        plt.xticks([r + (width) for r in range(len(medians))], 
            ['IMU', 'Radar'])
        ax.legend()

        ax.set_ylabel('Latency [ms]')
        plt.savefig(f"{path}/figures/{arch}_BAR_imu-radar_percentile_reliable_1-1_cent_intra_{time_type}_ms.png", dpi = 300)
        plt.savefig(f"{path}/figures/{arch}_BAR_imu-radar_percentile_reliable_1-1_cent_intra_{time_type}_ms.pdf")

        plt.clf()
        plt.cla()
        plt.close()