#!/bin/bash

# get directory name of the script and cd to it
SCRIPT_DIR=$(dirname $0)
cd $SCRIPT_DIR/../

if [ ! -f ./bin/soatracer-asoa ] || [ ! -f ./bin/asoa-orch ]; then
    echo "Binary files cannot be found in bin/ folder. Please compile the project first!"
    exit 1
fi


SAMPLES=10

cmd1='./bin/soatracer-asoa -s '$SAMPLES' -c "{\"id\":41,\"distributed\":false,\"comm_type\":\"intra\",\"publishers\":1,\"subscribers\":2,\"services\":[{\"id\":1,\"name\":\"camera\",\"type\":\"publisher\",\"msg\":{\"size\":\"40000000\",\"frequency\":\"50\"},\"parameters\":{\"reliability\":\"best_effort\",\"history\":\"keep_last_1\",\"durability\":\"transient_local\",\"task_type\":\"periodic\",\"task_frequency\":\"50\"}},{\"id\":1,\"name\":\"camera\",\"type\":\"subscriber\",\"msg\":null,\"parameters\":{\"reliability\":\"best_effort\",\"history\":\"keep_last_1\",\"durability\":\"transient_local\",\"task_type\":\"periodic\",\"task_frequency\":\"50\"}},{\"id\":2,\"name\":\"camera\",\"type\":\"subscriber\",\"msg\":null,\"parameters\":{\"reliability\":\"best_effort\",\"history\":\"keep_last_1\",\"durability\":\"transient_local\",\"task_type\":\"periodic\",\"task_frequency\":\"50\"}}]}"'

cmd2='./bin/asoa-orch -c "[{\"id\":41,\"distributed\":false,\"comm_type\":\"intra\",\"publishers\":1,\"subscribers\":2,\"services\":[{\"id\":1,\"name\":\"camera\",\"type\":\"publisher\",\"msg\":{\"size\":\"40000000\",\"frequency\":\"50\"},\"parameters\":{\"reliability\":\"best_effort\",\"history\":\"keep_last_1\",\"durability\":\"transient_local\",\"task_type\":\"periodic\",\"task_frequency\":\"50\"}},{\"id\":1,\"name\":\"camera\",\"type\":\"subscriber\",\"msg\":null,\"parameters\":{\"reliability\":\"best_effort\",\"history\":\"keep_last_1\",\"durability\":\"transient_local\",\"task_type\":\"periodic\",\"task_frequency\":\"50\"}},{\"id\":2,\"name\":\"camera\",\"type\":\"subscriber\",\"msg\":null,\"parameters\":{\"reliability\":\"best_effort\",\"history\":\"keep_last_1\",\"durability\":\"transient_local\",\"task_type\":\"periodic\",\"task_frequency\":\"50\"}}]}]"'

session_name="asoa_test"

tmux new-session -d -s $session_name

# Split same pane vertically
tmux split-window -v -t $session_name:0.0

# Pane 1: Run command 1
tmux send-keys -t $session_name:0.0 "$cmd1" C-m
# Pane 2: Run command 2
tmux send-keys -t $session_name:0.1 "$cmd2" C-m

# Attach to the session
tmux attach-session -t $session_name
