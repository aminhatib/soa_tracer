#!/bin/bash

# get directory name of the script and cd to it
SCRIPT_DIR=$(dirname $0)
cd $SCRIPT_DIR/../

if [ ! -f ./bin/soatracer-asoa ] || [ ! -f ./bin/asoa-orch ]; then
    echo "Binary files cannot be found in bin/ folder. Please compile the project first!"
    exit 1
fi


SAMPLES=10

cmd1='./bin/soatracer-asoa -s '$SAMPLES' -c "{\"id\":573,\"distributed\":false,\"comm_type\":\"intra\",\"publishers\":2,\"subscribers\":1,\"services\":[{\"name\":\"imu\",\"type\":\"publisher\",\"msg\":{\"size\":\"1000\",\"frequency\":\"200\"},\"parameters\":{\"reliability\":\"reliable\",\"history\":\"keep_all\",\"durability\":\"volatile\",\"task_type\":\"periodic\",\"task_frequency\":\"200\"},\"id\":1},{\"name\":\"imu\",\"type\":\"publisher\",\"msg\":{\"size\":\"1000\",\"frequency\":\"200\"},\"parameters\":{\"reliability\":\"reliable\",\"history\":\"keep_all\",\"durability\":\"volatile\",\"task_type\":\"periodic\",\"task_frequency\":\"200\"},\"id\":2},{\"name\":\"imu\",\"type\":\"subscriber\",\"msg\":null,\"parameters\":{\"reliability\":\"reliable\",\"history\":\"keep_all\",\"durability\":\"volatile\",\"task_type\":\"periodic\",\"task_frequency\":\"200\"},\"id\":1},{\"name\":\"imu\",\"type\":\"subscriber\",\"msg\":null,\"parameters\":{\"reliability\":\"reliable\",\"history\":\"keep_all\",\"durability\":\"volatile\",\"task_type\":\"periodic\",\"task_frequency\":\"200\"},\"id\":2}]}"'

cmd2='./bin/asoa-orch -c "[{\"id\":573,\"distributed\":false,\"comm_type\":\"intra\",\"publishers\":2,\"subscribers\":1,\"services\":[{\"name\":\"imu\",\"type\":\"publisher\",\"msg\":{\"size\":\"1000\",\"frequency\":\"200\"},\"parameters\":{\"reliability\":\"reliable\",\"history\":\"keep_all\",\"durability\":\"volatile\",\"task_type\":\"periodic\",\"task_frequency\":\"200\"},\"id\":1},{\"name\":\"imu\",\"type\":\"publisher\",\"msg\":{\"size\":\"1000\",\"frequency\":\"200\"},\"parameters\":{\"reliability\":\"reliable\",\"history\":\"keep_all\",\"durability\":\"volatile\",\"task_type\":\"periodic\",\"task_frequency\":\"200\"},\"id\":2},{\"name\":\"imu\",\"type\":\"subscriber\",\"msg\":null,\"parameters\":{\"reliability\":\"reliable\",\"history\":\"keep_all\",\"durability\":\"volatile\",\"task_type\":\"periodic\",\"task_frequency\":\"200\"},\"id\":1},{\"name\":\"imu\",\"type\":\"subscriber\",\"msg\":null,\"parameters\":{\"reliability\":\"reliable\",\"history\":\"keep_all\",\"durability\":\"volatile\",\"task_type\":\"periodic\",\"task_frequency\":\"200\"},\"id\":2}]}]"'

session_name="asoa_test"

tmux new-session -d -s $session_name

# Split same pane vertically
tmux split-window -v -t $session_name:0.0

# Pane 1: Run command 1
tmux send-keys -t $session_name:0.0 "$cmd1" C-m
# Pane 2: Run command 2
tmux send-keys -t $session_name:0.1 "$cmd2" C-m

# Attach to the session
tmux attach-session -t $session_name
