#!/bin/bash

# get directory name of the script and cd to it
SCRIPT_DIR=$(dirname $0)
cd $SCRIPT_DIR/../

if [ ! -f ./bin/soatracer-ros2 ]; then
    echo "Binary files cannot be found in bin/ folder. Please compile the project first!"
    exit 1
fi


SOURCE="source /opt/ros/iron/setup.sh"
SAMPLES=10

cmd1=$SOURCE' && ./bin/soatracer-ros2 -s '$SAMPLES' -c "{\"id\":41,\"distributed\":false,\"comm_type\":\"intra\",\"publishers\":1,\"subscribers\":2,\"services\":[{\"id\":1,\"name\":\"camera\",\"type\":\"publisher\",\"msg\":{\"size\":\"40000000\",\"frequency\":\"50\"},\"parameters\":{\"reliability\":\"best_effort\",\"history\":\"keep_last_1\",\"durability\":\"transient_local\",\"task_type\":\"conditional\",\"task_frequency\":\"0\"}},{\"id\":1,\"name\":\"camera\",\"type\":\"subscriber\",\"msg\":null,\"parameters\":{\"reliability\":\"best_effort\",\"history\":\"keep_last_1\",\"durability\":\"transient_local\",\"task_type\":\"conditional\",\"task_frequency\":\"0\"}},{\"id\":2,\"name\":\"camera\",\"type\":\"subscriber\",\"msg\":null,\"parameters\":{\"reliability\":\"best_effort\",\"history\":\"keep_last_1\",\"durability\":\"transient_local\",\"task_type\":\"conditional\",\"task_frequency\":\"0\"}}]}"'


session_name="ros2_test"

tmux new-session -d -s $session_name

# Pane 1: Run command 1
tmux send-keys -t $session_name:0.0 "$cmd1" C-m

# Attach to the session
tmux attach-session -t $session_name
