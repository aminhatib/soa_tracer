#!/bin/bash

# get directory name of the script and cd to it
SCRIPT_DIR=$(dirname $0)
cd $SCRIPT_DIR/../

if [ ! -f ./bin/soatracer-asoa ] || [ ! -f ./bin/asoa-orch ]; then
    echo "Binary files cannot be found in bin/ folder. Please compile the project first!"
    exit 1
fi


SAMPLES=10

cmd1='./bin/soatracer-asoa -s '$SAMPLES' -c "{\"id\":41,\"distributed\":false,\"comm_type\":\"inter\",\"publishers\":1,\"subscribers\":2,\"services\":[{\"id\":1,\"name\":\"lidar\",\"type\":\"subscriber\",\"msg\":null,\"parameters\":{\"reliability\":\"best_effort\",\"history\":\"keep_last_1\",\"durability\":\"transient_local\",\"task_type\":\"periodic\",\"task_frequency\":\"20\"}}]}"'

cmd2='./bin/soatracer-asoa -s '$SAMPLES' -c "{\"id\":41,\"distributed\":false,\"comm_type\":\"inter\",\"publishers\":1,\"subscribers\":2,\"services\":[{\"id\":2,\"name\":\"lidar\",\"type\":\"subscriber\",\"msg\":null,\"parameters\":{\"reliability\":\"best_effort\",\"history\":\"keep_last_1\",\"durability\":\"transient_local\",\"task_type\":\"periodic\",\"task_frequency\":\"20\"}}]}"'

cmd3='./bin/soatracer-asoa -s '$SAMPLES' -c "{\"id\":41,\"distributed\":false,\"comm_type\":\"inter\",\"publishers\":1,\"subscribers\":2,\"services\":[{\"id\":1,\"name\":\"lidar\",\"type\":\"publisher\",\"msg\":{\"size\":\"5000\",\"frequency\":\"20\"},\"parameters\":{\"reliability\":\"best_effort\",\"history\":\"keep_last_1\",\"durability\":\"transient_local\",\"task_type\":\"periodic\",\"task_frequency\":\"20\"}}]}"'

cmd4='./bin/asoa-orch -c "[{\"id\":41,\"distributed\":false,\"comm_type\":\"inter\",\"publishers\":1,\"subscribers\":2,\"services\":[{\"id\":1,\"name\":\"lidar\",\"type\":\"publisher\",\"msg\":{\"size\":\"5000\",\"frequency\":\"20\"},\"parameters\":{\"reliability\":\"best_effort\",\"history\":\"keep_last_1\",\"durability\":\"transient_local\",\"task_type\":\"periodic\",\"task_frequency\":\"20\"}}]},{\"id\":41,\"distributed\":false,\"comm_type\":\"inter\",\"publishers\":1,\"subscribers\":2,\"services\":[{\"id\":1,\"name\":\"lidar\",\"type\":\"subscriber\",\"msg\":null,\"parameters\":{\"reliability\":\"best_effort\",\"history\":\"keep_last_1\",\"durability\":\"transient_local\",\"task_type\":\"periodic\",\"task_frequency\":\"20\"}}]},{\"id\":41,\"distributed\":false,\"comm_type\":\"inter\",\"publishers\":1,\"subscribers\":2,\"services\":[{\"id\":2,\"name\":\"lidar\",\"type\":\"subscriber\",\"msg\":null,\"parameters\":{\"reliability\":\"best_effort\",\"history\":\"keep_last_1\",\"durability\":\"transient_local\",\"task_type\":\"periodic\",\"task_frequency\":\"20\"}}]}]"'

session_name="asoa_test"

tmux new-session -d -s $session_name

# Create a grid
# Split pane horizontally
tmux split-window -h -t $session_name:0.0

# Split same pane vertically
tmux split-window -v -t $session_name:0.0

# Split top right pane vertically
tmux split-window -v -t $session_name:0.2

# Pane 1: Run command 1
tmux send-keys -t $session_name:0.0 "$cmd1" C-m
# Pane 2: Run command 2
tmux send-keys -t $session_name:0.1 "$cmd2" C-m
# Pane 3: Run command 3
tmux send-keys -t $session_name:0.2 "$cmd3" C-m
# Pane 4: Run command 4
tmux send-keys -t $session_name:0.3 "$cmd4" C-m

# Attach to the session
tmux attach-session -t $session_name