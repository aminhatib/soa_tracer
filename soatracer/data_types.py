from textual.widgets.data_table import RowKey
from paramiko import SSHClient
from pydantic import BaseModel


class ServiceParameters(BaseModel):
    reliability: str
    history: str
    durability: str
    task_type: str
    task_frequency: str

class ServiceMsg(BaseModel):
    size: str
    frequency: str

class ServiceSetup(BaseModel):
    name: str
    type: str
    msg: ServiceMsg | None
    parameters: ServiceParameters | None

class MachineSetup(BaseModel):
    name: str
    services: list[ServiceSetup]

class TestSetup(BaseModel):
    distributed: bool
    comm_type: str
    publishers: int
    subscribers: int
    machines: list[MachineSetup]

class TestsData(BaseModel):
    asoa: list[TestSetup]
    ros2: list[TestSetup]

class RemoteServer():
    def __init__(self, host: str, name: str = None, username: str = None, password: str = None, id: int = None) -> None:
        self.host: str = host
        self.name: str = name
        self.host_and_name: str = "{}{}".format(self.host, " (" + self.name + ")" if name else "") # host (name) if name is provided
        self.username: str = username
        self.password: str = password
        self.status: str = "offline"
        self.health: str = "-"
        self.deps: str = "-"
        self.ssh: SSHClient = None
        self.row: RowKey = None
        self.id: int = id or None

class TestCase():
    def __init__(self, setup: TestSetup, *args) -> None:
        self.setup: TestSetup = setup
        self.args = args
        self.id: int = args[0]
        self.topology: str = args[1]
        self.distributed: bool = args[2]
        self.comm_type: str = args[3]
        self.arch: str = args[4]
        self.history: str = args[5]
        self.reliability: str = args[6]
        self.durability: str = args[7]
        self.task_type: str = args[8]
        self.task_frequency: str = args[9]
        self.data_type: str = args[10]
        self.status: str = "pending"
        self.row: RowKey = None
        self.dist_setup: TestSetup = None
