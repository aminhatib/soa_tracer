import os
import sys
import platform
import subprocess
import json
import shutil

from concurrent.futures import ThreadPoolExecutor, wait
from textual import log
from textual.widget import Widget

from soatracer.data_types import RemoteServer
from soatracer.nodes.common import run_node, SOATRACER_FOLDER
from soatracer import SOATracer
from soatracer.testing import test_to_json
from soatracer.utils import resolve_path
from soatracer.widgets import LogBox
from soatracer.server import copy_file_to_server, copy_file_from_server, get_curr_time, add_to_time, get_dir
from soatracer.nodes.asoa import run_orchestrator


def check_local_deps(soatracer: SOATracer):
    for dep in soatracer.deps:
        proc = subprocess.run(f"dpkg -s {dep}", shell=True)
        if proc.returncode != 0:
            log("[ERROR]: Dependencies must be installed on the system")
            quit(1)

def build_nodes(soatracer: SOATracer) -> None:
    if platform.system() == "Windows":
        log("[ERROR]: Cannot build soatracer-nodes on windows.")
        quit(1)
    else:
        check_local_deps(soatracer)
    
    cmd = " ".join([".", "/opt/ros/iron/setup.sh", "&&", sys.executable, resolve_path("build.py"), "-b", "bin"])
    subprocess.run(cmd, shell=True)

def check_nodes_exist(soatracer: SOATracer) -> None:
    """Check if nodes exist in bin/ folder"""
    for node in soatracer.nodes:
        if not os.path.exists(resolve_path(f"bin/{node}")):
            return False
    return True

def copy_nodes(server: RemoteServer, soatracer: SOATracer) -> bool:
    asoa_local_path = "bin/soatracer-asoa"
    asoa_orch_local_path = "bin/asoa-orch"
    ros2_local_path = "bin/soatracer-ros2"
    asoa_remote_path = os.path.join(SOATRACER_FOLDER, asoa_local_path)
    asoa_orch_remote_path = os.path.join(SOATRACER_FOLDER, asoa_orch_local_path)
    ros2_remote_path = os.path.join(SOATRACER_FOLDER, ros2_local_path)

    try:
        copy_file_to_server(ssh=server.ssh, local_path=asoa_local_path, remote_path=asoa_remote_path)
        copy_file_to_server(ssh=server.ssh, local_path=asoa_orch_local_path, remote_path=asoa_orch_remote_path)
        copy_file_to_server(ssh=server.ssh, local_path=ros2_local_path, remote_path=ros2_remote_path)
        return True
    except:
        return False

def setup_lttng(server: RemoteServer, wg: Widget) -> bool:
    ssh = server.ssh
    stdin, stdout, stderr = ssh.exec_command("./soatracer/scripts/lttng.sh")
    stdout.channel.recv_exit_status()

    error = stderr.read().decode().removeprefix("Error: A session daemon is already running.\n")
    if error:
        wg.post_message(LogBox.Msg(f"LTTng couldn't be started on {server.host}\n[STDERR]: {error}", "error"))
        return False
    else:
        wg.post_message(LogBox.Msg(f"LTTng setup successfully on {server.host}!", "info"))
        return True

def stop_lttng(server: RemoteServer, wg: Widget, view: bool = False):
    ssh = server.ssh

    args = "-s"
    if view:
        args += " -v"

    # Get session name from .lttng before destroying session
    session_name = get_lttng_session(server)

    stdin, stdout, stderr = ssh.exec_command(f"./soatracer/scripts/lttng.sh {args} | tee ./soatracer/lttng_output.log")
    stdout.channel.recv_exit_status()

    error = stderr.read().decode()
    if error:
        wg.post_message(LogBox.Msg(f"LTTng couldn't be stopped on {server.host}\n[STDERR]: {error}", "error"))
        return

    log_local_path = f"results/{server.host}/lttng_output.log"
    # Copy lttng output
    copy_file_from_server(ssh=ssh, remote_path="./soatracer/lttng_output.log", local_path=log_local_path)

    traces_path = f"results/{server.host}/traces"
    # Copy traces folder from remote machine
    get_dir(ssh, "lttng-traces", traces_path, skip_exist=True)

    # last session directoy
    # Empty the directory
    shutil.rmtree(f"results/last_session/{server.host}")
    # Create new folder
    os.makedirs(f"results/last_session/{server.host}", exist_ok=True)
    # Copy current session to the last_session folder
    shutil.copytree(f"{traces_path}/{session_name}", f"results/last_session/{server.host}/{session_name}", dirs_exist_ok=True)

    wg.post_message(LogBox.Msg(f"LTTng stopped on {server.host} and LTTng output is in: {log_local_path}.\
                    \nTraces folder: {traces_path}. LTTng session name: {session_name}.", "info", debug=True, timestamp=True))

def get_lttng_session(server: RemoteServer) -> str:
    sftp = server.ssh.open_sftp()
    try:
        with sftp.open(".lttngrc") as f:
            lines: list[str] = f.readlines()
            for line in lines:
                if line.startswith("session"):
                    return line.split("=")[1].strip()

        return ""
    except Exception as e:
        log(f"[ERROR]: get_lttng_session: {e}")
        return ""
    finally:
        sftp.close()

def get_log_files(server: RemoteServer, wg: Widget):
    ssh = server.ssh

    archs = ["ros2", "asoa"]

    for arch in archs:
        log_local_path = f"results/{server.host}/log_{arch}.soatracer"

        # Copy arch log file
        copy_file_from_server(ssh=ssh, remote_path=f"./soatracer/log_{arch}.soatracer", local_path=log_local_path)

        wg.post_message(LogBox.Msg(f"Log files for {arch} were copied to: {log_local_path}.", "info", debug=True, timestamp=True))

def start_nodes_tests(soatracer: SOATracer, wg: Widget, selected_row: int = 0) -> None:
    """Starts nodes tests on the servers"""
    tests = soatracer.tests
    servers = soatracer.servers
    tests_table = wg.parent.query_one("#tests-table")

    # setup lttng on all servers
    with ThreadPoolExecutor(max_workers=len(servers)) as executor:
        futures = {executor.submit(
            setup_lttng, server, wg): server for server in servers if server.ssh is not None}

        wait(futures, return_when="ALL_COMPLETED")

        # stop lttng and return if any of the setups failed
        if any(future.result() is False for future in futures):
            futures = {executor.submit(
                stop_lttng, server, wg): server for server in servers if server.ssh is not None}

            wait(futures, return_when="ALL_COMPLETED")
            return

    skipped_tests_ids: list[int] = []

    # start actual tests
    for idx, test in enumerate(tests):
        if wg.stopped:
            break

        # Skip test rows before the selected row
        if idx < selected_row:
            continue

        # If the number of defined servers are not enough for the distributed test,
        # then skip test
        if test.distributed and len(servers) < test.setup.publishers + test.setup.subscribers:
            tests_table.update_test_status(test, "skipped")
            skipped_tests_ids.append(test.id)
            continue

        # Skip conditional tasks for now
        if test.task_type == "conditional":
            tests_table.update_test_status(test, "skipped")
            skipped_tests_ids.append(test.id)
            continue

        tests_table.update_test_status(test, "testing")

        # Get distributed service setup of the test
        # to run each publisher/subscriber as an own executable on the same machine
        if test.distributed:
            setup = test.setup
        else:
            # Use normal setup to run in the same executable
            if test.comm_type == "intra":
                setup = test.setup
            else:
                setup = test.dist_setup

        arch = test.arch
        # arch = "ros2" # for DEBUGGING since ASOA node not ready yet

        machines_count = len(setup.machines)
        if arch == "asoa":
            # Add an extra thread for the orchestrator
            machines_count += 1

        machines_config_list = test_to_json(setup, test.id)

        # Get curr time and define a time to start the tests at
        # Use the time from the main/master clock server, assuming all clocks synced
        curr_time = get_curr_time(servers[0], 0)

        servers_setups = []
        for num, machine_config in enumerate(machines_config_list):
            service_type = machine_config["services"][0]["type"]

            if service_type == "subscriber":
                # Start subscriber 1 second earlier to make sure its ready to receive
                time_shift = 1
            else:
                time_shift = 2

            start_time = add_to_time(curr_time, time_shift)

            args = [
                f"-t {start_time}",
                "-c",
                json.dumps(machine_config, separators=(",", ":")),
                f">> ./soatracer/log_{arch}.soatracer 2>&1"
            ]

            if test.distributed:
                servers_setups.append((servers[num], args, service_type))
            else:
                # If not distributed use the same server as multiple nodes
                # to run the executables on multiple ssh sessions
                servers_setups.append((servers[0], args, service_type))

        # DEBUGGING
        log(servers_setups)

        with ThreadPoolExecutor(max_workers=machines_count) as executor:
            futures = {}
            futures = {
                executor.submit(
                    run_node, server_setup[0], arch, server_setup[1], server_setup[2]
                ): server_setup[0]
                for server_setup in servers_setups
            }

            if arch == "asoa":
                orch_args = [
                    f"-t {start_time}",
                    "-c",
                    json.dumps(machines_config_list, separators=(",", ":")),
                    f">> ./soatracer/log_orch.soatracer 2>&1"
                ]
                # Run orchestrator
                futures.update(
                    {executor.submit(run_orchestrator, servers[0], orch_args): servers[0]}
                )

            wait(futures, return_when="ALL_COMPLETED")

            if any([future.result()[1] != "" for future in futures]):
                tests_table.update_test_status(test, "error")
            else:
                tests_table.update_test_status(test, "done")

    # stop lttng on all servers
    with ThreadPoolExecutor(max_workers=len(servers)) as executor:
        futures = {executor.submit(
            stop_lttng, server, wg): server for server in servers if server.ssh is not None}

        wait(futures, return_when="ALL_COMPLETED")

    wg.post_message(LogBox.Msg(f"Last LTTng session from all hosts have been copied to: results/last_session/", "info", debug=True, timestamp=True))

    with ThreadPoolExecutor(max_workers=len(servers)) as executor:
        futures = {executor.submit(
            get_log_files, server, wg): server for server in servers if server.ssh is not None}

        wait(futures, return_when="ALL_COMPLETED")

    if skipped_tests_ids != []:
        wg.post_message(LogBox.Msg(f"Skipped tests, due to not enough servers configured.", "error"))

def kill_node(server: RemoteServer):
    ssh = server.ssh

    stdin, stdout, stderr = ssh.exec_command("pgrep soatracer | xargs kill -9")

    stdout.channel.recv_exit_status()