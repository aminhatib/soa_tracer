import os
import paramiko
from paramiko import SSHClient
from textual.widget import Widget

from soatracer.server import copy_file_to_server, SOATRACER_FOLDER, to_posix_path, sudo_execute
from soatracer.widgets import LogBox
from soatracer.data_types import RemoteServer
# from soatracer.nodes.common import run_node


def copy_ros2_deps(ssh: SSHClient) -> None:
    files = [
        "scripts/install_ros2.sh",
    ]
    for file in files:
        remote_path = to_posix_path(os.path.join(SOATRACER_FOLDER, file))
        copy_file_to_server(ssh=ssh, local_path=file, remote_path=remote_path)

def install_ros2(wg: Widget, server: RemoteServer) -> None:
    wg.post_message(LogBox.Msg(f"Copying ROS2 install script on {server.host}", "info", debug=True))
    copy_ros2_deps(server.ssh)

    path_to_script = to_posix_path(os.path.join(SOATRACER_FOLDER, "scripts", "install_ros2.sh"))
    cmd = "bash {}".format(path_to_script)
    stdin, stdout, stderr = sudo_execute(server, cmd)
    stdout.channel.recv_exit_status()

    error = stderr.read().decode().removeprefix(f"[sudo] password for {server.username}:").strip()
    if error and not error.startswith("WARNING"):
        wg.post_message(LogBox.Msg(f"Installing ROS2 failed on {server.host}\n[STDERR]: {error}", "error"))
    else:
        wg.post_message(LogBox.Msg(f"Successfully installed ROS2 on {server.host}", "info"))

# def run_ros2_node(ssh: SSHClient, args: list[str]) -> None:
#     stdin, stdout, stderr = run_node(ssh, "ros2", args)
#     stdout.channel.recv_exit_status()
#     print(stdout.read().decode())
