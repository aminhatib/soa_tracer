import os
import json
import threading
import time
from paramiko import SSHClient, SFTPClient
import paramiko
from textual.widget import Widget
from textual import log

from soatracer.utils import get_quoted_string
from soatracer.data_types import RemoteServer
from soatracer.server import copy_file_to_server, SOATRACER_FOLDER, to_posix_path, sudo_execute
from soatracer.constants import ROS2_DISTRO

SAMPLES = 200


def copy_build_to_server(ssh: SSHClient) -> None:
    copy_file_to_server(ssh=ssh, local_path="build.py", remote_path="soatracer/build.py")

def run_build(ssh: SSHClient, args: list[str]) -> None:
    cmd = "{}"
    arguments = " ".join(args)
    # return
    # server.ssh.exec_command(cmd.format(*args))
    stdin, stdout, stderr = ssh.exec_command(cmd.format(arguments))
    stdout.channel.recv_exit_status()

def run_node(server: RemoteServer, node_name: str, args: list[str], type: str, trace: bool = True):
    samples_count = SAMPLES

    ssh = server.ssh
    cmd = "{}"

    if node_name == "asoa":
        node_path = to_posix_path(os.path.join(SOATRACER_FOLDER, "bin", "soatracer-asoa"))
    elif node_name == "ros2":
        cmd = f"source /opt/ros/{ROS2_DISTRO}/setup.sh && "
        cmd = cmd + "{}"
        node_path = to_posix_path(os.path.join(SOATRACER_FOLDER, "bin", "soatracer-ros2"))
    else:
        return None

    # args = [json.dumps(arg) for arg in args]
    # args = " ".join(args)
    args = get_quoted_string(args)
    # DEBUGGING
    log(f"server: {server.host}, cmd run: {cmd.format(f'./{node_path} --samples {samples_count} {args}')}")
    stdin, stdout, stderr = ssh.exec_command(cmd.format(f"./{node_path} --samples {samples_count} {args}"))

    stdout.channel.recv_exit_status()

    output = stdout.read().decode()
    error = stderr.read().decode()
    log(f"server: {server.host}, ______stdout______\n" + output)
    log(f"server: {server.host}, ______stderr______\n" + error)

    return (output, error)
