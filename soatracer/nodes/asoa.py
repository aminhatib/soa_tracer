import os
from paramiko import SSHClient
from textual.widget import Widget
from textual import log

from soatracer.server import copy_file_to_server, SOATRACER_FOLDER, to_posix_path, sudo_execute
from soatracer.widgets import LogBox
from soatracer.data_types import RemoteServer
from soatracer.constants import ASOA_VERSION
from soatracer.utils import get_quoted_string


def copy_asoa_deps(ssh: SSHClient) -> None:
    files = [
        "scripts/install_asoa.sh",
        f"deps/asoa/asoa_security-{ASOA_VERSION}.deb",
        f"deps/asoa/asoa_core-{ASOA_VERSION}.deb",
        f"deps/asoa/asoa_orchestrator-{ASOA_VERSION}.deb",
        f"deps/asoa/asoa_utilities-{ASOA_VERSION}.deb",
    ]
    for file in files:
        remote_path = to_posix_path(os.path.join(SOATRACER_FOLDER, file))
        copy_file_to_server(ssh=ssh, local_path=file, remote_path=remote_path)

def install_asoa(wg: Widget, server: RemoteServer) -> None:
    copy_asoa_deps(server.ssh)

    path_to_script = to_posix_path(os.path.join(SOATRACER_FOLDER, "scripts", "install_asoa.sh"))
    cmd = "bash {}".format(path_to_script)
    stdin, stdout, stderr = sudo_execute(server, cmd)
    stdout.channel.recv_exit_status()

    error = stderr.read().decode().removeprefix(f"[sudo] password for {server.username}:").strip()
    if error:
        wg.post_message(LogBox.Msg(f"Installing ASOA failed on {server.host}\n[STDERR]: {error}", "error"))
    else:
        wg.post_message(LogBox.Msg(f"Successfully installed ASOA on {server.host}", "info"))

def run_orchestrator(server: RemoteServer, args: list[str]):
    ssh = server.ssh
    cmd = "{}"

    node_path = to_posix_path(os.path.join(SOATRACER_FOLDER, "bin", "asoa-orch"))

    args = get_quoted_string(args)

    cmd = cmd.format(f'./{node_path} {args}')
    # DEBUGGING
    log(f"server: {server.host}, cmd run: {cmd}")
    stdin, stdout, stderr = ssh.exec_command(cmd)

    stdout.channel.recv_exit_status()

    output = stdout.read().decode()
    error = stderr.read().decode()
    log("______stdout_____\n" + output)
    log("______stderr_____\n" + error)

    return (output, error)