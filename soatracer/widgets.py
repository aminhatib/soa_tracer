import datetime

from textual.widgets import Static, Label, RichLog
from textual.message import Message
from textual.app import ComposeResult
from textual import log

from soatracer import SOATracer

class LogBox(Static):
    """A logger field"""

    class Msg(Message):
        """
        Status message
        Types: [error, info]
        """
        def __init__(self, msg: str, type: str = "", debug: bool = False, timestamp: bool = False) -> None:
            super().__init__()

            self.value = ""

            if timestamp:
                current_timestamp = datetime.datetime.now().strftime("%H:%M:%S")
                self.value = f"[{current_timestamp}]"

            type = type.lower()
            if type == "info":
                self.value += f"[bold blue][INFO][/bold blue]: {msg}"
            elif type == "error":
                self.value += f"[bold red][ERROR][/bold red]: {msg}"
            else:
                self.value += msg
            
            # Log using textual logger for debugging
            if debug:
                log(msg)

    def __init__(self, soatracer: SOATracer) -> None:
        super().__init__()

        self.soatracer = soatracer

    def compose(self) -> ComposeResult:
        yield Label("[b][u]LOG:[/u][/b]", id="log-label")
        yield RichLog(id="logger", markup=True, highlight=True)
