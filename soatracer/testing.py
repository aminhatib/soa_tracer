import json

from soatracer.data_types import TestSetup

def test_to_json(test: TestSetup, test_id: int) -> list[dict[str]]:
    """
    Returns a list of machine setups.
    Based of the example in machine_config.json
    """
    pubs_count = test.publishers
    subs_count = test.subscribers

    pubs_id = 1
    subs_id = 1

    names_idx = 0
    pubs_names = []
    # Get publishers names
    for machine in test.machines:
        for service in machine.services:
            if service.type == "publisher":
                pubs_names.append(service.name)

    test_json_list = []
    for machine in test.machines:
        test_json = {
            "id": test_id,
            "distributed": test.distributed,
            "comm_type": test.comm_type,
            "publishers": pubs_count,
            "subscribers": subs_count,
            "services": []
        }

        for service in machine.services:
            service_copy = service.model_copy()

            if service.type == "subscriber":
                # Depending on the situation subscribers get the right service name, according to publishers,
                # to be able to subscribe to the right topic
                if pubs_count < subs_count:
                    service_copy.name = pubs_names[names_idx]
                elif pubs_count > subs_count:
                    # In this case, we create new services to match the publishers count and names on the same machine
                    services_dicts = []

                    for i in range(0, pubs_count):
                        service_copy = service.model_copy()

                        service_copy.name = pubs_names[i]

                        service_dict = service_copy.model_dump()
                        service_dict["id"] = subs_id
                        subs_id += 1

                        services_dicts.append(service_dict)

                    test_json["services"].extend([service for service in services_dicts])
                    continue
                else:
                    service_copy.name = pubs_names[names_idx]
                    names_idx += 1

            service_dict = service_copy.model_dump();
            if service.type == "subscriber":
                service_dict["id"] = subs_id
                subs_id += 1
            else:
                service_dict["id"] = pubs_id
                pubs_id += 1
            test_json["services"].append(service_dict)

        # test_json_list.append(json.dumps(test_json))
        test_json_list.append(test_json)

    return test_json_list