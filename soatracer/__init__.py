from textual.app import App
from pandas import DataFrame

from soatracer.config import read_tests, read_servers
from soatracer.utils import generate_table, generate_tests, resolve_path
from soatracer.data_types import RemoteServer, TestCase, TestsData, TestSetup
from soatracer.constants import ROS2_DISTRO

class SOATracer():
    def __init__(self, args):
        self.app: App = None

        self.install_deps = [
            "lttng-tools", "liblttng-ust-dev", "linuxptp",
            ]
        self.deps = self.install_deps + [
            "asoa_core", "asoa_orchestrator", "asoa_security", "asoa_utilities",
            f"ros-{ROS2_DISTRO}-ros-base",
            ]
        # self.deps = ["neovim"]
        self.test_deps = ["neovim"]
        self.nodes = [
            "soatracer-ros2",
            "soatracer-asoa",
            "asoa-orch",
            ]

        self.df: DataFrame = None
        self.tests_data: TestsData = None
        self.servers_data: any = None
        try:
            self.df: DataFrame = generate_tests.get_tests_df()
            self.tests_data = TestsData(**read_tests(resolve_path("tests.json"))["tests"])
        except:
            # generate table then tests if tests are not available
            generate_table.generate_table()
            generate_tests.generate_tests()
            self.df: DataFrame = generate_tests.get_tests_df()
            self.tests_data = TestsData(**read_tests(resolve_path("tests.json"))["tests"])

        if args.servers_config:
            try:
                self.servers_data = read_servers(args.servers_config)
            except Exception as e:
                raise e
        else:
            try:
                self.servers_data = read_servers(resolve_path("servers.yml"))
            except:
                print("[ERROR]: `servers.yml` is missing. Use `servers_sample.yml` as a template.")
                quit(1)

        self.servers: list[RemoteServer] = [RemoteServer(server["host"], server["name"] if "name" in server else "", server["username"], server["password"], idx+1) for idx, server in enumerate(self.servers_data["servers"])]

        self.tests: list[TestCase] = self.create_test_cases()


    def get_server(self, name: str) -> RemoteServer:
        """Get RemoteServer using name"""
        for server in self.servers:
            if server.host == name:
                return server
        return None

    def create_test_cases(self) -> list[TestCase]:
        """Creates tests cases (TestCase) from the DataFrame"""
        tests: list[TestCase] = []
        ros2_count = 0
        asoa_count = 0

        distributed_tests: list[TestCase] = []

        for idx, row in self.df.iterrows():
            ls = [idx+1, *list(row)]

            if row["architecture"] == "asoa":
                setup = self.tests_data.asoa[asoa_count]
                asoa_count += 1
            else:
                setup = self.tests_data.ros2[ros2_count]
                ros2_count += 1

            test_case = TestCase(setup, *ls)
            tests.append(test_case)

            if setup.distributed:
                distributed_tests.append(test_case)

        # Copy distributed setups to non distributed setups
        # to run each publisher/subscriber as an own executable
        for test in tests:
            if not test.distributed:
                for d_test in distributed_tests:
                    if (
                        test.topology == d_test.topology
                        and test.history == d_test.history
                        and test.reliability == d_test.reliability
                        and test.durability == d_test.durability
                        and test.task_type == d_test.task_type
                        and test.task_frequency == d_test.task_frequency
                        and test.data_type == d_test.data_type
                    ):
                        # Make sure to copy the object to be able to change it
                        test.dist_setup = d_test.setup.model_copy()
                        # Set the distributed value
                        test.dist_setup.distributed = False
                        # Make sure comm_type is like the original test
                        test.dist_setup.comm_type = test.comm_type
                        break

        return tests