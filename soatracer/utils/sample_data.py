sample_data = {
    "imu": {
        "frequency": "200",
        "size": "1000",
    },
    "lidar": {
        "frequency": "20",
        "size": "3500000",
    },
    "radar": {
        "frequency": "20",
        "size": "100000",
    },
    "camera": {
        "frequency": "50",
        "size": "800000",

    },
}

data_types = list(sample_data.keys())

sample_frequency = {sample:sample_data[sample]["frequency"] for sample in sample_data}
sample_size = {sample:sample_data[sample]["size"]for sample in sample_data}
