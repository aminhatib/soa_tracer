from pathlib import Path
import os
import re

def get_root_dir() -> Path:
    """Get path to the project root directory"""
    return Path(__file__).parent.parent.parent

def resolve_path(path: str) -> Path:
    """Return path appended to the project's root directory"""
    return os.path.join(get_root_dir(), path)

def resolve_paths(path_list: list[str]) -> list[str]:
    """Return list of paths appended to the project's root directory"""
    result = []
    for path in path_list:
        result.append(resolve_path(path))

    return result

def to_posix_path(path: str) -> str:
    return path.replace("\\", "/")

def escape_quotes(string: str) -> str:
    """Return the same string with all quotes escaped in it"""
    result = string
    result = re.sub(r'"', r'\"', result)
    result = re.sub(r"'", r"\'", result)
    return result

def get_quoted_string(strings: str | list[str], all=False) -> str:
    """
    From a list of strings, return quoted string with all quotes in the string escaped.
    @all can be specified, to quote each string. Otherwise it will only quote the strings that has ' or " in it
    """
    if isinstance(strings, str):
        escaped_strings = escape_quotes(strings)
        return f'"{escaped_strings}"'
    else:
        escaped_strings = []
        for string in strings:
            escaped_strings.append(escape_quotes(string))

    if all:
        quoted_string = " ".join([fr'"{item}"' for item in escaped_strings])
    else:
        quoted_string = " ".join([fr'"{item}"' if ('"' in item) or ("'" in item) else item for item in escaped_strings])
        
    return quoted_string