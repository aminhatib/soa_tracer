import itertools, pandas
from soatracer.utils import resolve_path
from soatracer.utils.sample_data import sample_frequency, data_types


tests_columns = {
    "topology": ["1-1", "1-2", "2-1"],
    "distributed": [True, False],
    "comm_type": ["inter", "intra"],
    "architecture": ["asoa", "ros2"],
    "history": ["keep_last_1", "keep_all"],
    "reliability": ["best_effort", "reliable"],
    "durability": ["transient_local", "volatile"],
    "task_type": ["periodic", "conditional"],
    "task_frequency": sample_frequency.values(),
    "data_type": data_types,
}

qos_combos = [("keep_all", "reliable", "transient_local"), ("keep_last_1", "best_effort", "volatile")]

def generate_combinations(arrays):
    # Generate all combinations of arrays
    combinations = list(itertools.product(*arrays))

    # Filter combinations with exceptions
    filtered_combinations = []
    for combo in combinations:
        topology = combo[0]
        distributed = combo[1]
        task_type = combo[-3]
        task_frequency = combo[-2]
        data_type = combo[-1]

        history = combo[4]
        reliability = combo[5]
        durability = combo[6]

        combo = list(combo)

        if (history, reliability, durability) not in qos_combos:
            continue

        if distributed:
            combo[2] = "-"

        if task_type == "conditional":
            combo[-2] = "-"
            filtered_combinations.append(tuple(combo))
        elif sample_frequency[data_type] == task_frequency:
            filtered_combinations.append(tuple(combo))
        else:
            continue

    return filtered_combinations

def generate_table():
    df = pandas.DataFrame(generate_combinations(list(tests_columns.values())), columns=tests_columns.keys())
    df = df.drop_duplicates()
    df = df.sort_values(by=["architecture", "topology", "task_type"])
    df.to_excel(resolve_path("generated_tests.xlsx"), index=False)

def main():
    generate_table()

if __name__ == "__main__":
    main()