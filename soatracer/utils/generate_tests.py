import pandas, json, re
from soatracer.utils.generate_table import tests_columns
from soatracer.utils import resolve_path
from soatracer.utils.sample_data import sample_size, sample_frequency

table_file = "generated_tests.xlsx"

def parse_topology(top: str) -> tuple[int, int]:
    '''
    Return (publisher, subscriber) as a tuple from string (e.g. 1-4)
    '''
    (p, s) = top.split('-')
    return (int(p), int(s))

def parse_task_type(task: str) -> tuple[str, str]:
    '''
    Return (tast_type, architecture) extracted from input str
    '''
    typ = task.split(' ')[0]

    result = re.search(r'\((.*)\)', task)
    arch = result.group(1) if result else ''

    return (typ, arch)

def compose_test(row: dict[str, any]):
    '''
    Returns a dictionary of the machines extracted from the test row from input.\\
    Composes the test logic based on the given configuration.

    For example: setup: (1-1, distributed, ...), in this case, we generate a test\
    with two machines, each machine has one service.
    '''
    machine_count = 0
    task_frequency = row["task_frequency"]
    if task_frequency == "-":
       task_frequency = "0"

    (pub, sub) = parse_topology(row["topology"])
    parameters = {
        "reliability": row["reliability"],
        "history": row["history"],
        "durability": row["durability"],
        "task_type": row["task_type"],
        "task_frequency": task_frequency,
    }
    # Debug
    # print((pub,sub))
    service_count = pub + sub
    distributed = row["distributed"]
    comm_type = row["comm_type"]
    data_type = row["data_type"]

    if distributed:
        machine_count = service_count 
        service_count = 1
    else:
        machine_count = 1
        
    test_data = {}
    test_data["distributed"] = distributed
    test_data["comm_type"] = comm_type
    test_data["publishers"] = pub
    test_data["subscribers"] = sub
    test_data["machines"] = []

    # Set machine attributes
    for i in range(machine_count):
        machine = {}
        machine["name"] = f"machine_{i+1}"

        services = []

        if distributed:
            service = {}

            # Add publisher services first
            if i + 1 <= pub:
                sample_idx = machine_count - pub - 1 + i
                
                # In case there is no specific data_type we iterate over the list
                # in case of n-1 (pubs-subs), otherwise use the specified one
                if data_type == "-":
                    service["name"] = list(sample_size)[sample_idx]
                else:
                    service["name"] = data_type

                service["type"] = "publisher"
                service["msg"] = {}
                msg = service["msg"]

                # In case there is no specific data_type we iterate over the list
                # in case of n-1 (pubs-subs), otherwise use the specified one
                if data_type == "-":
                    sample_size_list = list(sample_size.values())
                    sample_frequency_list = list(sample_frequency.values())
                    msg["size"] = sample_size_list[sample_idx]
                    msg["frequency"] = sample_frequency_list[sample_idx]
                else:
                    msg["size"] = sample_size[data_type]
                    msg["frequency"] = sample_frequency[data_type]
            else:
                service["name"] = f"service_{i+1}"
                service["type"] = "subscriber"
                service["msg"] = None
            
            service["parameters"] = parameters
            services.append(service)

        else:
            for j in range(service_count):

                service = {}

                # Add publisher services first
                if j + 1 <= pub:
                    sample_idx = machine_count - pub - 1 + j

                    # In case there is no specific data_type we iterate over the list
                    # in case of n-1 (pubs-subs), otherwise use the specified one
                    if data_type == "-":
                        service["name"] = list(sample_size)[sample_idx]
                    else:
                        service["name"] = data_type

                    service["type"] = "publisher"
                    service["msg"] = {}
                    msg = service["msg"]

                    # In case there is no specific data_type we iterate over the list
                    # in case of n-1 (pubs-subs), otherwise use the specified one
                    if data_type == "-":
                        sample_size_list = list(sample_size.values())
                        sample_frequency_list = list(sample_frequency.values())
                        msg["size"] = sample_size_list[sample_idx]
                        msg["frequency"] = sample_frequency_list[sample_idx]
                    else:
                        msg["size"] = sample_size[data_type]
                        msg["frequency"] = sample_frequency[data_type]
                else:
                    service["name"] = f"service_{j+1}"
                    service["type"] = "subscriber"
                    service["msg"] = None

                service["parameters"] = parameters
                
                services.append(service)

        machine["services"] = services

        test_data["machines"].append(machine)

    return test_data


def get_tests_df():
    """Return dataframe of the excel file of the table"""
    data = pandas.read_excel(resolve_path(table_file))
    df = pandas.DataFrame(data)
    return df

def generate_tests():
    df = get_tests_df()

    test_data = {
        "tests": {
            architecture:[] for architecture in tests_columns["architecture"]
        }
    }
    
    json_obj: str
    json_obj_human: str
    # Iterate over the dataframe read from the generated table file
    for idx, row in df.iterrows():
        row = dict(row)
        architecture = row["architecture"]
        test_data["tests"][architecture].append(compose_test(row))
        json_obj = json.dumps(test_data)
        json_obj_human = json.dumps(test_data, indent=2)
    
    # Writing to json file
    with open(resolve_path("tests.json"), "w") as outfile:
        outfile.write(json_obj)

    # Writing to json file
    with open(resolve_path("tests_for_humans.json"), "w") as outfile:
        outfile.write(json_obj_human)

def main():
    generate_tests()

if __name__ == "__main__":
    main()