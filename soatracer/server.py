import os
import time
import stat
import datetime
from datetime import datetime, timedelta

from textual import log
from paramiko import SSHClient, SFTPClient, SSHException
from paramiko.channel import ChannelStdinFile, ChannelFile, ChannelStderrFile
from textual.widget import Widget

from soatracer.utils import resolve_path, to_posix_path
from soatracer.data_types import RemoteServer
from soatracer import SOATracer
from soatracer.widgets import LogBox


SOATRACER_FOLDER = "soatracer"

def sudo_execute(server: RemoteServer, cmd: str) -> tuple[ChannelStdinFile, ChannelFile, ChannelStderrFile]:
    """Execute command using sudo, given a remote server"""
    log(f"sudo_execute: on server: {server.host}, cmd: {cmd}")
    ssh = server.ssh
    stdin, stdout, stderr = ssh.exec_command(f"sudo -S {cmd}")
    stdin.write(f"{server.password}\n")
    stdin.flush()
    # DEBUG
    # log(f"SUDO: {server.host}\n STDOUT: {stdout.read().decode()}\n STDERR: {stderr.read().decode()}")
    return (stdin, stdout, stderr)

def copy_file_to_server(server: RemoteServer = None, ssh: SSHClient = None, local_path: str = None, remote_path: str = None) -> None:
    """Copy file to server, and create any directory missing in the path."""
    sftp = ssh.open_sftp()
    if not remote_path:
        remote_path = local_path

    remote_path = to_posix_path(remote_path)
    # Create directories needed to the path
    mkdirs(sftp, remote_path)

    # Resolve path to root directory of the project
    local_path = resolve_path(local_path)
    try:
        sftp.put(local_path, remote_path)
        # Get the local file permissions
        st = os.stat(local_path)

        # Set the file permissions on the remote file
        sftp.chmod(remote_path, st.st_mode)
    except FileNotFoundError as e:
        log(f"[ERROR]: file {local_path} was not found on local system")
        raise e
    finally:
        sftp.close()

def copy_file_from_server(server: RemoteServer = None, ssh: SSHClient = None, remote_path: str = None, local_path: str = None) -> None:
    """Copy file to server, and create any directory missing in the path."""
    sftp = ssh.open_sftp()
    if not local_path:
        local_path = remote_path

    remote_path = to_posix_path(remote_path)
    # Create directories needed to the path
    os.makedirs(os.path.dirname(local_path), exist_ok=True)

    # Resolve path to root directory of the project
    local_path = resolve_path(local_path)
    try:
        sftp.get(remote_path, local_path)

        # Get the remote file permissions
        st = sftp.stat(remote_path)

        # Set the file permissions on the local file
        os.chmod(local_path, st.st_mode)
    except FileNotFoundError as e:
        log(f"[ERROR]: file {remote_path} was not found on remote system")
        raise e
    finally:
        sftp.close()

def put_dir(ssh: SSHClient, local_path: str, remote_path: str) -> None:
    """Transfer the folder and its content to the remote machine"""
    sftp = ssh.open_sftp()

    # Create subdirectories
    mkdirs(sftp, remote_path)
    try:
        sftp.mkdir(remote_path)
    except:
        pass

    for item in os.listdir(local_path):
        src = os.path.join(local_path, item)
        dst = to_posix_path(os.path.join(remote_path, item))

        if os.path.isfile(src):
            sftp.put(src, dst)
        elif os.path.isdir(src):
            put_dir(sftp, src, dst)

    sftp.close()

def get_dir(ssh: SSHClient, remote_path: str, local_path: str, skip_exist: bool = False) -> None:
    """Transfer the folder and its content from the remote machine"""
    sftp = ssh.open_sftp()
    # Create subdirectories
    local_path = resolve_path(local_path)
    os.makedirs(local_path, exist_ok=True)

    for item in sftp.listdir(to_posix_path(remote_path)):
        src = to_posix_path(os.path.join(remote_path, item))
        dst = os.path.join(local_path, item)

        # skip if the folder/file exists
        if skip_exist:
            if os.path.exists(dst):
                continue

        if stat.S_ISDIR(sftp.stat(src).st_mode):
            get_dir(ssh, src, dst)
        else:
            try:
                sftp.get(src, dst)
            except FileNotFoundError as e:
                log(f"[ERROR]: file {remote_path} was not found on remote system")
                raise e
    
    sftp.close()

def mkdirs(sftp: SFTPClient, path: str) -> None:
    """Recursively create directory on the remote machine"""
    dir_list = to_posix_path(os.path.dirname(path)).split("/")
    curr_dir = ""
    for dir in dir_list:
        curr_dir = to_posix_path(os.path.join(curr_dir, dir))
        try:
            sftp.mkdir(curr_dir)
        except:
            continue

def get_curr_time(server: RemoteServer, add_seconds: int = 0) -> str:
    ssh = server.ssh
    stdin, stdout, stderr = ssh.exec_command("date +'%T'")
    stdout.channel.recv_exit_status()

    curr_time = stdout.read().decode().strip()

    return add_to_time(curr_time, add_seconds)

def add_to_time(time: str, add_seconds: int):
    if add_seconds == 0:
        return time

    time_obj = datetime.strptime(time, "%H:%M:%S")
    time_obj += timedelta(seconds=add_seconds)

    return time_obj.strftime("%H:%M:%S")

def sync_clocks(server: RemoteServer, master: bool = False) -> bool:
    interface = "eno1"
    sync_time = "25"

    cmd = f"bash ./soatracer/scripts/sync_clock.sh -i {interface} -s {sync_time}"
    if master:
        cmd = f"{cmd} -m"

    stdin, stdout, stderr = sudo_execute(server, cmd + " > ./soatracer/sync_log.soatracer")
    stdout.channel.recv_exit_status()

    error = stderr.read().decode().removeprefix(f"[sudo] password for {server.username}:").strip()
    if error:
        return False
    else:
        return True

def install_deps_scripts():
    pass

def copy_scripts(ssh: SSHClient) -> None:
    files = [
        "scripts/install_lttng.sh",
        "scripts/gen_traces.sh",
        "scripts/lttng.sh",
        "scripts/sync_clock.sh",
    ]
    for file in files:
        remote_path = to_posix_path(os.path.join(SOATRACER_FOLDER, file))
        copy_file_to_server(ssh=ssh, local_path=file, remote_path=remote_path)

def install_lttng(wg: Widget, server: RemoteServer) -> None:
    copy_scripts(server.ssh)

    path_to_script = to_posix_path(os.path.join(SOATRACER_FOLDER, "scripts", "install_lttng.sh"))
    cmd = "bash {}".format(path_to_script)
    stdin, stdout, stderr = sudo_execute(server, cmd)
    stdout.channel.recv_exit_status()

    error = stderr.read().decode().removeprefix(f"[sudo] password for {server.username}:").strip()
    if error:
        wg.post_message(LogBox.Msg(f"Installing LTTng failed on {server.host}\n[STDERR]: {error}", "error"))
    else:
        wg.post_message(LogBox.Msg(f"Successfully installed LTTng on {server.host}", "info"))


def check_connection(soatracer: SOATracer, server: RemoteServer) -> None:
    """
    Checks every 5 seconds if connection is still alive, otherwise exception raised and connection closed
    """
    ssh = server.ssh
    while True:
        try:
            stdin, stdout, stderr = ssh.exec_command('echo hello')
            stdin.flush()
            stdout.channel.recv_exit_status()
        except SSHException as e:
            log(f"SSH Error: {e}")
            break
        except Exception as e:
            log(f"Error: {e}")
            break

        time.sleep(5)

    ssh.close()

def check_stderr(stdin, stdout, stderr):
    '''
    Checks if there is any errors while executing the command on the remote
    Raises Exception if true
    '''
    if stderr.read().decode() != "":
        raise Exception("Installing dependencies failed")


def test_conn(soatracer: SOATracer, server: RemoteServer):
    _stdin, stdout, _stderr = sudo_execute(server, "touch test.soatracer")
    stdout.channel.recv_exit_status()

    return True