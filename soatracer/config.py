import json
import yaml

def read_tests(path):
    try:
        with open(path, "r") as f:
            data = json.load(f)
        return data
    except Exception as e:
        raise e

def read_servers(path):
    try:
        with open(path, "r") as f:
            data = yaml.load(f, yaml.FullLoader)
        return data
    except Exception as e:
        raise e
