from concurrent.futures import ThreadPoolExecutor

from textual.screen import ModalScreen
from textual.widgets import Label, Button
from textual.containers import Grid
from textual.app import ComposeResult
from textual import work

from soatracer import SOATracer
from soatracer.server import test_conn

class QuitScreen(ModalScreen):
    """Screen with a dialog to quit."""

    BINDINGS = [
        ("ctrl+q, ctrl+c, escape", "app.pop_screen", "Quit")
    ]

    def __init__(self, soatracer: SOATracer) -> None:
        super().__init__()

        self.soatracer = soatracer

    def compose(self) -> ComposeResult:
        yield Grid(
            Label("Are you sure you want to quit?\n(This may take a while if tests are running)", id="quit-question"),
            Button("Quit", variant="error", id="quit"),
            Button("Cancel", variant="primary", id="cancel"),
            id="quit-dialog",
        )

    async def on_button_pressed(self, event: Button.Pressed) -> None:
        if event.button.id == "quit":
            # Stop all background task before quitting
            default_screen = self.app.main_screen
            control_panel = default_screen.query_one("#control-panel")
            control_panel.stop_tests()
            if control_panel.tests_worker:
                await control_panel.tests_worker.wait()

            self.close_connections()
            self.app.exit()
        else:
            self.app.pop_screen()
    
    def close_connections(self) -> None:
        servers = self.soatracer.servers
        for server in servers:
            if server.ssh is not None:
                server.ssh.close()

class TestConnScreen(ModalScreen):
    """Screen with a dialog to quit."""
    BINDINGS = [
        ("ctrl+q, ctrl+c, escape", "app.pop_screen", "Quit")
    ]

    def __init__(self, soatracer: SOATracer) -> None:
        super().__init__()

        self.soatracer = soatracer

    def compose(self) -> ComposeResult:
        yield Grid(
            Label("This will create an empty 'test.soatracer' file (using `touch`) on all connected server\
                  \n[b]NOTE: You have to press the 'Connect' first.[/b]", id="test-conn-question"),
            Button("Test Connection", variant="warning", id="test-conn-button", disabled=True),
            Button("Cancel", variant="primary", id="cancel"),
            id="test-conn-dialog",
        )

    def on_mount(self) -> None:
        if any(server.ssh is not None for server in self.soatracer.servers):
            test_button = self.query_one("#test-conn-button")
            test_button.disabled = False

    def on_button_pressed(self, event: Button.Pressed) -> None:
        if event.button.id == "cancel":
            self.app.pop_screen()
        elif event.button.id == "test-conn-button":
            self.action_test_connections()

    @work(thread=True)
    def action_test_connections(self) -> None:
        servers = [server for server in self.soatracer.servers if server.ssh is not None]
        
        with ThreadPoolExecutor(max_workers=len(servers)) as executor:
            {executor.submit(
                test_conn, self.soatracer, server): server for server in servers}