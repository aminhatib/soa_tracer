import time
from concurrent.futures import ThreadPoolExecutor, wait
from textual.app import App, ComposeResult
from textual.widgets import (
    Header,
    Footer,
    Button,
    Static,
    DataTable,
    TabbedContent,
    TabPane,
    Select,
    Rule,
    Checkbox
    )
from textual.containers import (
    Horizontal,
    Center,
    Middle,
    )
from textual.screen import Screen
from textual import on, log, work
from rich.text import Text
import paramiko
from paramiko import SSHClient
import argparse

from soatracer import SOATracer
from soatracer.server import sudo_execute, check_connection, copy_scripts, sync_clocks
from soatracer.utils.generate_table import tests_columns, generate_table
from soatracer.utils.generate_tests import generate_tests
from soatracer.data_types import RemoteServer, TestCase
from soatracer.nodes import ros2, asoa, start_nodes_tests, copy_nodes, check_nodes_exist
from soatracer.widgets import LogBox
from soatracer.screens import TestConnScreen, QuitScreen


def parse_args():
    parser = argparse.ArgumentParser(description="SOATracer, Service Oriented Architecture Latency Testing Tool.")
    parser.add_argument(
        "-s", "--servers-config", help="config file for the servers"
    )
    return parser.parse_args()

#########################################################################################

class ServersTable(Static):
    SERVERS_COLUMNS = [
        "id", "host (name)", "status", "health", "dependencies"
    ]

    def __init__(self, soatracer: SOATracer) -> None:
        super().__init__()

        self.soatracer = soatracer
        self.columns = {}
        self.rows = []

    def compose(self) -> ComposeResult:
        yield DataTable()

    def on_mount(self) -> None:
        servers = self.soatracer.servers
        table = self.query_one(DataTable)
        table.cursor_type = "row"

        self.columns = {col: key for col, key in zip(self.SERVERS_COLUMNS, table.add_columns(*self.SERVERS_COLUMNS))}

        for server in servers:
            server.row = table.add_row(
                server.id,
                server.host_and_name,
                Text(server.status, style="italic #ac2020"),
                Text(server.health),
                Text(server.deps)
                )

    def update_connect_status(self, server: RemoteServer) -> None:
        table = self.query_one(DataTable)
        table.update_cell(server.row, self.columns["status"], Text(server.status, style="italic #0c6600"))

    def update_deps_status(self, server: RemoteServer, installed: bool) -> None:
        table = self.query_one(DataTable)

        style = ""
        if installed:
            style="italic #0c6600"
        else:
            style="italic #ac2020"
        table.update_cell(server.row, self.columns["dependencies"], Text(server.deps, style=style))


class TestsTable(Static):
    TESTS_COLUMNS = [
        "id",
        "status",
        *tests_columns.keys(),
    ]

    def __init__(self, soatracer: SOATracer, id: str | None = None) -> None:
        super().__init__(id=id)

        self.soatracer = soatracer
        self.columns = {}
        self.rows = []

    def compose(self) -> ComposeResult:
        yield DataTable(id="tests-data-table")

    def on_mount(self) -> None:
        table = self.query_one(DataTable)
        table.cursor_type = "row"
        self.columns = {col: key for col, key in zip(self.TESTS_COLUMNS, table.add_columns(*self.TESTS_COLUMNS))}

        tests = self.soatracer.tests
        for test in tests:
            test.row = table.add_row(
                test.id,
                Text(test.status, style="italic blue"),
                test.topology,
                test.distributed,
                test.comm_type,
                test.arch,
                test.history,
                test.reliability,
                test.durability,
                test.task_type,
                test.task_frequency,
                test.data_type,
            )

        self.rows = [test.row for test in tests]

    def update_test_status(self, test: TestCase, status: str) -> None:
        table = self.query_one(DataTable)

        style = ""
        if status == "done":
            style = "italic #0c6600"
        elif status == "testing":
            style = "italic #eacd61"
        elif status == "error":
            style = "italic #ac2020"
        text = Text(status, style=style)

        table.update_cell(test.row, self.columns["status"], text)

class ControlPanel(Static):
    def __init__(self, soatracer: SOATracer, id: str | None = None) -> None:
        super().__init__(id=id)

        self.soatracer = soatracer
        self.deps_installed = False

        self.start_from_selection = False
        self.stopped = False
        self.tests_worker = None

    def compose(self) -> ComposeResult:
        with Horizontal(id="control-buttons"):
            yield Button("Connect", id="connect", variant="default", classes="control-button", disabled=True)
            yield Button("Install Dependencies", id="install-deps", variant="warning", classes="control-button", disabled=True)
            yield Button("Start Tests", id="start-tests", variant="success", classes="control-button", disabled=True)
            yield Button("Stop Tests", id="stop-tests", variant="error", classes="control-button", disabled=True)

        with Center():
            with Horizontal(id="checkboxes"):
                yield Checkbox("Start tests from selected row", id="tests-checkbox")
    
    def on_mount(self) -> None:
        # Enable connect button if there are servers defined
        if self.soatracer.servers != []:
            connect_button = self.query_one("#connect")
            connect_button.disabled = False

    async def on_button_pressed(self, event: Button.Pressed) -> None:
        button_id = event.button.id
        if button_id == "connect":
            # Turn control panel into a loading indicator
            control_buttons = self.parent.query_one("#control-buttons")
            control_buttons.loading = True

            self.action_connect_to_hosts()
        elif button_id == "install-deps":
            # Turn control panel into a loading indicator
            control_buttons = self.parent.query_one("#control-buttons")
            control_buttons.loading = True

            self.action_install_deps()
        elif button_id == "start-tests":
            start_button = self.query_one("#start-tests")
            start_button.disabled = True
            stop_button = self.query_one("#stop-tests")
            stop_button.disabled = False
            # Turn control panel into a loading indicator
            checkboxes = self.query_one("#checkboxes")
            checkboxes.loading = True

            self.tests_worker = self.start_tests()
        elif button_id == "stop-tests":
            stop_button = self.query_one("#stop-tests")
            stop_button.disabled = True
            self.post_message(LogBox.Msg("Stopping tests (this may take a moment) ...", "info", debug=True))
            self.stop_tests()

    def on_checkbox_changed(self, checkbox: Checkbox.Changed) -> None:
        id = checkbox.checkbox.id
        if id == "tests-checkbox":
            self.start_from_selection = checkbox.value

    def connect(self, server: RemoteServer) -> SSHClient:
        '''
        Connects to server (RemoteServer) and returns a SSHClient
        '''
        ssh = SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            ssh.connect(server.host, username=server.username,
                        password=server.password)

            # Add ssh client to remote server obj
            server.ssh = ssh

            server.status = "online"
            self.post_message(LogBox.Msg(f"Connected to {server.host}", "info"))
            return ssh
        except Exception as e:
            server.status = "failed"
            self.post_message(LogBox.Msg(f"Connecting to {server.host} failed: {e}", "error"))
            return None

    def install_deps(self, server: RemoteServer) -> bool:
        '''
        Returns true if installing SOATracer.install_deps was success otherwise false
        '''
        ssh = server.ssh
        if server.deps == "installed":
            return True
        try:
            _stdin, stdout, _stderr = sudo_execute(server, "apt-get update -y")
            stdout.channel.recv_exit_status()

            # DEBUG
            # for dep in self.soatracer.test_deps:
            for dep in self.soatracer.install_deps:
                # check_stderr(*ssh.exec_command(f"sudo apt-get install {dep} -y"))
                _stdin, stdout, _stderr = sudo_execute(server, f"apt-get install {dep} -y")
                exit_code = stdout.channel.recv_exit_status()
                if exit_code == 100:
                    self.post_message(LogBox.Msg(f"Installing {dep} failed on {server.host}", "error"))
                    server.deps = "failed"
                    return False
            
            self.post_message(LogBox.Msg(f"Successfully installed dependencies on {server.host}\nDEPS: {self.soatracer.install_deps}", "info"))
            server.deps = "installed"
            return True
        except Exception as e:
            self.post_message(LogBox.Msg(f"Installing dependencies failed on {server.host}: {e}", "error"))
            server.deps = "failed"
            return False

    def check_installed_deps(self, server: RemoteServer) -> bool:
        """
        Return True if all dependencies defined in SOATracer.deps are installed, False otherwise
        """
        self.post_message(LogBox.Msg(f"Checking dependencies on {server.host} ...", "info"))

        try:
            # DEBUG
            # for dep in self.soatracer.test_deps:
            for dep in self.soatracer.deps:
                # stdin, stdout, stderr = ssh.exec_command(f"dpkg -s {dep}")
                _stdin, stdout, stderr = sudo_execute(server, f"dpkg -s {dep}")
                stdout.channel.recv_exit_status()


                # dep. is not installed
                # remove the password prompt from stderr output to check if there is anything else left
                error = stderr.read().decode().removeprefix(f"[sudo] password for {server.username}:").strip()
                if error != "":
                    self.post_message(LogBox.Msg(f"Dependencies missing on {server.host}", "error"))
                    server.deps = "missing"
                    return False
                    
            self.post_message(LogBox.Msg(f"Dependencies already installed on {server.host}\nDEPS: {self.soatracer.deps}", "info"))
            server.deps = "installed"
            return True
            
        except Exception as e:
            log(f"Error: {e}")
            self.post_message(LogBox.Msg(f"Dependencies missing on {server.host}: {e}", "error"))

            server.deps = "missing"
            return False

    @work(thread=True)
    async def action_connect_to_hosts(self) -> None:
        '''
        Connects to the servers (RemoteServer) in SOATracer.servers and checks the dependencies
        '''
        servers = self.soatracer.servers
        servers_table = self.parent.query_one(ServersTable)

        with ThreadPoolExecutor(max_workers=len(servers)) as executor:
            connections = {executor.submit(
                self.connect, server): server for server in servers}
            
            wait(connections, return_when="ALL_COMPLETED")

            # Change host status and activate other buttons only when a connection established
            if any(conn.result() is not None for conn in connections):
                # Update table for established connections
                {executor.submit(
                    servers_table.update_connect_status, connections[ssh]): ssh for ssh in connections if ssh.result() is not None}

                self.action_check_deps()
            
            if not any(conn.result() is None for conn in connections):
                connect_button = self.query_one("#connect")
                connect_button.disabled = True

        self.stop_loading_indicator()

    @work(thread=True)
    async def action_install_deps(self) -> None:
        """
        Installs dependencies (SOATracer.deps) on servers (SOATracer.servers)
        """
        servers = self.soatracer.servers
        with ThreadPoolExecutor(max_workers=len(servers)) as executor:
            # Install dependencies with pkg manager
            futures = {executor.submit(
                self.install_deps, server): server for server in servers if server.ssh is not None}
            wait(futures, return_when="ALL_COMPLETED")

            # Install ROS2 dependencies
            futures = {executor.submit(
                ros2.install_ros2, self, server): server for server in servers if server.ssh is not None}
            wait(futures, return_when="ALL_COMPLETED")

            # Install ASOA dependencies
            futures = {executor.submit(
                asoa.install_asoa, self, server): server for server in servers if server.ssh is not None}
            wait(futures, return_when="ALL_COMPLETED")
            
            self.action_check_deps()
        
        self.stop_loading_indicator()

    @work(thread=True)
    async def action_check_deps(self) -> None:
        """
        Checks if dependencies are installed in each of the servers, then updates the status in the table.
        """
        connections = [server.ssh for server in self.soatracer.servers]
        servers = self.soatracer.servers
        servers_table = self.parent.query_one(ServersTable)

        with ThreadPoolExecutor(max_workers=len(connections)) as executor:
            status_futures = {executor.submit(
                self.check_installed_deps, server): server for server in servers if server.ssh is not None}

            wait(status_futures, return_when="ALL_COMPLETED")
            
            {self.app.call_from_thread(
                servers_table.update_deps_status, status_futures[status], status.result()): status for status in status_futures}

            for status in status_futures:
                if not status.result():
                    self.deps_installed = False
                    self.update_install_deps_button()
                    return

            self.deps_installed = True
            self.update_install_deps_button()
        
    @work(thread=True)
    async def action_check_connection(self) -> None:
        connections = [server.ssh for server in self.soatracer.servers]
        with ThreadPoolExecutor(max_workers=len(connections)) as executor:
            futures = {executor.submit(
                check_connection, ssh): ssh for ssh in connections}

    def update_install_deps_button(self) -> None:
        """Update buttons on control panel depending installed dependencies"""
        install_deps_button = self.query_one("#install-deps")
        start_tests_button = self.query_one("#start-tests")

        installed_in_all = self.deps_installed
        if not installed_in_all:
            install_deps_button.disabled = False
        else:
            install_deps_button.disabled = True
            start_tests_button.disabled = False

    @work(thread=True)
    def start_tests(self) -> None:
        servers = self.soatracer.servers

        if not check_nodes_exist(self.soatracer):
            self.post_message(LogBox.Msg("Nodes not found in bin/", "error"))
            self.reset_buttons()
            self.stop_loading_indicator()
            return

        with ThreadPoolExecutor(max_workers=len(servers)) as executor:
            # COPY NODES
            self.post_message(LogBox.Msg("Copying nodes binaries to the servers ...", "info", debug=True))

            futures = {executor.submit(
                copy_nodes, server, self.soatracer): server for server in servers if server.ssh is not None}
            wait(futures, return_when="ALL_COMPLETED")
            
            if any(future.result() is False for future in futures):
                self.post_message(LogBox.Msg("Copying nodes (executables) to servers failed", "error", debug=True))
                self.reset_buttons()
                self.stop_loading_indicator()
                return
            else:
                self.post_message(LogBox.Msg("Successfully copied the soatracer-nodes to the servers", "info"))

            # COPY SCRIPTS
            self.post_message(LogBox.Msg("Copying scripts to the servers ...", "info", debug=True))
            futures = {executor.submit(
                copy_scripts, server.ssh): server for server in servers if server.ssh is not None}
            wait(futures, return_when="ALL_COMPLETED")
            
            if any(future.result() is False for future in futures):
                self.post_message(LogBox.Msg("Copying scripts to servers failed", "error", debug=True))
                self.reset_buttons()
                self.stop_loading_indicator()
                return
            else:
                self.post_message(LogBox.Msg("Successfully copied the scripts to the servers", "info"))

            # Check, since syncing takes too long
            if not self.stopped:
                # SYNC CLOCKS ON REMOTE SERVERS
                self.post_message(LogBox.Msg("Syncing clocks of the servers ...", "info", debug=True, timestamp=True))
                futures = {executor.submit(
                    sync_clocks, server, True if idx == 0 else False): server for idx, server in enumerate(servers) if server.ssh is not None}
                wait(futures, return_when="ALL_COMPLETED")

                if any(future.result() is False for future in futures):
                    self.post_message(LogBox.Msg("Syncing clocks failed!", "error", debug=True, timestamp=True))
                    self.reset_buttons()
                    self.stop_loading_indicator()
                    return
                else:
                    self.post_message(LogBox.Msg("Clocks on the servers have been successfully synced!", "info", timestamp=True))


        selected_row = 0
        if self.start_from_selection:
            tests_table = self.parent.query_one("#tests-data-table", DataTable)
            selected_row = tests_table.cursor_row
            self.post_message(LogBox.Msg(f"Selected row {selected_row + 1}", "info"))

        if not self.stopped:
            self.post_message(LogBox.Msg("Tests starting ...", "info", timestamp=True))
            start_nodes_tests(self.soatracer, self, selected_row)

        # If stopped while testing:
        if self.stopped:
            self.post_message(LogBox.Msg("Tests stopped", "info", timestamp=True))
        else:
            self.post_message(LogBox.Msg("Tests completed", "info", timestamp=True))
            
        self.reset_buttons()
        self.stop_loading_indicator()

    def stop_tests(self) -> None:
        self.stopped = True
    
    def stop_loading_indicator(self) -> None:
        control_buttons = self.parent.query_one("#control-buttons")
        control_buttons.loading = False
        checkboxes = self.parent.query_one("#checkboxes")
        checkboxes.loading = False

    def reset_buttons(self) -> None:
        stop_button = self.parent.query_one("#stop-tests")
        stop_button.disabled = True
        start_button = self.parent.query_one("#start-tests")
        start_button.disabled = False
        # reset stop
        self.stopped = False

class Dashboard(Static):
    def __init__(self, soatracer: SOATracer) -> None:
        super().__init__()

        self.soatracer = soatracer

    def compose(self) -> ComposeResult:
        with TabbedContent():
            with TabPane("Servers"):
                yield ServersTable(self.soatracer)
            with TabPane("Tests"):
                yield TestsTable(self.soatracer, id="tests-table")
        yield Rule(id="controls-rule")
        yield ControlPanel(self.soatracer, id="control-panel")
        yield Rule(id="log-rule")
        with Middle():
            with Center():
                yield LogBox(self.soatracer)

    def on_log_box_msg(self, msg: LogBox.Msg) -> None:
        # log everything sent to the logbox
        # self.log(msg.msg)
        rich_log = self.query_one("#logger")
        rich_log.write(msg.value)


class UtilsRunner(Static):
    def compose(self) -> ComposeResult:
        options = ["generate_table", "generate_tests"]
        with Horizontal():
            yield Select.from_values(options)
            yield Button("Run command", id="run_cmd", variant="default")

    @on(Select.Changed)
    def select_changed(self, event: Select.Changed) -> None:
        self.title = str(event.value)

    def on_button_pressed(self, event: Button.Pressed) -> None:
        if self.title == "generate_table":
            generate_table()
        elif self.title == "generate_tests":
            generate_tests()

class MainScreen(Screen):
    def __init__(self, soatracer: SOATracer, id: str | None = None) -> None:
        super().__init__(id=id)

        self.soatracer = soatracer
        soatracer.app = self

    def compose(self) -> ComposeResult:
        yield Header()
        yield Footer()
        yield Dashboard(self.soatracer)


class SOATracerApp(App):
    """Framework Latency Testing Tool"""
    CSS_PATH = "styles/styles.tcss"

    TITLE = "SOATracer"
    SUB_TITLE = "SOA Latency Testing Tool"

    SCREENS = {
        "main": MainScreen
    }

    BINDINGS = [
        # ("d", "toggle_dark", "Toggle dark mode"),
        ("ctrl+q, ctrl+c", "request_quit", "Quit"),
        # ("ctrl+q", "request_quit", "Quit"),
        ("ctrl+t", "request_test_conn", "Test Connections")
    ]

    def __init__(self, soatracer: SOATracer) -> None:
        super().__init__()

        self.soatracer = soatracer
        soatracer.app = self
        self.main_screen = MainScreen(self.soatracer)

    def on_mount(self) -> None:
        self.push_screen(self.main_screen)

    def action_toggle_dark(self) -> None:
        return super().action_toggle_dark()

    def action_request_quit(self) -> None:
        """Action to display the quit dialog."""
        self.push_screen(QuitScreen(self.soatracer))

    def action_request_test_conn(self) -> None:
        """Action to display the quit dialog."""
        self.push_screen(TestConnScreen(self.soatracer))
    


def main():
    args = parse_args()
    soatracer = SOATracer(args)
    app = SOATracerApp(soatracer)
    app.run()


if __name__ == "__main__":
    main()